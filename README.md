# CES-Seminar ADMission


## Automatic AD mission (ADM) pipeline execution
Functionality to execute all steps of the AD mission workflow are implemented in Julia.
A 1D Sod shock tube FVM solver is used as an application example.
The basic steps are
- 1) tracing the target function to create a truncated call tree
- 2) converting the trace to a dag via a taping procedure that uses dependency analysis
- 3) profiling the dag edges to create an annotated dag
- 4) calling the admission utility to compute a schedule
- 5) create a re-trace from the trace to enable updating of dag edge arguments
- 6) creating a so-called (for lack of a better name) recipe from the dag, schedule and retrace that can be used to compute the Jacobian of the target function at some arguments

While the recipe can be used to compute only the Jacobian, the intended purpose is to compute the primal and Jacobian together.
The recipe allows for dense and sparse (intermediate Jacobian) modes. Note however that the annotation and schedule are created assuming dense Jacobians.

Enzyme is employed to create the AD models of DAG edges.
ColPack is used for the sparse extension.
AD mission is used for schedule creation from an annotated dag.

**Requirements**:
Julia (version 1.8.5 or newer), some packages (most notably Enzyme, Cassette).
For ColPack and AD mission calls to work a .\temp directory should be created.

### file overview
**module**: Contains modules for the individual functionalities outlined above. To understand it, start at recipe.jl and work backwards.\
**tests**: Contains tests and small benchmarks for the individual module components.\
**main.jl**: Some computations for the report and presentation. To be augmented by smaller separate benchmark files.\
(Instead of a loose assortment of files, wrapping everything in a package would be preferable. This is left as an exercise for the user.)

### disclaimer
This is a rapid prototype. Consequently spaghetti code and questionable comments are present. Proceed at your own risk.



## Initial investigations

### randomDAG
###### standalone
Contains initial files for random layered DAG generation and admission call.
Requires admission app to be inside of PATH.
###### dependent
Contains source file for an app using the ADMission source code as library.
Should be placed inside of the ADMission/app folder together with the modified CMakeLists.txt, or alternatively somehow link to ADMission library with cmake.

### profiling
###### attempt++
Function capture in a Tape with automatic dag generation and profiling attempt in c++.
Currently has issues with return type of captured functions for the Jacobian being double instead of tangent_t.
