

#=
Various jacobian accumulation methods benchmarked for number of cells / system size and chunk size of AD vector mode.
Uses Fixed dt SSPRK3! function.
=#


include("../module/shock_tube.jl")
include("../module/recipe.jl")
include("bench_config.jl")

using BenchmarkTools




n_cells = [32, 128, 512, 1048]
n_chunks= [8, 16, 32, 64]


schedule=
" ACC TAN (4 5) 
  ELI TAN (4 5 6)
  ELI ADJ (0 4 6)
  ACC ADJ (0 6)
  ACC TAN (0 1)
  ELI TAN (0 1 6)
  ELI TAN (0 1 2)
  ELI TAN (0 1 4)
  ELI MUL (0 4 6)
  ELI ADJ (3 4 6)
  ACC TAN (0 2)
  ELI TAN (0 2 3)
  ELI TAN (0 3 6)
  ELI MUL (0 3 6)"


function wrapper!(u)
  SSPRK3!(u)
  return u
end
#=
The jacobian() functions execute primal and thus require the input argument to not be modified.
Wrapping SSPRK3! in another function with a copy operations (as done in enzyme_test.jl) crashes the reverse mode (although just the copy is fine again).
Hence, we run forward and reverse with wrong primal result, since it gets incremented.
As an additional comparison point, forward with a wrapper is also benchmarked.
=#


for N in n_cells
  for chunk in n_chunks 

    u = zeros(3*N); set_initial_conditions!(u)
    config = ADmConfig(mode="both", depth=2, bench_params=BenchmarkParams(1e7, 1), elim_method="GreedyMinFill", chunk=chunk, temp_dir=true)
    dense_recipe, sparse_recipe = adm_generate(SSPRK3!, (u,), config, schedule=schedule);

    acc_fwd = CAccumulate(SSPRK3!, u, Forward)
    acc_bwd = CAccumulate(SSPRK3!, u, Reverse)

    # compile
    println("JIT compiling jacobian functions")
    @time Enzyme.jacobian(Forward, wrapper!, u, Val(chunk))
    println("JIT non-enzyme jacobians")
    @time jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(chunk))
    @time jacobian(Reverse, SSPRK3!, (u,), Val(1), Val(1), Val(chunk))
    println("JIT compressed accumulation")
    @time acc_fwd(u)
    @time acc_bwd(u)
    println("JIT recipes")
    @time dense_recipe(u)
    @time sparse_recipe(u)

    println("================================================ Benchmarking N=$N, chunk=$chunk ===========================================================")
    
    # ignore enzyme forward primal multiple evaluation issue here for benchmarking purposes
    println("========================================= Enzyme forward jacobian:")
    display(@benchmark Enzyme.jacobian(Forward, wrapper!, $u, Val($chunk)) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
    
    println("================================================================================== My jacobian forward:")
    display(@benchmark jacobian(Forward, SSPRK3!, ($u,), Val(1), Val(1), Val($chunk)) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
    println("================================================================================== My jacobian backward:")
    display(@benchmark jacobian(Reverse, SSPRK3!, ($u,), Val(1), Val(1), Val($chunk)) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
    
    println("================================================================================== Dense recipe:")
    display(@benchmark $dense_recipe($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
    println("================================================================================== Sparse recipe: ")
    display(@benchmark $sparse_recipe($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds)

    println("================================================================================== Compressed Jacobian forward:")
    display(@benchmark $acc_fwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
    println("================================================================================== Compressed Jacobian backward: ")
    display(@benchmark $acc_bwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
  end
end

#= Results:
It seems that the chunksize does not have a large impact. I assume this is due to the large overhead.
The ad evaluation cost and primal cost pale in comparison to the overhead.
Hence, a chunksize of 16 x 2 evals vs. 32 x 1 eval makes little difference, since the overhead is split, but the autodiff cost is negligible.

=> Choose a chunk size of 32, since it allows for a single eval sparse pattern. But for our cases, the choice is kinda inconsequential.
=#





