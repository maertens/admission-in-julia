#=
Runs a long dag annotation and writes the resulting dag xml files into ./temp
=#

include("../module/recipe.jl")
include("../module/shock_tube.jl")
include("bench_config.jl")


N = 128
chunk=32
u = zeros(3*128)
set_initial_conditions!(u)

config = ADmConfig(mode="both", depth=2, bench_params=BenchmarkParams(samples, seconds), elim_method="BranchAndBound", chunk=chunk, temp_dir=true)


trace = trace_function(config.depth, SSPRK3!, (u,))
dag = tape(trace)
annotation = annotate_dag(dag, config.bench_params)
# write the xml for later admission planning
to_xml(annotation, "const_dt_annotation_seconds_$(seconds)_dag.xml")


trace = trace_function(config.depth, SSPRK3_dt!, (u,))
dag = tape(trace)
annotation = annotate_dag(dag, config.bench_params)
# write the xml for later admission planning
to_xml(annotation, "varying_dt_annotation_seconds_$(seconds)_dag.xml")




# use the annotation to calculate costs of given schedules

baseline_schedule=
"ELI TAN (0 1 3) 1539456
ELI TAN (0 1 5) 2184576
ACC TAN (0 2) 1797674880
ELI TAN (0 2 3) 70369920
ELI TAN (0 2 5) 82192128
ELI TAN (0 2 7) 125111808
ACC TAN (0 3) 70704768
ELI TAN (0 3 4) 1831230720
ELI TAN (0 4 5) 82581504
ACC TAN (0 5) 82993920
ELI TAN (0 5 6) 1888210560
ELI TAN (0 4 7) 125013504
ELI TAN (0 6 7) 123663360
ACC TAN (0 7) 116592000
ACC ADJ (0 1) 38346
ACC TAN (1 7) 6947
ELI MUL (0 1 7) 147456"

delay_dt_schedule=
"ACC TAN (1 3)
 ACC TAN (1 4)
 ACC TAN (1 6)
 ACC TAN (1 8)
 ACC ADJ (1 2)
 ACC TAN (2 4)
 ACC TAN (2 6)
 ACC TAN (2 8)
 ELI TAN (1 3 8)
 ELI TAN (1 3 4)
 ELI TAN (1 3 6)
 ELI TAN (1 4 5)
 ELI TAN (1 5 6)
 ELI TAN (1 5 8)
 ELI TAN (1 6 7)
 ELI TAN (1 7 8)
 ELI TAN (2 4 5)
 ELI TAN (2 5 6)
 ELI TAN (2 5 8)
 ELI TAN (2 6 7)
 ELI TAN (2 7 8)
 ELI MUL (1 2 8)"
# decrement all node indices, since we expect zero-start (admission output is zero indexed)
temp = collect(delay_dt_schedule) # convert to mutable vector
for i in eachindex(temp)
  if isdigit(temp[i])
    temp[i] -= 1
  end
end
delay_dt_schedule = String(temp)



function compute_cost(annotation, schedule)
  cost = 0
  edge_dict = Dict()
  for e in annotation.edges
    edge_dict[e.conn] = e.cost
  end

  # same structure as recipe creation procedure
  for (op, mode, target) in zip(schedule.operations, schedule.modes, schedule.targets)
    i = target[1]; j = target[end] # i = input idx, j = output idx
    # can treat ELI and ACC at once, since we only require the input column/row seeding sizes, which can be accessed 
    if mode == "TAN"
      # ACC has [i j], so [end-1]==[i] is the same seeding column size as i=target[1] from elimination
      cost += edge_dict[target[end-1] => target[end]].first * annotation.node_sizes[i] # edge cost * #columns to seed
    elseif mode == "ADJ"
      cost += edge_dict[target[1] => target[2]].second * annotation.node_sizes[j]
    else # MUL
      cost += annotation.node_sizes[target[1]] * annotation.node_sizes[target[2]] * annotation.node_sizes[target[3]]
    end

  end
  return cost
end



schedule_baseline = parse_schedule(split(baseline_schedule, "\n"))
schedule_delay_dt = parse_schedule(split(delay_dt_schedule, "\n"))


println("Baseline schedule cost:                     ", compute_cost(annotation, schedule_baseline))
println("Custom delay dt accumulation schedule cost: ", compute_cost(annotation, schedule_delay_dt))

