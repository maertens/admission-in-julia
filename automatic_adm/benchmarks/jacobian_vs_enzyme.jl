#=
Benchmark enzyme jacobian function vs. my simple jacobian wrappers.
Used to present once to establish comparability in runtime to the baseline Enzyme.
Also discard enzyme thereafter due to the long compile times. -> Only consider our implementation afterwards.
=#

include("../module/shock_tube.jl")
include("../module/jacobian.jl")
include("bench_config.jl")
using Enzyme
using BenchmarkTools

function wrapper_RK3!(u)
  SSPRK3!(u)
  return u
end

function copy_wrapper_RK3!(u)
  w = copy(u)
  SSPRK3!(w)
  return w
end

#=
The cause behind the inconsistent behavior of the reverse enzyme jacobian (sometimes crashing and sometimes not)
seems to be partially due to the chunk size.
Here, a chunk size fo 16 works fine, but 32 crashes the reverse compilation.
=> Use a chunksize of 16 here, irrespective of what is chosen afterwards for the other comparisons (e.g. 32).
=#

chunk::Int = 16


dict = Dict() # store trials for post-processing


for N in [32, 128, 1048] # small, baseline, (relatively) large number of cells
  println("Benchmarking for N=$N")
  u = zeros(3*N); set_initial_conditions!(u)
  # precompile
  println("Precompiling enzyme forward wrapper")
  Enzyme.jacobian(Forward, wrapper_RK3!, u, Val(chunk))
  println("Precompiling enzyme reverse wrapper")
  Enzyme.jacobian(Reverse, wrapper_RK3!, u, Val(length(u)), Val(chunk))
  println("Precompiling enzyme forward copy wrapper")
  Enzyme.jacobian(Forward, copy_wrapper_RK3!, u, Val(chunk))
#  println("Precompiling enzyme reverse copy wrapper")
#  Enzyme.jacobian(Reverse, copy_wrapper_RK3!, u, Val(length(u)), Val(chunk))
  println("Precompiling jacobian forward")
  jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(chunk))
  println("Precompiling jacobian reverse")
  jacobian(Reverse, SSPRK3!, (u,), Val(1), Val(1), Val(chunk))
  # autodiff, even for fixed chunk makes sense here, since size N is changing -> cost is too
  println("Precompiling autodiff forward")
  Enzyme.autodiff(Forward, SSPRK3!, Const, BatchDuplicated(u, chunkedonehot(u, Val(chunk))[1]))
  println("Precompiling autodiff reverse")
  Enzyme.autodiff(Reverse, SSPRK3!, Const, BatchDuplicated(u, chunkedonehot(u, Val(chunk))[1]))
  println("Finished precompiling, starting benchmarks...")

  dict[N => "EnzymeFwdNoCopy"] = @benchmark Enzyme.jacobian(Forward, wrapper_RK3!, $u, Val(chunk)) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  dict[N => "EnzymeRevNoCopy"] = @benchmark  Enzyme.jacobian(Reverse, wrapper_RK3!, $u, Val(length($u)), Val(chunk)) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  dict[N => "EnzymeFwdCopy"] = @benchmark Enzyme.jacobian(Forward, copy_wrapper_RK3!, $u, Val(chunk)) setup=set_initial_conditions!($u) samples=samples seconds=seconds
#  dict[N => "EnzymeRevCopy"] = @benchmark Enzyme.jacobian(Reverse, copy_wrapper_RK3!, $u, Val(length(u)), Val(chunk)) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  dict[N => "JacFwd"] = @benchmark jacobian(Forward, SSPRK3!, ($u,), Val(1), Val(1), Val(chunk)) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  dict[N => "JacRev"] = @benchmark jacobian(Reverse, SSPRK3!, ($u,), Val(1), Val(1), Val(chunk)) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  dict[N => "autodiffFwd"] = @benchmark Enzyme.autodiff(Forward, SSPRK3!, Const, BatchDuplicated($u, chunkedonehot($u, Val(chunk))[1])) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  dict[N => "autodiffRev"] = @benchmark Enzyme.autodiff(Reverse, SSPRK3!, Const, BatchDuplicated($u, chunkedonehot($u, Val(chunk))[1])) setup=set_initial_conditions!($u) samples=samples seconds=seconds
end


# print results
for trial in dict
  println("================================================================== ", trial.first)
  display(trial.second)
end


# print median raw data for the table
for N in [32, 128, 1048]
  println("============== N = $N ================")
  for trial in dict
    if trial.first.first == N
      ms = median(trial.second).time / 1e6
      if occursin("autodiff", trial.first.second)
        ms = ms * 3 * N / chunk # multiply runtime with number of evaluations required for jacobian accumulation
      end
      println("$(trial.first.second): $ms [ms]")
    end
  end
end



#=
du = chunkedonehot(u, Val(chunk))[1]
Enzyme.autodiff(Forward, SSPRK3!, Const, BatchDuplicated(u, du))
Enzyme.autodiff(Forward, SSPRK3!, Const, BatchDuplicated(copy(u), chunkedonehot(u, Val(chunk))[1]))
@benchmark Enzyme.autodiff(Forward, SSPRK3!, Const, BatchDuplicated(copy(u), chunkedonehot(u, Val(chunk))[1])) setup=set_initial_conditions!(u)

for autodiff, there seems to be no detriment to calling copy and onehot init inside the function call.
Furthermore, especially for reverse, a single eval is way more expensive than a chunked evaluation.

note: multiply autodiff runtimes with the amount of runs required for the primal evaluation
=#

