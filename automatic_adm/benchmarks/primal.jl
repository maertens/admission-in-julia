
include("../module/shock_tube.jl")
include("../module/recipe.jl")
include("bench_config.jl")
using BenchmarkTools


######################## Benchmark primal with and without preallocation ########################


n = 1e4

n_cells = [32, 128, 512, 1048]

for N in n_cells
  println("================================================================== N = $N ==================================================================")
  u = zeros(3*N)
  set_initial_conditions!(u)

  trace = trace_function(2, SSPRK3!, (u,))
  dag = tape(trace)
  retrace = ReTrace(dag, trace)


  println("============================================== PRIMAL (PRE)ALLOCATION ==============================================")
  display(@benchmark n_steps_primal!($u, n) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
  display(@benchmark n_steps_primal_prealloc!($u, n) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
  #=
  Result: barely any difference in time (2.195 s vs 2.211 s), but also barely a difference in #allocations (4.81 GiB vs 4.66 GiB)
  Reason for this is rhs!() and internally lax_friedrichs() which produce allocations, in total more than the measly auxiliary vectors.
  While for overall code efficiency this is detrimental, for us this just adds extra internal complexity (like a more expensive flux function).
  Good, since we want expensive elementals/primal evaluation.

  => Neglect the allocations in the SSPRK3! function as they are irrelevant for the qualitative comparisons of jacobian accumulations.
  This is in the spirit of getting a prototype up and running, not a final product.
  =#



  println("============================================== PRIMAL RETRACING ==============================================")

  # comparison of primal, traced primal and retraced primal
  println("\n========================================================================\nPrimal benchmark:")
  display(@benchmark SSPRK3!($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
  println("\n========================================================================\nTracing benchmark:")
  display(@benchmark trace_function(2, SSPRK3!, ($u,)) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
  println("\n========================================================================\nRe-Tracing benchmark:")
  display(@benchmark primal_with_trace!($retrace, SSPRK3!, ($u,)) setup=set_initial_conditions!($u) samples=samples seconds=seconds)
  # => barely any difference between retrace and primal

end



