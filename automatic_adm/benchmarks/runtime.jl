#=
Benchmark different jacobian accumulation functions over system size for runtime plots.
=#

include("../module/recipe.jl")
include("../module/shock_tube.jl")
include("bench_config.jl")

n_cells = [32, 64, 128, 256, 512, 1048] # [32, 64]

chunk = 32





adm_config = ADmConfig(mode="both", depth=2, elim_method="BranchAndBound", chunk=32, temp_dir=true)



function bench(func, N, schedule)
  println("Benchmarking $func with N=$N. Schedule:")
  println(schedule)
  u = zeros(3*N)
  set_initial_conditions!(u)
  median_times = []

  # plain jacobian functions
  @inline jac_fwd(x) = jacobian(Forward, func, (x,), Val(1), Val(1), Val(chunk))
  @inline jac_bwd(x) = jacobian(Reverse, func, (x,), Val(1), Val(1), Val(chunk))
  # compressed accumulation functions
  acc_fwd = CAccumulate(func, u, Forward)
  acc_bwd = CAccumulate(func, u, Reverse)
  # ADM recipes
  dense_recipe, sparse_recipe = adm_generate(func, (u,), adm_config, schedule=schedule)

  # precompile
  jac_fwd(u); jac_bwd(u); acc_fwd(u); acc_bwd(u); dense_recipe(u); sparse_recipe(u);

  trial = @benchmark $jac_fwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== jac_fwd"); display(trial)
  trial = @benchmark $jac_bwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== jac_bwd"); display(trial)
  
  trial = @benchmark $acc_fwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== acc_fwd"); display(trial)
  trial = @benchmark $acc_bwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== acc_bwd"); display(trial)

  trial = @benchmark $dense_recipe($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== dense_recipe"); display(trial)
  trial = @benchmark $sparse_recipe($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== sparse_recipe"); display(trial)

  return median_times
end








println("================================================================ CONSTANT DT ================================================================")


branch_and_bound_schedule=
" ACC TAN (0 1)
  ACC TAN (0 2)
  ACC TAN (0 4)
  ACC TAN (0 6)
  ELI TAN (0 1 2)
  ELI TAN (0 1 4)
  ELI TAN (0 1 6)
  ELI TAN (0 2 3)
  ELI TAN (0 3 4)
  ELI TAN (0 4 5)
  ELI TAN (0 3 6)
  ELI TAN (0 5 6)" # dense tangent -> gives measurement of overhead when compared to jac_fwd() call


runtime_dict_constant_dt = Dict()
for N in n_cells
  runtime_dict_constant_dt[N] = bench(SSPRK3!, N, branch_and_bound_schedule)
end

println("Tabulated results for constant dt branch and bound schedule:")
println("# N jac_fwd jac_bwd acc_fwd acc_bwd dense_recipe sparse_recipe")
for N in n_cells
  times = runtime_dict_constant_dt[N]
  println("$N $(times[1]) $(times[2]) $(times[3]) $(times[4]) $(times[5]) $(times[6])")
end



println("================================================================ VARYING DT ================================================================")


branch_and_bound_schedule=
"ELI TAN (0 1 3)
ELI TAN (0 1 5)
ACC TAN (0 2)
ELI TAN (0 2 3)
ELI TAN (0 2 5)
ELI TAN (0 2 7)
ACC TAN (0 3)
ELI TAN (0 3 4)
ELI TAN (0 4 5)
ACC TAN (0 5)
ELI TAN (0 5 6)
ELI TAN (0 4 7)
ELI TAN (0 6 7)
ACC TAN (0 7)
ACC ADJ (0 1)
ACC TAN (1 7)
ELI MUL (0 1 7)"

delay_dt_schedule=
"ACC TAN (0 2)
 ACC TAN (0 3)
 ACC TAN (0 5)
 ACC TAN (0 7)
 ACC ADJ (0 1)
 ACC TAN (1 3)
 ACC TAN (1 5)
 ACC TAN (1 7)
 ELI TAN (0 2 7)
 ELI TAN (0 2 3)
 ELI TAN (0 2 5)
 ELI TAN (0 3 4)
 ELI TAN (0 4 5)
 ELI TAN (0 4 7)
 ELI TAN (0 5 6)
 ELI TAN (0 6 7)
 ELI TAN (1 3 4)
 ELI TAN (1 4 5)
 ELI TAN (1 4 7)
 ELI TAN (1 5 6)
 ELI TAN (1 6 7)
 ELI MUL (0 1 7)"



runtime_dict_varying_dt = Dict()
for N in n_cells
  runtime_dict_varying_dt[N] = bench(SSPRK3_dt!, N, branch_and_bound_schedule)
end

println("Tabulated results for varying dt branch and bound schedule:")
println("# N jac_fwd jac_bwd acc_fwd acc_bwd dense_recipe sparse_recipe")
for N in n_cells
  times = runtime_dict_varying_dt[N]
  println("$N $(times[1]) $(times[2]) $(times[3]) $(times[4]) $(times[5]) $(times[6])")
end



runtime_dict_varying_dt_delay_dt = Dict()
for N in n_cells
  runtime_dict_varying_dt_delay_dt[N] = bench(SSPRK3_dt!, N, delay_dt_schedule)
end

println("Tabulated results for varying dt custom delay dt accumulation schedule:")
println("# N jac_fwd jac_bwd acc_fwd acc_bwd dense_recipe sparse_recipe")
for N in n_cells
  times = runtime_dict_varying_dt_delay_dt[N]
  println("$N $(times[1]) $(times[2]) $(times[3]) $(times[4]) $(times[5]) $(times[6])")
end

