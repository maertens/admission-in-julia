#=
COPY OF runtime.jl FOR ONLY VARYING DT, SINCE PREVIOUS BENCH SEEMS TO HAVE CRASHED
=#

include("../module/recipe.jl")
include("../module/shock_tube.jl")
include("bench_config.jl")

#n_cells = [parse(Int, s) for s in ARGS] # command line argument
n_cells = [32, 64, 128, 256, 512, 1048]

chunk = 32





adm_config = ADmConfig(mode="both", depth=2, elim_method="BranchAndBound", chunk=32, temp_dir=true)



function bench(func, N, schedule1, schedule2)
  println("Benchmarking $func with N=$N. Schedule:")
  println(schedule1)
  println("\nand\n", schedule2)
  u = zeros(3*N)
  set_initial_conditions!(u)
  median_times = []

  # plain jacobian functions
  @inline jac_fwd(x) = jacobian(Forward, func, (x,), Val(1), Val(1), Val(chunk))
  @inline jac_bwd(x) = jacobian(Reverse, func, (x,), Val(1), Val(1), Val(chunk))
  # compressed accumulation functions
  acc_fwd = CAccumulate(func, u, Forward)
  acc_bwd = CAccumulate(func, u, Reverse)
  # ADM recipes
  dense_recipe1, sparse_recipe1 = adm_generate(func, (u,), adm_config, schedule=schedule1)
  dense_recipe2, sparse_recipe2 = adm_generate(func, (u,), adm_config, schedule=schedule2)


  # precompile
  println("precompiling")
  println("jac fwd"); jac_fwd(u);
  println("jac bwd"); jac_bwd(u);
  println("acc fwd"); acc_fwd(u);
  println("acc bwd"); acc_bwd(u);
  println("dense recipe 1"); dense_recipe1(u);
  println("sparse recipe 1"); sparse_recipe1(u);
  println("dense recipe 2"); dense_recipe2(u);
  println("sparse recipe 2"); sparse_recipe2(u);
  println("finished precompiling, starting benchmarks")

  trial = @benchmark $jac_fwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== jac_fwd"); display(trial)
  trial = @benchmark $jac_bwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== jac_bwd"); display(trial)
  
  trial = @benchmark $acc_fwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== acc_fwd"); display(trial)
  trial = @benchmark $acc_bwd($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== acc_bwd"); display(trial)

  trial = @benchmark $dense_recipe1($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== dense_recipe 1"); display(trial)
  trial = @benchmark $sparse_recipe1($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== sparse_recipe 1"); display(trial)

  trial = @benchmark $dense_recipe2($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== dense_recipe 2"); display(trial)
  trial = @benchmark $sparse_recipe2($u) setup=set_initial_conditions!($u) samples=samples seconds=seconds
  push!(median_times, median(trial).time / 1e6)
  println("============================== sparse_recipe 2"); display(trial)

  return median_times
end







println("================================================================ VARYING DT ================================================================")


branch_and_bound_schedule=
"ELI TAN (0 1 3)
ELI TAN (0 1 5)
ACC TAN (0 2)
ELI TAN (0 2 3)
ELI TAN (0 2 5)
ELI TAN (0 2 7)
ACC TAN (0 3)
ELI TAN (0 3 4)
ELI TAN (0 4 5)
ACC TAN (0 5)
ELI TAN (0 5 6)
ELI TAN (0 4 7)
ELI TAN (0 6 7)
ACC TAN (0 7)
ACC ADJ (0 1)
ACC TAN (1 7)
ELI MUL (0 1 7)"

delay_dt_schedule=
"ACC TAN (0 2)
 ACC TAN (0 3)
 ACC TAN (0 5)
 ACC TAN (0 7)
 ACC ADJ (0 1)
 ACC TAN (1 3)
 ACC TAN (1 5)
 ACC TAN (1 7)
 ELI TAN (0 2 7)
 ELI TAN (0 2 3)
 ELI TAN (0 2 5)
 ELI TAN (0 3 4)
 ELI TAN (0 4 5)
 ELI TAN (0 4 7)
 ELI TAN (0 5 6)
 ELI TAN (0 6 7)
 ELI TAN (1 3 4)
 ELI TAN (1 4 5)
 ELI TAN (1 4 7)
 ELI TAN (1 5 6)
 ELI TAN (1 6 7)
 ELI MUL (0 1 7)"



runtime_dict_varying_dt = Dict()
for N in n_cells
  runtime_dict_varying_dt[N] = bench(SSPRK3_dt!, N, branch_and_bound_schedule, delay_dt_schedule)
end

println("Tabulated results for varying dt branch and bound schedule:")
println("# N jac_fwd jac_bwd acc_fwd acc_bwd dense_recipe1 sparse_recipe dense_recipe (manual schedule) sparse_recipe (manual schedule)")
for N in n_cells
  times = runtime_dict_varying_dt[N]
  println("$N $(times[1]) $(times[2]) $(times[3]) $(times[4]) $(times[5]) $(times[6]) $(times[7]) $(times[8])")
end

