#=
Bench greedy min fill schedule vs branch and bound (dense tangent) schedule.
Goal is to compare this with the theoretical difference/ratio in runtime / cost.
=#

include("../module/recipe.jl")
include("../module/shock_tube.jl")
include("bench_config.jl")


config = ADmConfig(depth=2, mode="dense", chunk=32, temp_dir=true)
u = zeros(3*128); set_initial_conditions!(u);


branch_and_bound_schedule=
" ACC TAN (0 1)
  ACC TAN (0 2)
  ACC TAN (0 4)
  ACC TAN (0 6)
  ELI TAN (0 1 2)
  ELI TAN (0 1 4)
  ELI TAN (0 1 6)
  ELI TAN (0 2 3)
  ELI TAN (0 3 4)
  ELI TAN (0 4 5)
  ELI TAN (0 3 6)
  ELI TAN (0 5 6)"

greedy_min_fill_schedule=
" ACC TAN (4 5) 
  ELI TAN (4 5 6)
  ELI ADJ (0 4 6)
  ACC ADJ (0 6)
  ACC TAN (0 1)
  ELI TAN (0 1 6)
  ELI TAN (0 1 2)
  ELI TAN (0 1 4)
  ELI MUL (0 4 6)
  ELI ADJ (3 4 6)
  ACC TAN (0 2)
  ELI TAN (0 2 3)
  ELI TAN (0 3 6)
  ELI MUL (0 3 6)"


recipe = adm_generate(SSPRK3!, (u,), config, schedule=branch_and_bound_schedule)
println("Branch and Bound (tense tangent) schedule:")
recipe(u)
trial = @benchmark recipe(u) setup=set_initial_conditions!(u) samples=samples seconds=seconds
display(trial)
branch_and_bound_time = median(trial).time

recipe = adm_generate(SSPRK3!, (u,), config, schedule=greedy_min_fill_schedule)
println("GreedyMinFill schedule:")
recipe(u);
trial = @benchmark recipe(u) setup=set_initial_conditions!(u) samples=samples seconds=seconds
display(trial)
greedy_min_fill_time = median(trial).time



println("Ratio of greedy min fill to branch and bound time: $(greedy_min_fill_time/branch_and_bound_time)")
#=
from admission call on annotated xml we get the costs
branch and bound / dense tangent: 0
GreedyMinFill :                   0
Ratio:                            0

=#

# TODO:
# from the annotation from the cluster benchmark calculate the dense and greedy costs via calling admission -> paste results here in comment
# divide results to get ratio
