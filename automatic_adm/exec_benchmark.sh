# chmod 755 exec_benchmark.sh
# call with ./exec_benchmark.sh primal.jl
# assumes called in highest top level directory with ./benchmarks and ./bench_output directories existing


BENCH_DIR="./benchmarks"
OUT_DIR="./bench_output"

basename=$(echo $1 | tr "." " " | cut --delimiter " " --fields 1)
core=$2
# echo $basename
# echo $basename.txt
# echo $BENCH_DIR/$1
# echo $OUT_DIR/$basename.txt
# echo $core

#taskset -c $core julia --project=cess benchmarks/$1 > bench_output/$basename.txt | tee bench_output/$basename.txt
taskset -c $core julia --project=cess BENCH_DIR/$1 | tee OUT_DIR/$basename.txt
