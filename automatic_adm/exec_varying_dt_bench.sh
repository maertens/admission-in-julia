# chmod 755 exec_benchmark.sh


taskset -c 0 julia --project=cess benchmarks/runtime_varying_dt.jl 32 | tee bench_output/runtime_varying_dt_n32.txt
taskset -c 2 julia --project=cess benchmarks/runtime_varying_dt.jl 64 | tee bench_output/runtime_varying_dt_n64.txt
taskset -c 4 julia --project=cess benchmarks/runtime_varying_dt.jl 128 | tee bench_output/runtime_varying_dt_n128.txt
taskset -c 6 julia --project=cess benchmarks/runtime_varying_dt.jl 256 | tee bench_output/runtime_varying_dt_n256.txt
taskset -c 8 julia --project=cess benchmarks/runtime_varying_dt.jl 512 | tee bench_output/runtime_varying_dt_n512.txt
taskset -c 10 julia --project=cess benchmarks/runtime_varying_dt.jl 1048 | tee bench_output/runtime_varying_dt_n1048.txt
