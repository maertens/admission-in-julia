using Cassette

Cassette.@context TraceCtx

mutable struct Trace
    current::Vector{Any}
    stack::Vector{Any}
    Trace() = new(Any[], Any[])
end

function enter!(t::Trace, args...)
    pair = args => Any[] # args is a tuple -> creates a pair args => Any[] (vector of any)
    push!(t.current, pair) # append the pair to the current trace
    push!(t.stack, t.current) # push current trace into stack
    t.current = pair.second # set current to the second element of the current pair (which is Any[])
    # -> the empty Any passed to the next lower call will collect information of those calls in a nested structure!
    # at the end, current contains a full call-stack, since pair.second is a reference (or whatever in julia)
    return nothing
end

function exit!(t::Trace)
    t.current = pop!(t.stack) # retrieve the trace of the next higher call (the callers current)
    return nothing
end

Cassette.prehook(ctx::TraceCtx, args...) = enter!(ctx.metadata, args...)
Cassette.posthook(ctx::TraceCtx, args...) = exit!(ctx.metadata)

trace = Trace()
x, y, z = rand(3)
f(x, y, z) = x*y + y*z
Cassette.overdub(TraceCtx(metadata = trace), () -> f(x, y, z))

# returns `true`
trace.current == Any[
    (f,x,y,z) => Any[
        (*,x,y) => Any[(Base.mul_float,x,y)=>Any[]]
        (*,y,z) => Any[(Base.mul_float,y,z)=>Any[]]
        (+,x*y,y*z) => Any[(Base.add_float,x*y,y*z)=>Any[]]
    ]
]

