using Graphs, GraphPlot, Compose
using GraphRecipes, Plots

path = pwd()*"/temp"*"/image.svg"

g = SimpleDiGraph(0, 0) # number of nodes, number of edges | randomly generated
add_vertices!(g, 4) # add 4 vertices
add_edge!(g, 1, 2) # (g, source, destination)
add_edge!(g, 1, 3)
add_edge!(g, 3, 4)

#rem_vertex!(g, 6) # remove vertex 6 (which was just added) => vertices are all just enumerated

edgelabel_dict = Dict()
edgelabel_dict[(1,2)] = "one to two"
edgelabel_dict[(1,3)] = "one to three"
edgelabel_dict[(3,4)] = "three to four"

#nodelabels = 1:nv(g) # nv(g) is number of vertices
nodelabels = ["one", "two", "three", "four"]

# using GraphRecipes
graphplot(g, names=nodelabels, edgelabel=edgelabel_dict, method=:tree, curves=false, show=true)


# using GraphPlot gplot() and Compose draw()
draw(SVG(path, 16cm, 16cm), gplot(g))



# one can even plot an abstract syntax tree of julia code:
# https://docs.juliaplots.org/stable/GraphRecipes/examples/#Julia-code-%E2%80%93-AST



