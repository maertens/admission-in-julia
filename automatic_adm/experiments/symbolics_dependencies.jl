
using Symbolics

output = Vector{Float64}(undef, 9);
input = Vector{Float64}(undef, 9);
# note: julia has a convection that outputs arguments come before input arguments
Symbolics.jacobian_sparsity((one, two) -> target(two, one), output, input) # ERROR: MethodError: no method matching Float64(::Num)



function symtest(output, input)
  output[:] = input
  SSPRK3!(output)
  return nothing
end



output = Vector{Float64}(undef, 3);
input = Vector{Float64}(undef, 3);

# use this function to test if the jacobian sparsity pattern neglects zeros, as I want
function foo(z, x, y)
  z .= x .* y
  return nothing
end

# when y is zero, the resulting jacobian should still be structurally correct
function bar(z, x)
  y = zeros(eltype(x), size(x))
  foo(z, x, y)
  return nothing
end

Symbolics.jacobian_sparsity(bar, output, input)
# => since y is constant zero, the sparsity turns out to be zero too!
# i.e. wrapping a function with some arguments left constant will result in 

# can this be avoided by randomizing some arguments?
# also try: min(0, rand argument > 0) -> is this dependency still detected?

## tentative workaround: init other vector arguments (note: since all vectors!) to undef !
# this way we capture the dependencies for sure!
# lastly, try then a z = maxelement(y) * x to see if this gives a dense jacobian

# only consider active/tracked vars to set to undef -> consts can be left const


# consider bar() as wrapper of foo; now baz is a wrapper that does not assume y constant (could be =0 in call to function, killing dependencies)
function baz(z, x)
  y = ones(eltype(x), size(x)) # cannot init to Vector{eltype(x)}(undef, length(x)) since undef is left there! (jacobian sparsity inits undef to whatever)
  foo(z, x, y)
  return nothing
end

Symbolics.jacobian_sparsity(baz, output, input)

# now test a function that goes to zero if y = 1 !
function foo2(z,x,y)
  z .= x .* (y .- 1)
  return nothing
end

function bar2(z, x)
  y = ones(eltype(x), size(x))
  foo2(z, x, y)
  return nothing
end

Symbolics.jacobian_sparsity(bar2, output, input)
# => results in zeros!
# ===> the other arguments, that are wrapped away, will always be non-arbitrary in the sense that they may result in
# auslöschung or zeroing that yields an erroneous sparsity pattern!
# BUT: we can pass additional arguments ot jacobian_sparsity() that will be passed on to the function!
# => wrapper just needs to rearrange the arguments

# NOTE: unmodified / pure input detection:
# if the sparsity patterns between all other args to one input (declared the output in jacobian_sparsity) is zero, then the one input is a candidate (!!) for a pure input
# note: could also be modified without other influences: f(x,y) = {x .*= 3; y .= 2 .* x;} -> x is not pure input, since it is modified!
# ===> still need a not modified detection!
# if x does not depend on other args then any modification must occur always (since no control flow!) hence a simple '==' check suffices to determine whether it is a pure input.
# further note: control flow could be handled via AST in julia or also the trace check for iterate!() etc. functions, but probably better in a compiler pass context! A general tool should be build upon a compiler architecture/framework such as LLVM.



### Test of a function containing minimum(vec). Required behavior: full Jacobian, showing possible influence from any vec val to the result.

function foo3(z,x,y)
  z .= x .* minimum(y)
  return nothing
end

output = Vector{Float64}(undef, 3);
input = Vector{Float64}(undef, 3);
extra = Vector{Float64}(undef, 3);
Symbolics.jacobian_sparsity(foo3, output, input, extra) # does not work

@variables output[1:3], input[1:3], extra[1:3]
# test whether the minimum in a side-argument deletes dependencies
Symbolics.jacobian_sparsity(foo3, output, input, extra) # this works! -> gives 3x3 identity! => accounts for all possible influences of y via minimum on the result z
# now test if the minimum on an input variable causes issues
Symbolics.jacobian_sparsity((z, x, y)->foo3(z, y, x), output, input, extra) # works! gives dense 3x3 pattern => all y can influence all z!

# lastly check if the pattern of an output taken as input and the input as output is zero (no back dependency) | here: how does x depend on z? (should not depend)
Symbolics.jacobian_sparsity((x, z)->foo3(z, x, ones(3)), output, input) # issue: this gives ones in the last column



### test use of in-out variables in functions
function foo4(z, y)
  z .= z .* y
  return nothing
end
# sparsity of output z w.r.t. input y
Symbolics.jacobian_sparsity(foo4, output, input) # error! -> zero pattern
# sparsity of input y w.r.t. output z
Symbolics.jacobian_sparsity((y, z)->foo4(z, y), output, input) # error: gives last column pattern again
# sparsity of output z w.r.t. output z
#Symbolics.jacobian_sparsity((z)->foo4(z, input), output) # error, code execution does not work


# attempt a wrapper for in-out variables | keeps input unmodified by copying it to a separate variable that then is modified
function wrapper4(z_out, z_in, y)
  #z_out .= z_in # this crashes z_out on y and furthermore yields wrong results for z_out on z_in!!!
  z_out[:] = z_in # this seems to work fine!
  foo4(z_out, y)
  return nothing
end
# compute how z_out depends on y
Symbolics.jacobian_sparsity((z_out, y, z_in)->wrapper4(z_out, z_in, y), output, input, extra) # using [:] gives identity, which is correct
# compute how z_out depends on z_in
Symbolics.jacobian_sparsity(wrapper4, output, input, extra) # using [:] gives identity, which is correct
# compute if the input depends on the output
Symbolics.jacobian_sparsity((y, z_out, z_in)->wrapper4(z_out, z_in, y), output, input, extra) # last column issue for both y and z_in as tests


# try foo3 again, but with [:] | result: not better, but worse...
function foo5(z,x,y)
  z[:] = x .* minimum(y) # using just y crashes even for the normal dependency test...
  return nothing
end
Symbolics.jacobian_sparsity(foo5, output, input, extra) # works as before
Symbolics.jacobian_sparsity((z, x, y)->foo5(z, y, x), output, input, extra) # this now crashes!!! (worked before...)
Symbolics.jacobian_sparsity((x, z)->foo5(z, x, ones(3)), output, input) # still gives the wacky last colum result
# it does not make sense, that normal works, but swapping x and y crashes | i.e. now the input is taken the minimum of



#=
current issues:
1) Dependency on itself requires a wrapper to work. The wrapper needs assignment with [:]= (instead of .=), else it crashes.
2) Cannot compute correct zero pattern for output influence on input. It always gives a weird last column=1 pattern.
3) swapping input arguments in functions with [:]= assignment crashes (see foo5)
=#


# think about: let's try a operator overloading attempt at the dependency analysis
# => Float becomes a type that internally stores 1) its name/id 2) a dependency list
# input and output vectors get seeded with the custom type and names (e.g. x[1], x[2]...)
# after execution, in all arguments check the dependency lists.
# construct sparsity patterns for each argument combination as in & output (also reverse direction -> two variables could influence each other)

#= further reading:
https://docs.sciml.ai/Symbolics/stable/
https://docs.sciml.ai/Symbolics/stable/manual/sparsity_detection/
https://docs.sciml.ai/Symbolics/stable/tutorials/auto_parallel/ # automated sparse parallelism via converting julia function to symbolic function
=#
