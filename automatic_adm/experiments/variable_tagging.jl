

include("../module/tracing.jl")


#=
An integral component of the taping mechanism (generating a DAG from a trace) is the tracking of variables.
A correspondence between the arguments of the calling function and all arguments of all
called functions in the function body needs to be established to track the flow of data.
Hence, a method to track variables is required.
In other words: We want to know for an argument in a function call in the trace,
where that exact argument (not its numerical value) appeared before preceding function calls.

Several methods accomplish this.
Here, we test two options:
1) '===' checks if two objects are the same object, in contrast to '==', which checks value equality, i.e. if two potentially different objects are the same.
   Advantages are its simplicity. Disadvantages are it only works for container objects, since for a simple Float64 == is equivalent to === (numeric/bit equality).
   The method to establish the status of an encountered variable is to search a list of known variable references checking for '===' equality.

2) Tags: Cassette allows to tag values with respect to a context. This (hopefully) enables the tracking and identification of variables.

Note that this following/tracking of variables is similar to adjoint taping in the sense that every adjoint variable
has a id in the adjoint tape. Here, since we do not use operator overloading (which would be an option) to track variables,
the tape needs to store pointers/references to the adjoints contained within. This is accomplished with a list of references and checking ===.
Or alternatively, the values can be tagged, which would be more like the variable holding the id in the tape, just as with operator overloading.
=> Difference in approach philosophies is what holds the connection: Does the variable refer to itself in the tape,
or does the tape refer to the variable outside of the tape.

Here, both approaches are tested. In particular it is checked if the tagging approach can replace the surefire === approach.
=#




# simple in place element-wise square function
function h(y, x)
  y[:] = x .* x # equivalent: y .= x .* x
  return nothing
end

# passes its arguments directly to h(y, x)
function foo(y, x)
  h(y, x)
  return nothing
end

# obfuscates the passing of x to h by only passing a copy
function bar(y, x)
  z = copy(x)
  h(y, z)
  return nothing
end

#=
We require the following behavior:
foo: the arguments passed to h should be identified as the same ones as passed to foo.
bar: although x == z numerically, the first argument of h (i.e. z) should not be identified as x.

It can be seen when using ===, that in the second check the copy() results in (intended) disassociation w.r.t. '==='.
=> For vectors, since they are containers, === can tell whether two things are really the exact same object.
=#


##### Testing ===

x = rand(2)
y = zeros(2)
trace = trace_function(3, foo, (y, x))
# check the references in the traces of foo and h
println("foo:")
println(x == trace.current[1].first[3] ==  trace.current[1].second[1].first[3]) # true
println(     trace.current[1].first[3] === trace.current[1].second[1].first[3]) # true
# check the deepcopy arguments in the traces of foo and h
println(x == trace.current_deepcopy[1].first[2] ==  trace.current_deepcopy[1].second[1].first[2]) # true
println(     trace.current_deepcopy[1].first[2] === trace.current_deepcopy[1].second[1].first[2]) # false | since we took a deep copy (twice even)

# do the same for bar | note second[2] is now h, since [1] is copy
y = zeros(2)
trace = trace_function(3, bar, (y, x))
println("bar:")
println(x == trace.current[1].first[3] ==  trace.current[1].second[2].first[3]) # true
println(     trace.current[1].first[3] === trace.current[1].second[2].first[3]) # false | this is the vial difference!
println(x == trace.current_deepcopy[1].first[2] ==  trace.current_deepcopy[1].second[2].first[2]) # true
println(     trace.current_deepcopy[1].first[2] === trace.current_deepcopy[1].second[2].first[2]) # false (since deep copies again)


##### Testing tagging
println("\nTag testing:")

x = rand(2); y = zeros(2)

trace = Trace(3)
ctxNoTag = TraceCtx(metadata = trace)
ctx = Cassette.enabletagging(ctxNoTag, foo)
println("Context now has tagging enabled: ",  Cassette.hastagging(typeof(ctx)))

mutable struct Tag
  name::String
  # todo: maybe add an id for complete system

  Tag(str) = new(str)
end

# allow all values (::DataType) in the Trace context to be tagged with metadata=Tag
Cassette.metadatatype(::Type{<:TraceCtx}, ::DataType) = Tag


# tag the values: given them metadata with respect to a context | note: no metadata= keyword argument, as the docs suggest
x = Cassette.tag(x, ctx, Tag("x")) # returns a tagged value
y = Cassette.tag(y, ctx, Tag("y"))

# retrieve the metadata of a tag
println(Cassette.metadata(x, ctx).name)

args = (y, x)
enter!(trace, foo, args...)
Cassette.overdub(ctx, foo, args...)
exit!(trace)

# might want to use prehook and posthook instead of directly calling enter! and exit!
# although this should not matter, since we here directly set the functions (no additional shenanigans are performed)
# Cassette.prehook(ctx::TraceCtx, args...) = enter!(ctx.metadata, args...)
# Cassette.posthook(ctx::TraceCtx, args...) = exit!(ctx.metadata)



#= Fazit:
I can use tags, and also remove them in trace to make the trace appear as it was without tags.
Now the next step is to use the tags for stuff!
It seems that the tags can be used to do what I want.

Therefore, let's first do the dependency analysis.
Then write a taping mechanism that works while tracing and makes use of the tags.
=> on enter store args & tags, on exit determine dependencies and what arguments the function modified -> deps analysis
Use the tag system to create a DAG.
=> DAG struct that is part of the trace

Note: dep analysis could run function to see if modified, or take pre and post args state from the trace.

Although, we kinda want to do taping in prehook, since then for nested taping we step forward and add to the tape.
=====> call dependency analysis in prehook. deps(f, args) -> run again to check const args
Then directly do taping.

NOTE NOTE: do not need to rely on trace! Do everything in a new DAG metadata structure that does DAG generation without the trace!
...
...
think a bit about how to do the taping with a nested trace!
Then decide on taping procedure in pre or posthook and if to trace or only dag metadata etc.




SIDE NOTE:
I think we only need to tape the lowest level, i.e. when the trace second is empty (by it though max depth, or lowest level before that!).
The levels above are just recursion layers of passing shit downwards!
=> i.e. the leaf nodes of the trace are what is really executed
--
Hence, we can also trim the trace if wanted: if a function is fast to exec we can stay on the higher level and treat that as elementary function.
So we can truncate / prune the call tree / trace to be reasonable.
Preprocessor pruning: if the benchmark of the higher level function is only a fraction of the total runtime
Postprocessor pruning: if an entire section is just evaluated in pure tangent or adjoint, the above function can just be evaluated and the ADM overhead can be eliminated.
Further note: maybe only a subset of functions is evaluated in one mode
---> MAKE NOTE: very far future: e.g. f -> {g, h, j} => want f_hat = {wrapper, j} where wrapper is a function that wrapps (g, h), both evaluated with e.g. tangent after each other!
AGAIN make a note in Obsidian: wrappers may be more costly, best would be source code injection (could be done with Cassette), but best in LLVM.
Also, in general the ADM model could be done via injection of the AD functions -> build a new ADM version of the function.
Cassette can do this as far as I know, but this is a lot of extra effort.
ONLY ATTEMPT THIS IF THE OVERHEAD IS LARGE AND THERE IS HOPE OF BEING BETTER THAN PURE ENZYME
=> else: in note, link to a note of stuff for report (random text note) -> further extensions: source code injection/transformation -> """" reinforces the need for a compiler plugin level tool """""



=======> seems doch better to split trace and taping. For simplicity oder so...
(first collect all data, then process it)





!!!
We could to the taping from the trace, since the trace can be used to re-execute the function.
But, we can just borrow/use the pre-existing recursive structure of the tracing to do the taping.
This works well, since taping is also in the same order as the trace runs (at leas for non-nested problems / one trace level).
=#


