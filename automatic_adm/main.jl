
#=
All benchmarks have been moved to individual benchmark files in ./benchmarks
List of benchmarks:
* chunksize.jl compares the dependence of the cost of various jacobian computation methods on the chunksize / vector mode vector size
* exex_annotation.jl computes the annotations of both (dt const and non-const) test case functions and computes the theoretical cost of my varying dt manual schedule
* jacobian_vs_enzyme.jl computes a table of runtimes for some jacobian functions to establish that the overhead is reasonable
* primal.jl  compare primal with primal+preallocation of temporaries. Additionally compare with retrace time
* runtime_varying_dt.jl runtime over input size N for recipes (and my jacobian functions) for the varying dt test case
* runtime.jl runtime over N for dt=const
* smaller_benchs.jl comparison of the real and theoretical ratios between the greedy min fill and branch and bound runtimes (gauge overhead of recipe by looking at the difference in the ratios)


Execution of the benchmark files can be done with the exec_benchmark.sh shell script.
This pipes the console out into a extra file. The cluster can do this anyway in the batch job, but this is nice for local execution.
The runtime_varying_dt.jl has an extra script that splits up the different runs on different cpu cores. Uncomment the command line argument line in the jl file for this.

Plotting in performed in ./plots



This mainfile does some small computations for the report:
* Calculates the error in the Jacobian when using compression on the varying-dt case
* shock tube allocation optimization (see if allocations are a huge time-hit; not that relevant for our case, since what the function does below is kinda irrelevant)
* some schedules that are important

=#


include("module/shock_tube.jl")
include("module/recipe.jl")


######################## compressed dt!=const jacobian error ########################


# determine the error in the jacobian at t=0.2

u = zeros(3*128)
set_initial_conditions!(u)
plotloop(u, 0, 0.2)
w = deepcopy(u)

# compute the banded jacobian of the dt!=const case using the dt=const sparsity pattern
acc = CAccumulate(SSPRK3!, deepcopy(u), Forward)
approx_jacobian = acc(deepcopy(u), SSPRK3_dt!)
true_jacobian = jacobian(Forward, SSPRK3_dt!, (u,), Val(1), Val(1), Val(32)) 
println("u unmodified: ", w == u)


#### compute and print errors ####

froebenius_abs_diff = norm(true_jacobian - approx_jacobian, 2)
spectral_abs_diff = opnorm(true_jacobian - approx_jacobian, 2)
println("Frobenius-Norm absolute error ||J - J_true|| = ", froebenius_abs_diff)
println("Spectral-Norm absolute error ||J - J_true|| = ", spectral_abs_diff)
println("Frobenius-Norm relative error ||J - J_true|| / ||J_true|| = ", froebenius_abs_diff / norm(true_jacobian, 2))
println("Spectral-Norm relative error ||J - J_true|| / ||J_true|| = ", spectral_abs_diff / opnorm(true_jacobian, 2))


#### element-wise maximum relative error ####

max_abs_diff = maximum(abs.(true_jacobian - approx_jacobian))
println("Maximum absolute element-wise error: ", max_abs_diff)

function print_max_rel(tol)
  exclude_infs(val, ref) = if ref > tol; return abs.((val - ref)/ref); else return 0.0; end;
  max_rel_diff = maximum(exclude_infs.(approx_jacobian, true_jacobian))
  println("Maximum element-wise relative error (relative w.r.t. true jacobian values) of elements larger than ", tol, ": ", max_rel_diff)
  return nothing
end
print_max_rel(1e-12)
print_max_rel(1e-6)
print_max_rel(1e-3)
print_max_rel(1e-1)


#= Results

Frobenius-Norm absolute error ||J - J_true|| = 0.28618994144764637
Spectral-Norm absolute error ||J - J_true|| = 0.23132317665178645
Frobenius-Norm relative error ||J - J_true|| / ||J_true|| = 0.019218556637279317
Spectral-Norm relative error ||J - J_true|| / ||J_true|| = 0.19748943347306364
Maximum absolute element-wise error: 0.05962924294525383
Maximum element-wise relative error (relative w.r.t. true jacobian values) of elements larger than 1.0e-12: 632.2357546924512
Maximum element-wise relative error (relative w.r.t. true jacobian values) of elements larger than 1.0e-6: 237.05864992764648
Maximum element-wise relative error (relative w.r.t. true jacobian values) of elements larger than 0.001: 19.526566461422664
Maximum element-wise relative error (relative w.r.t. true jacobian values) of elements larger than 0.1: 0.1419240686072044

=> small, but noticable Frobenius error, but large spectral error.
The real effect on the solution of optimization and inverse problems would need to be further classified to see if this error matters.
Needles to say, if an accurate method exists that has the same cost, its better to choose that.

=#










######################## RK3 allocation optimization ########################


u = zeros(3*128); #set_initial_conditions!(u) # leave zero, no re-init needed then for all outer variables (else k1,... need re-zeroing in benchmarking)

k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
y1, y2 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u))




function flux_modified(U)
  rho, rho_v, rho_E = U
    v = rho_v / rho
    return (rho_v, 2/3*(rho_v*v + rho_E), (5/3*rho_E - 1/3*rho_v*v)*v) # tuple instead of dynamic vector
  end

function lax_friedrichs_modified(U_left, U_right)
  λ_max = max(max_wavespeed(U_left), max_wavespeed(U_right)) # max wave speed estimate
  return 0.5 .* ((flux_modified(U_right) .+ flux_modified(U_left)) .- λ_max .* (U_right .- U_left))
end

  # get the rhs (i.e. the balance of fluxes)
function rhs_modified!(y, dy)
  N::Int = Int(length(y)/3)
  dx::Float64 = 1/N

  # for all inner interfaces, calculate fluxes and add to rhs
  for i=1:N-1
    left_idx = i*3-2 #1+(i-1)*3
    right_idx = left_idx+3
    flux = lax_friedrichs_modified(y[left_idx:left_idx+2], y[right_idx:right_idx+2])
    dy[left_idx:left_idx+2]   -= flux/dx
    dy[right_idx:right_idx+2] += flux/dx
  end
  # boundary fluxes
  dy[1:3] += lax_friedrichs_modified(U_left_cond, y[1:3])/dx
  dy[end-2:end] -= lax_friedrichs_modified(y[end-2:end], U_right_cond)/dx
  return nothing
end


function foo!(u, k1, k2, k3, y1, y2)
  rhs_modified!(u, k1)
  helper1!(y1, u, k1)
  rhs_modified!(y1, k2)
  helper2!(y2, u, k1, k2)
  rhs_modified!(y2, k3)
  helper3!(u, k1, k2, k3)
  return nothing
end




SSPRK3!(u); SSPRK3_prealloc!(u, k1, k2, k3, y1, y2); foo!(u, k1, k2, k3, y1, y2); # compile

@time SSPRK3!(u)                              # allocations: 504.766 KiB
@time SSPRK3_prealloc!(u, k1, k2, k3, y1, y2) # allocations: 489.141 KiB
@time foo!(u, k1, k2, k3, y1, y2)             # allocations: 307.734 KiB

using BenchmarkTools
@benchmark SSPRK3!(u)                              # 142.404 μs
@benchmark SSPRK3_prealloc!(u, k1, k2, k3, y1, y2) # 137.955 μs
@benchmark foo!(u, k1, k2, k3, y1, y2)             # 88.701 μs

#= Result
No effort was made to make the inner function optimized.
The use of dynamic vectors (opposed to e.g. tuples) results in quite a lot of allocations, which slows down the function.
For our purposes this should be irrelevant, since we do not care what the inner function does,
the differentiation scales with the runtime and thats is all we always have.
Quite the opposite: to short of a primal may cause benchmarking timing inaccuracy issues.

Extrapolation of no-allocation runtime:
504.766 - 489.141 = 15.625; 142.404 - 137.955 = 4.449; 4.449/15.625 = 0.284736 μs/kilo-allocation
489.141 - 307.734 = 181.407; 137.955 - 88.701 = 49.254; 49.254/181.407 =0.2715 μs/kilo-allocation
=> 0.28 * 307.734 = 86.16552 μs; 88.701 - 86.16552 = 2.53548 μs; 142.404 / 2.53548 = 56.16451322826447
=> It would probably be possible to reduce the runtime by a factor 56 by optimizing the code.

This has some important implications for the jacobian accumulation code:
If the accumulation cost inside the jacobian routines (seeding and harvesting vectors) dominates the primal runtime, this can skew the results quite a bit.
=#









######################## some schedules (not used here, but in some benchmarks) ########################


#=
The Branch and Bound is done via depth first search. It seems that it gets lost in a very deep branch.
Multithreading splits of at the beginning. One of the other staring branches gets to the adjoint dt accumulation branch very quickly.
Although it seems to lack an initial accumulation and the adjoint reverse accumulation only comes later. The cause is currently unresolved.
(Schedule processing has been amended to do all accumulations first)
=#
schedule=
"ELI TAN (0 1 3) 1539456
ELI TAN (0 1 5) 2184576
ACC TAN (0 2) 1797674880
ELI TAN (0 2 3) 70369920
ELI TAN (0 2 5) 82192128
ELI TAN (0 2 7) 125111808
ACC TAN (0 3) 70704768
ELI TAN (0 3 4) 1831230720
ELI TAN (0 4 5) 82581504
ACC TAN (0 5) 82993920
ELI TAN (0 5 6) 1888210560
ELI TAN (0 4 7) 125013504
ELI TAN (0 6 7) 123663360
ACC TAN (0 7) 116592000
ACC ADJ (0 1) 38346
ACC TAN (1 7) 6947
ELI MUL (0 1 7) 147456"


#=
Preserve sparsity as much as possible by computing ∂u_out/∂u and ∂u_out/∂Δt then at the end compute du_out/du = ∂u_out/∂u + ∂u_out/∂Δt * ∂Δt/∂u.
This allows the computation of ∂u_out/∂u to be sparse and ∂u_out/∂Δt is only a single column, which is very efficient in tangent mode.
Furthermore, the time step contribution gets incremented on at the end, causing Julia to drop all numeric zeros in the resulting jacobian.
In this way, the resulting recipe remains fully sparse and retains most of the sparse fixed time step performance.
=#
delay_dt_schedule=
"ACC TAN (1 3)
 ACC TAN (1 4)
 ACC TAN (1 6)
 ACC TAN (1 8)
 ACC ADJ (1 2)
 ACC TAN (2 4)
 ACC TAN (2 6)
 ACC TAN (2 8)
 ELI TAN (1 3 8)
 ELI TAN (1 3 4)
 ELI TAN (1 3 6)
 ELI TAN (1 4 5)
 ELI TAN (1 5 6)
 ELI TAN (1 5 8)
 ELI TAN (1 6 7)
 ELI TAN (1 7 8)
 ELI TAN (2 4 5)
 ELI TAN (2 5 6)
 ELI TAN (2 5 8)
 ELI TAN (2 6 7)
 ELI TAN (2 7 8)
 ELI MUL (1 2 8)"
# decrement all node indices, since we expect zero-start (admission output is zero indexed)
temp = collect(delay_dt_schedule) # convert to mutable vector
for i in eachindex(temp)
  if isdigit(temp[i])
    temp[i] -= 1
  end
end
delay_dt_schedule = String(temp)


#config = ADmConfig(mode="both", depth=2, bench_params=BenchmarkParams(1e7, 100), elim_method="BranchAndBound", chunk=16)
#dense_recipe, sparse_recipe = adm_generate(SSPRK3_dt!, (u,), config, schedule=schedule)
#dense_recipe2, sparse_recipe2 = adm_generate(SSPRK3_dt!, (u,), config, schedule=delay_dt_schedule)




