
include("taping.jl") # dag
include("jacobian.jl")
using BenchmarkTools

# container for information of a edge in an annotated DAG
struct AnnotationEdge
  conn::Pair # connectivity: from source => to target
  cost::Pair # AD evaluation cost: tangent/forward => adjoint/reverse
end

# stores information of a dag needed for the ADM planner input xml file
mutable struct Annotation
  node_sizes::Vector{Int} # every node as an index=id=i and a vector size
  edges::Vector{AnnotationEdge} # edges in the annotated dag (only annotation info, discard other edge information from DAG)

  Annotation() = new([], [])
end


struct BenchmarkParams # see https://juliaci.github.io/BenchmarkTools.jl/stable/manual/#Benchmark-Parameters
  samples::Int # the maximum number of samples
  seconds::Float64 # "The number of seconds budgeted for the benchmarking process."  
end




# benchmarks an edge to obtain tangent and adjoint AD model evaluation costs
function benchmark_edge(edge::Edge, fma_per_ns_reference::Float64, params::BenchmarkParams)
  # perturb the input a bit to preclude any strange repeated-run optimizations on the input vector
  #init(x) = x + 1e-7 * abs.(rand(eltype(x), length(x)))
  # functions should be complex enough and not compile on the actual values, so omit this to make edge use easier (else copy of edge etc.)

  # the dollar sight means to interpolate the local variable into the @benchmark scope | note: the time is in nanoseconds (ns)
  forward_time = median(@benchmark edge_jacobian(Forward, $edge, Val(1)) samples=params.samples seconds=params.seconds).time
  reverse_time = median(@benchmark edge_jacobian(Reverse, $edge, Val(1)) samples=params.samples seconds=params.seconds).time 
  
  # convert the timings into a fma estimate using the MVM model (conversion factor passed to this function)
  tangent_cost = Int(round(forward_time * fma_per_ns_reference / length(edge.args[edge.arg_ind.first]))) # fma per eval = time/full eval * fma/time * full eval/eval
  adjoint_cost = Int(round(reverse_time * fma_per_ns_reference / length(edge.args[edge.arg_ind.second]))) # output length <=> rows in jacobian

  return tangent_cost => adjoint_cost
end


# update a const conversion factor dict with a new factor (rows, cols) -> factor
function cost_conversion(edge::Edge, dict, params)
  # to convert the runtime to #FMA used for the annotation a reference factor [fma/ns] is introduced
  # to make it more specific (hopefully increase the accuracy / applicability of the fma estimate) the function is treated as a MVM product
  # benchmark a MVM with a random matrix with size matching the edge size
  
  # note: rows/cols does not matter here (same est for forward and reverse) | but also: always go from first->second (reverse is treated correctly for mismatched sizes!)
  rows = length(edge.args[edge.arg_ind.second]) # output size
  cols = length(edge.args[edge.arg_ind.first]) # input size

  if !haskey(dict, (rows, cols)) # only create a new factor if we don't already have computed one
    fma = rows * cols
    mat = rand(rows, cols)
    vec = rand(cols)
    mvm(m, v) = m*v
    time = median(@benchmark $mvm($mat,$vec) setup=(mat=rand($rows, $cols); vec = rand($cols)) samples=params.samples seconds=params.seconds).time
    dict[(rows, cols)] = fma / time # fma operations per nanosecond -> multiply with time in ns to get number of fma estimate though this MVM model
  end

  return dict[(rows, cols)]
end



function annotate_dag(dag::DAG, params::BenchmarkParams=BenchmarkParams(1e8, 60)) # default samples very large -> let the time budget determine the benchmark runs
  annotation = Annotation()
  # copy over vertex sizes (bzw. a reference to it)
  annotation.node_sizes = dag.vertex_sizes
  # track cost conversion factors in this dictionary
  costs = Dict()

  # iterate edges, store connectivity and benchmark the edge
  for edge in dag.edges
    # determine a reference time for the conversion of runtime to #FMA used for the annotation
    push!(annotation.edges, AnnotationEdge(edge.conn, benchmark_edge(edge, cost_conversion(edge, costs, params), params)))
  end

  return annotation#, costs
end





# outputs an annotation into xml readable by the ADMission planner
function to_xml(annotation::Annotation, filename::String)
  io = open(filename, "w") # opens writable file and clears the file contents

  # write header
  write(io, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
  write(io, "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">\n")
  write(io, "  <key id=\"has_jacobian\" for=\"edge\" attr.name=\"has_jacobian\" attr.type=\"boolean\" />\n")
  write(io, "  <key id=\"adjoint_cost\" for=\"edge\" attr.name=\"adjoint_cost\" attr.type=\"long\" />\n")
  write(io, "  <key id=\"tangent_cost\" for=\"edge\" attr.name=\"tangent_cost\" attr.type=\"long\" />\n")
  write(io, "  <key id=\"has_model\" for=\"edge\" attr.name=\"has_model\" attr.type=\"boolean\" />\n")
  write(io, "  <key id=\"index\" for=\"node\" attr.name=\"index\" attr.type=\"long\" />\n")
  write(io, "  <key id=\"size\" for=\"node\" attr.name=\"size\" attr.type=\"long\" />\n")
  write(io, "  <key id=\"size\" for=\"node\" attr.name=\"size\" attr.type=\"long\" />\n")
  write(io, "  <graph id=\"G\" edgedefault=\"directed\" parse.nodeids=\"free\" parse.edgeids=\"canonical\" parse.order=\"nodesfirst\">\n")

  # write the nodes (i.e. their sizes)
  for i in eachindex(annotation.node_sizes)
    write(io, "  <node id=\"$i\">\n")
    write(io, "    <data key=\"index\">$i</data>\n")
    write(io, "    <data key=\"size\">$(annotation.node_sizes[i])</data>\n")
    write(io, "  </node>\n")
  end

  write(io, "\n")

  # write edges with associated AD model costs
  for i in eachindex(annotation.edges)
    source, target = annotation.edges[i].conn
    tangent, adjoint = annotation.edges[i].cost
    write(io, "  <edge id=\"$i\" source=\"$source\" target=\"$target\">\n";)
    write(io, "    <data key=\"has_jacobian\">0</data>\n")
    write(io, "    <data key=\"adjoint_cost\">$adjoint</data>\n")
    write(io, "    <data key=\"tangent_cost\">$tangent</data>\n")
    write(io, "    <data key=\"has_model\">1</data>\n")
    write(io, "  </edge>\n")
  end

  # write footer
  write(io, "  </graph>\n")
  write(io, "</graphml>\n")

  close(io)
  return nothing
end




# stores the execution schedule of an ADMission plan (output of admission call)
mutable struct Schedule
  operations::Vector{String}
  modes::Vector{String}
  targets::Vector{Vector{Int}}
  Schedule() = new([], [], [])
end


function admission_str_to_rows(str::String)
  rows = split(str, "\n")

  start_idx = findfirst((s)->(occursin("(operation mode target cost):", s)), rows) + 1
  end_idx   = findfirst((s)->(occursin("dense tangent cost:", s)), rows) - 2
  return rows[start_idx:end_idx]
end

function parse_schedule(rows)
  schedule = Schedule()

  # resort schedule to do all accumulation steps first (for some reason, sometimes this is necessary)
  acc = []; eli = []
  for row in rows; if occursin("ACC", row); push!(acc, row); else; push!(eli, row); end; end
  rows = vcat(acc, eli)

  for row in rows
    cols1, cols2 = split(row, "(")
    cols1 = split(strip(cols1), " ")
    push!(schedule.operations, cols1[1])
    push!(schedule.modes, cols1[2])
    push!(schedule.targets, parse.(Int, split(split(cols2, ")")[1], " ")) .+ 1)
  end

  return schedule
end


function admission(annotation::Annotation; method="GreedyMinFill", temp_dir=false)
  base_name = "annotated_dag"
  if temp_dir
    path = mktempdir() # random dir /tmp/jl_<random chars> that is automatically deleted when julia process exits
  else
    path = "./temp/" # a fixed temp directory (so i can see the results) | better: generate a random unused temp dir and delete afterwards
  end

  # write annotation to file
  dag_filename = path * base_name * ".xml"
  to_xml(annotation, dag_filename)

  # write input file
  in_filename = path * base_name*".in"
  io = open(in_filename, "w")
  write(io, "dag $dag_filename\n")
  write(io, "method $method\n")
  close(io)

  # call shell admission and get result
  str = read(`admission $in_filename`, String)

  # write the result into a schedule
  return parse_schedule(admission_str_to_rows(str))
end







########################### NOTES

# ON VECTOR MODE AND CONVERSION OF NS WALL TIME TO INTEGER FMA COST ESTIMATE
# 4) note: bench with vector mode =1, all improvements should scale equally (although MMM relatively cheaper then... -> real data is a future must have)
# 5) conversion of nanoseconds time to an integer time used for the adm planner -> MMM nxnxn = n^3 fma => benchmark MMM (use input size?) -> conversion factor
#    alternative: treat edge eval as matrix*vector -> benchmark MVM => n_in x n_out fma -> time that bench !!!! (I think this may make more sense)
#    => for every edge bench a reference value (note here: since all same size, this is kinda witzlos, but naja) ---> create a dict (n,m)->factor (for consistency)
#    Note with text: 'For any combination jacobian sizes (rows and cols) we compute a reference factor to use' (and store in a dict)
# ===> use MVM to model the fma cost of the AD evaluation -> cost estimates in line with the ADM planner MMM internal cost model



# The benchmark is configured with a large amount of samples (to make the time dominate) -> set a time budget for the benchmarks
# future work: better would be an additional accuracy budget (e.g. benchmark to a certain certainty/guarantee/accuracy estimate of the runtime/benchmark result)



# sparsity treatment exacerbates/increases the need for more accurate timings / cost models (!! use this word: cost model)





#= in some cases the adjoint benchmarks to be faster than the tangent mode!

The annotation (all sizes 384)
AnnotationEdge(1 => 2, 5510731 => 15522810)
AnnotationEdge(1 => 3, 180686 => 198314)
AnnotationEdge(2 => 3, 185751 => 217006)
AnnotationEdge(3 => 4, 5234632 => 15071566)
AnnotationEdge(1 => 5, 224533 => 227765)
AnnotationEdge(2 => 5, 221577 => 233125)
AnnotationEdge(4 => 5, 253341 => 245503)
AnnotationEdge(5 => 6, 5285954 => 15404059)
AnnotationEdge(1 => 7, 333773 => 330826)
AnnotationEdge(2 => 7, 372506 => 406440)
AnnotationEdge(4 => 7, 436083 => 523761)
AnnotationEdge(6 => 7, 529041 => 577527)

gives with the GreedyMinFill heuristic

elimination sequence
  (operation mode target cost):
  ACC TAN (4 5) 2029806336 # accumulate edge v5->v6 => dv6/dv5
  ELI TAN (4 5 6) 203151744 # evaluate v6->v7 with dv6/dv5 as input => dv7/dv5
  ELI ADJ (0 4 6) 87461760 # adjoint edge 1=>5 with the jacobian dv7/dv5 as seeding!
  ACC ADJ (0 6) 127037184
  ACC TAN (0 1) 2116120704
  ELI TAN (0 1 6) 143042304
  ELI TAN (0 1 2) 71328384
  ELI TAN (0 1 4) 85085568
  ELI MUL (0 4 6) 56623104 # =384^3 simple MMM cost estimate
  ELI ADJ (3 4 6) 94273152
  ACC TAN (0 2) 69383424
  ELI TAN (0 2 3) 2010098688
  ELI TAN (0 3 6) 167455872
  ELI MUL (0 3 6) 56623104

dense tangent cost: 7207145472
dense adjoint cost: 18800141568
optimized cost: 7317491328
227765
note: the indices are zero-based (admission just ignores the xml vertex and edge ids....) -> need to shift back to 1-indexed
253341*384 = 97282944 # cost of 4 => 5
2029806336 ACC TAN (4 5)
2029806336/384 = 5285954 # this is tangent cost of 5=>6

note: optimized cost worse than tangent (greedy chose differently since was better locally at the beginning of the heuristic procedure)

note: this contains accumulation in tan and adj and also elimination in tan and adj modes and furthermore multiplication steps ---> really good test, I think!



==========================================================
##### elimination sequence explained: (i shifted indices by +1)
(also look at the dag plot for this)
ACC TAN (5 6)   # accumulate edge v5->v6 => dv6/dv5
ELI TAN (5 6 7) # evaluate tangent edge v6=>v7 with dv6/dv5 as input => dv7/dv5 (contribution from path 5=>6=>7, which is the full dv7/d5)
ELI ADJ (1 5 7) # evaluate adjoint edge v1=>v5 with the jacobian dv7/dv5 as seeding! =>  dv7/dv1 (along path 1=>5=>6=>7)
ACC ADJ (1 7)   # accumulate dv7/dv1 (and add to the existing dv7/dv1) -> NOTE: jacobian function modification: accumulation (pass in jacobian & modify)
ACC TAN (1 2)   # accumulate dv2/dv1
ELI TAN (1 2 7) # use dv2/dv1 as seed to evaluate edge 2=>7 to increment onto dv7/dv1
ELI TAN (1 2 3) # use dv2/dv1 as seed to evaluate edge 2=>3 to increment onto dv3/dv1 (which was zero before)
ELI TAN (1 2 5) # use dv2/dv1 as seed to evaluate edge 2=>5 to increment onto dv5/dv1 (which was zero before)
ELI MUL (1 5 7) # multiply dv7/dv5 (from step 2) with dv5/dv1 from last step onto dv7/dv1 += dv7/dv5*dv5/dv1
ELI ADJ (4 5 7) # use dv7/dv5 as seeding to evaluate edge 4=>5 to increment dv7/dv4 (which was zero before)
ACC TAN (1 3)   # accumulate dv3/dv1 (+= accumulation, since dv3/dv1 was written to before)
ELI TAN (1 3 4) # use dv3/dv1 as seed to evaluate edge 3=>4 to increment dv4/dv1 (which was zero before)
ELI TAN (1 4 7) # use dv4/dv1 as seed to evaluate edge 4=>7 to increment dv7/dv1
ELI MUL (1 4 7) # multiply dv7/dv1 += dv7/dv4 * dv4/dv1 (this accounts for the dependency of v7 on v4 over vertex v5)

=> list of all required Jacobians -> init first to zero (will need to do this somewhere anyway, since only low level can skip zero init, my funcs its difficult)
=> call incremental jacobian model for elimination steps but also accumulation (it would zero init internally anyway)
=> every step in the schedule calls jacobian with either a identity seed (ACC) or other jacobian as seed (ELI)
=> OR: if ELI, then MUL is possible, which instead of calling jacobian just multiplies two jacobians together
=> for every step store its operands (seeding matrix (if required), jacobian to increment, or two jacobians to multiply) -> jacobians get id's

==========================================================
For report: Can represent these computational steps as a DAG again | maybe do this
==========================================================




note: the first and second step could be done together (reduce ADM overhead by concatenating those steps)
      -> compiler tool can then remove the intermediate vertex v6 from its code dag representation to speed things up
      (Similarly for v2, I think)


==========================================================
sparsity considerations:
the seed may be compressed, such that the result is a compressed jacobian, which needs to be incremented onto a dense jacobian.
(side note: simplification: do not transfer one compression to another, always convert to dense first | possible performance gains here)
Hence, the jacobian function takes the sparsity patter, performs seed compression, executes AD and decompresses the seed during incrementing.
Alternatively, just write a wrapper that takes dense seed and patter, compresses, runs jacobian on a compressed jacobian then decompresses.
This approach necessitates the allocation of a temporary compresses jacobian and is therefore suboptimal.
But its faster to implement, so do that!

text note: overall many "could be done" -> goal of this work is to guide future development, in part by providing possible avenues of exploration / performance gains
==========================================================
=#




# note: some steps of the sequence (such as accumulations) are interchangeable -> ordering should optimize parallelism and primal eval efficiency
# -> ties into the primal evaluation on the side for the jacobian (as we need the primal function values to call the edges with)


# side note: regarding the note with linked jacobians due to same function that induces these edges: compiler could pull apart or keep cached better maybe


