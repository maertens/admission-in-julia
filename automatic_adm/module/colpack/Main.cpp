


/* README
This is a stripped down and modified version of the ColPack main file. (The ColPack licence can be found in this directory)
An executable is obtained by replacing ColPack/Examples/ColPackAll/Main.cpp with this file.
After make install the executable can then be added to the user path to allow Julia code to call it.
This was deemed simpler calling ColPack library calls from Julia.
Note that the best method seems to be making locally in  ColPack/Examples/ColPackAll/Main.cpp.
!!! remove $(OMP_FLAG) from CCFLAGS (but keep in linker flags), else coloring races to suboptimal results.

Opposed to the original main file, this file is only for Jacobian row or column bi-coloring.
All possible orderings/heuristics are executed and the best one is chosen.
The resulting seed matrix is printed to console.

To get the seed matrix the following example file was used as a reference:
https://eecs.wsu.edu/~assefaw/ColPackDoxygen/html/d3/dbc/_column__compression__and__recovery__for___jacobian__return___row___compressed___format_8cpp_source.html

New call signature is ColPack <fname> <method>
where <fname> is .mtx matrix file name and <method> is either COLUMN_PARTIAL_DISTANCE_TWO or ROW_PARTIAL_DISTANCE_TWO
*/



#include "ColPackHeaders.h"
#include <cstring>
#include <unordered_set>
#include <iostream>

using namespace ColPack;
void partial_coloring(int argc, char* argv[]);

const unordered_set<string> PARTIAL_COLORING({
        "COLUMN_PARTIAL_DISTANCE_TWO",
        "ROW_PARTIAL_DISTANCE_TWO"});

// see Table 3 at https://cscapes.cs.purdue.edu/coloringpage/software.htm
const std::vector<std::string> ORDERINGS = {"NATURAL", "LARGEST_FIRST", "SMALLEST_LAST", "INCIDENCE_DEGREE"};


int main(int argc, char* argv[]) {
  string fname (argv[1]);
  string method (argv[2]);
  if (!PARTIAL_COLORING.count(method)) { std::cout << "ERROR: method " << method << " not supported\n"; return 1; }

  /* find the ordering with the least colors (best compression of Jacobian) */
  int min = -1; std::string min_o;
  BipartiteGraphPartialColoringInterface *g = new BipartiteGraphPartialColoringInterface(0, fname.c_str(), "AUTO_DETECTED");
  for(std::string const& ordering : ORDERINGS) {
    g->PartialDistanceTwoColoring(ordering.c_str(), method.c_str());
    int count = g->GetVertexColorCount();
    if(count < min || min<0) { min = count; min_o=ordering; }
  } // end for orderings

  /* recompute minimal color ordering and print seed matrix */
  int *seed_n_rows = new int;
  int *seed_n_cols = new int;
  double*** seed = new double**;

  g->PartialDistanceTwoColoring(min_o.c_str(), method.c_str());
  (*seed) = g->GetSeedMatrix(seed_n_rows, seed_n_cols);

  std::cout << "Best ordering (" << min_o << ") seed matrix:\n";
  std::cout << *seed_n_rows << " " << *seed_n_cols << "\n";
  for(int i=0; i < *seed_n_rows; i++) {
    for(int j=0; j < *seed_n_cols; j++) {
      std::cout << (*seed)[i][j] << " ";
    }
    std::cout << "\n";
  }

  std::vector<int> vertex_partial_colors;
  g->GetVertexPartialColors(vertex_partial_colors);
  std::cout << "Color Groups:\n";
  std::cout << g->GetVertexColorCount() << " " << vertex_partial_colors.size() << "\n";
  for(size_t i = 0; i < vertex_partial_colors.size(); i++) {
    std::cout << vertex_partial_colors[i] << " ";
  } std::cout << "\n";


  delete seed; seed = nullptr;
  delete seed_n_rows; seed_n_rows = nullptr;
  delete seed_n_cols; seed_n_cols = nullptr;

  delete g;

  return 0;
}
