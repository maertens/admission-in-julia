
using SparseArrays



# wraps a Real type to track dependencies between in and outputs of functions
mutable struct Tracker{T <: Real}
  # keep the value (not needed for dependency tracking) for completeness and to check results with primal run
  val::T
  # 'name' of this variable (-1 if non/temporary/untracked variable)
  #  | required to avoid self-dependence (e.g. differentiate x = x.*x, from identity dependence (no modification))
  id::Int64
  # dependencies of this variable
  deps::Vector{Int64}

  Tracker(value) = new{typeof(value)}(value, -1, Int64[])
  Tracker(value, id) = new{typeof(value)}(value, id, Int64[])
end

# vector creation overload for the tracker | given value and m create tracker vector initialized with that value
function Vector{Tracker{T}}(value::T, m::Integer) where {T}
  vec = Vector{Tracker{T}}(undef, m)
  for i=1:m
    vec[i] = Tracker(value)
  end
  return vec
end


# return a tracked version (copy) of a vector with given values and ids
function track_vector(values::Vector{T}, ids::Vector{Int}) where {T}
  if length(values) != length(ids); error("track_vector input length missmatch"); end
  m = length(values)
  vec = Vector{Tracker{T}}(undef, m)
  for i=1:m; vec[i] = Tracker(values[i], ids[i]); end
  return vec
end
# track_vector, but accepts a unit range
track_vector(values::Vector{T}, ids::UnitRange{Int}) where {T} = track_vector(values, collect(ids))

# no id version of track_vector
function track_vector(values::Vector{T}) where {T}
  m = length(values)
  vec = Vector{Tracker{T}}(undef, m)
  for i=1:m; vec[i] = Tracker(values[i]); end
  return vec
end

# return a tracked set of arguments (all vectors)
track_args(args...) = track_vector.(args)


function get_values(tracked::Vector{Tracker{T}}) where {T}
  m = length(tracked)
  vec = Vector{T}(undef, m)
  for i=1:m; vec[i] = tracked[i].val; end
  return vec
end 


# overloads for other vector creation functions
Base.zeros(::Type{Tracker{T}}, m::Integer) where {T} = Vector{Tracker{T}}(T(0), m)
Base.ones(::Type{Tracker{T}}, m::Integer) where {T} = Vector{Tracker{T}}(T(1), m)
Base.rand(::Type{Tracker{T}}, m::Integer) where {T} = track_vector(rand(T, m))







# seeds/sets ids of Tracker elements of vectors sequentially, starting from id=0
# returns the seeding id vectors of each argument in a tuple
function seed_trackers(args...)
  id = 0
  seedings = ntuple((i)->Vector{Int}(undef, length(args[i])), length(args)) # Int[] for each argument with length of the args length
  for (arg, seeding) in zip(args, seedings)
    for i in eachindex(arg) # cannot zip, since seed is primitive and will not reference-modify on assign
      seeding[i] = id
      arg[i].id = id
      id += 1
    end
  end
  return seedings
end


# empty! passthrough of tracker to the dependencies
empty!(tracker::Tracker{T}) where {T} = empty!(tracker.deps)



#=
In: a function and a tuple of calling arguments
All arguments should be vectors and the function should not have an explicit return.
Out: dependency matrix, sparsity patterns for all argument dependencies as a dictionary of (i_arg_in, j_arg_out) => pattern
the matrix and patterns are of type compressed sparse column (CSC) sparse matrix with Bool values and Int index type,
i.e. SparseArrays.SparseMatrixCSC{Bool, Int64}

assumptions:
 - all arguments are vectors
 - the vector arguments retain their size (same size before and after function call)

dependency matrix: gives dependencies between the vector arguments before and after the function call as Boolean matrix
  each row corresponds to an output argument, while each column is an input argument (similar to a jacobian matrix)
   -> i.e. row i, col j gives: output arg i depends on input arg j
   -> a row tells you what inputs one output, corresponding to the row, depends on
   -> a col tells you what outputs are influenced by / depend on this input
  gives bool whether a dependence exists between the argument as input and the argument after the function call (as output)
  a pure output variable does not depend on itself (diagonal entry zero), but on other arguments
  a pure input (i.e. const / unmodified) variable depends on no argument (i.e. zero row corresponding to that argument)

sparsity patterns:
  gives element-wise dependencies between a pair of arguments (one is input other is output)
  a variable/argument can depend on itself with a pattern
  two arguments can depend on each other differently
  The dependency sparsity pattern corresponds to the jacobian sparsity pattern
=#
function analyze_dependencies(func, args)
  #rintln("Analyzing ", string(func), " with arguments ", args)
  n_args = length(args)
  dep_matrix = spzeros(Bool, n_args, n_args)
  patterns = Dict()

  targs = track_args(args...) # track all arguments
  seeds = seed_trackers(targs...) # uniquely seed the trackers
  func(targs...) # compute the target function with dependency tracking enabled

  # extract the pattern information between all argument combinations
  for i_arg=1:n_args # output arg = row of dependency matrix
    for j_arg=1:n_args # input arg = column
      n, m = length(args[i_arg]), length(args[j_arg]) 
      pattern = spzeros(Bool, n, m)
      input_seeds = seeds[j_arg]

      # for all vector elements of output and input args, check for dependency
      for j=1:m # all cols/inputs
        for i=1:n # all rows/outputs
          # check if the i-th element of the output contains the j-th input as a dependency
          if input_seeds[j] in targs[i_arg][i].deps
            pattern[i,j] = true
          end
        end
      end

      # for all vector elements of output and input args, check for assignment (!!) dependency
      # an exception to the assignment dependency check is the case of self-dependency, since this check would induce non-existent/trivial self-dependency
      if i_arg != j_arg; for j=1:m; for i=1:n
        # check if the i-th element of the output has the id of the j-th input (assignment dependency)
        if input_seeds[j] == targs[i_arg][i].id; pattern[i,j] = true; end
      end; end; end

      # if there exists dependency (i.e. nonzero sparsity pattern) then take note of that and store the pattern
      if count(==(true), pattern) > 0
        dep_matrix[i_arg, j_arg] = true
        patterns[(i_arg, j_arg)] = pattern
      end
    end
  end
  return dep_matrix, patterns
end




#=
In: a function and a tuple of calling arguments
All arguments should be vectors and the function should not have an explicit return.
Out: dependency matrix, sparsity patterns for all argument dependencies as a dictionary of (i_arg_in, j_arg_out) => pattern
the matrix and patterns are of type compressed sparse column (CSC) sparse matrix with Bool values and Int index type,
i.e. SparseArrays.SparseMatrixCSC{Bool, Int64}

assumptions:
 - all arguments are vectors
 - the vector arguments retain their size (same size before and after function call)

dependency matrix: gives dependencies between the vector arguments before and after the function call as Boolean matrix
  each row corresponds to an output argument, while each column is an input argument (similar to a jacobian matrix)
   -> i.e. row i, col j gives: output arg i depends on input arg j
   -> a row tells you what inputs one output, corresponding to the row, depends on
   -> a col tells you what outputs are influenced by / depend on this input
  gives bool whether a dependence exists between the argument as input and the argument after the function call (as output)
  a pure output variable does not depend on itself (diagonal entry zero), but on other arguments
  a pure input (i.e. const / unmodified) variable depends on no argument (i.e. zero row corresponding to that argument)

sparsity patterns:
  gives element-wise dependencies between a pair of arguments (one is input other is output)
  a variable/argument can depend on itself with a pattern
  two arguments can depend on each other differently
  The dependency sparsity pattern corresponds to the jacobian sparsity pattern
=#

######################### overload operators and functions for the Tracker type #########################

#= NOTE:
Julia does not allow the assignment operator to be overloaded! (wow...)
Hence, all other operators need to be designed to return what the assign should be.
This is the natural way anyway. There is not much i want to do on assignment here, I think.

This influences the behavior of vector .= and [:], which assign on element level.
The dependency analysis function is updated to reflect this:
  to circumvent this, it should be checked if the outputs still have the same id's (if overwritten then id=-1, but can also have id of another input!)
note: scalars (z,x)->(z=x) will induce no dependency, since z becomes x (===).
More tricky to deal with, but also not that problematic, since further modification will induce the desired dependency list.


function Base.:=(lhs::Tracker{T}, rhs::Tracker{T}) where {T}
  lhs.val = rhs.val
  empty!(deps) # loose all dependencies on overwrite
  depends!(lhs, rhs)
  return lhs
end
=#



### Many base functions should behave in the same way internally.
### Thus we define short hand helper functions that implement the core tracking behavior of operations.


# establish dependency between a dependant and dependencies | the dependant's dependencies are not cleared  (which is fine, since its likely a new Tracker)
function depends!(dependant::Tracker{T}, dependencies::Union{Tracker{T}, Real}...) where {T}
  for dep in dependencies
    if typeof(dep) == Tracker{T} # ignore reals in the dependency tracking
      if dep.id != -1; push!(dependant.deps, dep.id); end # if the dependency is actively tracked, note the dependency on it
      append!(dependant.deps, dep.deps) # depend on the dependencies of the dependency
      unique!(dependant.deps) # memory footprint becomes an issue fast, this alleviates this severely
    end
  end
  return nothing
end

# get the value of a Tracker or Real
value(x::Tracker{T}) where {T} = x.val
value(x::Real) = x

# standard template for tracking dependencies for an operation
function optrack(op, operands::Union{Tracker{T}, Real}...) where {T}
   # extract values out of trackers and call op with them to save into val of new result Tracker
  res = Tracker(op(ntuple((i)->(value(operands[i])), length(operands))...))
  depends!(res, operands...)
  return res
end

# NOTE: to allow also interaction with general Real type, functions value() and depends!()
# could be implemented to treat general reals as reals. and then used in the above tracked operation function.
# => is done above!

#################### Base Arithmetic Operators ####################

Base.:+(lhs::Tracker{T}, rhs::Tracker{T}) where {T} = optrack(Base.:+, lhs, rhs)
Base.:-(lhs::Tracker{T}, rhs::Tracker{T}) where {T} = optrack(Base.:-, lhs, rhs)
Base.:-(rhs::Tracker{T}) where {T}                  = optrack(Base.:-, rhs)
Base.:*(lhs::Tracker{T}, rhs::Tracker{T}) where {T} = optrack(Base.:*, lhs, rhs)
Base.:/(lhs::Tracker{T}, rhs::Tracker{T}) where {T} = optrack(Base.:/, lhs, rhs)
Base.:^(lhs::Tracker{T}, rhs::Tracker{T}) where {T} = optrack(Base.:^, lhs, rhs)

# operations with Real values (e.g. Int or Float64)
Base.:+(lhs::Real, rhs::Tracker{T}) where {T} = optrack(Base.:+, lhs, rhs)
Base.:+(lhs::Tracker{T}, rhs::Real) where {T} = optrack(Base.:+, lhs, rhs)
Base.:-(lhs::Real, rhs::Tracker{T}) where {T} = optrack(Base.:-, lhs, rhs)
Base.:-(lhs::Tracker{T}, rhs::Real) where {T} = optrack(Base.:-, lhs, rhs)
Base.:*(lhs::Real, rhs::Tracker{T}) where {T} = optrack(Base.:*, lhs, rhs)
Base.:*(lhs::Tracker{T}, rhs::Real) where {T} = optrack(Base.:*, lhs, rhs)
Base.:/(lhs::Tracker{T}, rhs::Real) where {T} = optrack(Base.:/, lhs, rhs)
Base.:/(lhs::Real, rhs::Tracker{T}) where {T} = optrack(Base.:/, lhs, rhs)


# multiplication of vector time scalar both trackers
function Base.:*(lhs::Vector{Tracker{T}}, rhs::Tracker{T}) where {T}
  res = Vector{Tracker{T}}(undef, length(lhs))
  for i in eachindex(res); res[i] = lhs[i]*rhs; end # use tracker*tracker multiplication
  return res
end
Base.:*(lhs::Tracker{T}, rhs::Vector{Tracker{T}}) where {T} = rhs*lhs # commutative



#################### Base Comparison Operators ####################
# nothing to do here currently.
# in advanced code, the output bool depends on both inputs.
# Since the output is bool, underlying type interaction should be accounted for.



#################### Broadcasting ####################
# https://docs.julialang.org/en/v1/manual/interfaces/#man-interfaces-broadcasting

# broadcasting scalar*vector of trackers should treat the Tracker as a scalar
Base.Broadcast.broadcastable(x::Tracker{T}) where {T} = Base.RefValue{Tracker{T}}(x)


#Base.BroadcastStyle(::Type{Vector{Tracker{T}}}) where {T} = Broadcast.ArrayStyle{Vector{Tracker{T}}}()
#Base.BroadcastStyle(::Type{Tracker{T}}) where {T} = Broadcast.Style{Tracker{T}}()



#################### Additional functions ####################

# depend on all elements, not only the minimum element, since we want the general possible dependencies
function Base.minimum(vec::Vector{Tracker{T}}) where {T}
  res = Tracker(minimum(get_values(vec)))
  for i in eachindex(vec)
    depends!(res, vec[i])
  end
  return res
end

Base.abs(val::Tracker{T}) where {T} = optrack(Base.abs, val)
Base.sqrt(val::Tracker{T}) where {T} = optrack(Base.sqrt, val)
Base.max(lhs::Tracker{T}, rhs::Tracker{T}...) where {T} = optrack(Base.max, lhs, rhs...)

# max with Real, used for flux across boundaries (fixed bc state is Float64)
# cannot do union on all, since plot(spy()) will call max on reals, which then for some reason picks this...
Base.max(lhs::Tracker{T}, rhs::Union{Tracker{T}, Real}...) where {T} = optrack(Base.max, lhs, rhs...) # first arg tracker, others all
Base.max(lhs::Union{Tracker{T}, Real}, rhs::Tracker{T}...) where {T} = optrack(Base.max, lhs, rhs...) # first arg all, others tracker

Base.adjoint(val::Tracker{Float64}) = val



