

include("annotation.jl")
include("recipe_retrace.jl")

using LinearAlgebra # for I


# dummy functions for now to sketch out design
function multiply!(lhs, rhs1, rhs2)
  lhs .+= rhs1 * rhs2
  return nothing
end
@inline eliminate!(::ForwardMode, jacobian, seed, edge, chunk) = edge_jacobian!(Forward, edge, jacobian, seed, Val(chunk))
@inline eliminate!(::ReverseMode, jacobian, seed, edge, chunk) = edge_jacobian!(Reverse, edge, jacobian, seed, Val(chunk))


mutable struct Recipe
  func::Any # the function associated with this recipe
  dag::DAG # dag that is connected to this recipe (e.g. if one wants to access the edges / change the arguments) (not necessarily needed here)
  schedule::Vector{Any} # list of functions corresponding to the individual steps of the schedule
  jacobians::Vector{Matrix} # list of all intermediate jacobians | final jacobian is at [1]

  retrace::ReTrace # retrace object used to update primal arguments in dag

  Recipe(f, dag, trace) = new(f, dag, [], [], ReTrace(dag, trace))
end


# returns value of key if exists, else registers in dict, creates a new jacobian and returns index of that
function register_jacobian(jac_dict, key, recipe)
  # create a new entry in dict and jacobian vector if the key/jacobian is not known
  if !haskey(jac_dict, key)
    n_rows = recipe.dag.vertex_sizes[key[2]] # output/to/target vertex size
    n_cols = recipe.dag.vertex_sizes[key[1]] # input/from/source vertex size
    push!(recipe.jacobians, zeros(n_rows, n_cols))
    jac_dict[key] = length(recipe.jacobians)
  end
  return jac_dict[key]
end


#=NOTE: unnecessary, since for ELI MUL the target is always (i k j), with two final jacobians =# 
# helper to find intermediate k from multiplication
function find_k(jac_dict, target)
  for kidx=2:length(target)-1
    k = target[kidx]
    if haskey(jac_dict, (k, target[end])) && haskey(jac_dict, (target[1], k))
      return k
    end
  end
  return -1 # produces error
end




function create_recipe(f, dag::DAG, schedule::Schedule, trace::Trace, chunk::Int)
  recipe = Recipe(f, dag, trace)

  # maps two indices (i, j) to the index in the jacobian list corresponding to dvj / dvi
  # e.g. ACC TAN (5 6) leads to dv6/dv5 which is in some arbitrary position in the jacobians vector
  jac_dict = Dict()
  # register the overall goal/final jacobian
  register_jacobian(jac_dict, (1, dag.n_vertices), recipe) # jacobian from first to last vertex

  # get the edge index in the dag from its connecting nodes
  edge_dict = Dict()
  for i in eachindex(dag.edges); edge_dict[(dag.edges[i].conn.first, dag.edges[i].conn.second)] = i; end;


  # for all entries in the schedule 1) determine needed jacobians and 2) create a function
  for (op, mode, target) in zip(schedule.operations, schedule.modes, schedule.targets)

    # the target is the path along which the jacobian is taken
    # therefore, the first and last entry in the target vector correspond to the 'in' and 'out' indices of the jacobian
    i = target[1]; j = target[end] # (i, j) key gives dvj/dvi jacobian
    J_idx = register_jacobian(jac_dict, (i, j), recipe) # register the jacobian or retrieve its index

    if op == "ELI"
      if mode == "TAN"
        # tangent adds the last edge in the target list to the path
        seed_idx = jac_dict[(i, target[end-1])] # jacobian of path before last edge is seed
        edge_idx = edge_dict[(target[end-1], j)]
        push!(recipe.schedule, ()->eliminate!(Forward, recipe.jacobians[J_idx], recipe.jacobians[seed_idx], recipe.dag.edges[edge_idx], chunk))

      elseif mode == "ADJ"
        # adjoint adds the first edge in the target list to the path
        seed_idx = jac_dict[(target[2], j)]
        edge_idx = edge_dict[(i, target[2])]
        push!(recipe.schedule, ()->eliminate!(Reverse, recipe.jacobians[J_idx], recipe.jacobians[seed_idx], recipe.dag.edges[edge_idx], chunk))

      else # mode == "MUL"
        # we need to find the dividing index k, for which we then multiply dvj/dvi += dvj/dvk * dvk/dvi
        # this means k such that (i, k) and (k, j) exist (assume no ambiguity in k / multiple possibilities)
        k = find_k(jac_dict, target)
        rhs1_idx = jac_dict[(k, j)] # dvj/dvk
        rhs2_idx = jac_dict[(i, k)]
        push!(recipe.schedule, ()->multiply!(recipe.jacobians[J_idx], recipe.jacobians[rhs1_idx], recipe.jacobians[rhs2_idx]))
      end

    else # op == "ACC"
      # accumulation can only have a single edge target, thus the elimination is simple
      # here, the identity seed is explicitly created and passed to the elimination method
      edge_idx = edge_dict[(i, j)]
      if mode == "TAN"
        n_cols = length(dag.edges[edge_idx].args[dag.edges[edge_idx].arg_ind.first]) # source dimension
        identity =  Matrix(1.0I, n_cols, n_cols)
        push!(recipe.schedule, ()->eliminate!(Forward, recipe.jacobians[J_idx], identity, recipe.dag.edges[edge_idx], chunk))
      else # mode == "ADJ"
        n_rows = length(dag.edges[edge_idx].args[dag.edges[edge_idx].arg_ind.second]) # target dimension
        identity =  Matrix(1.0I, n_rows, n_rows)
        push!(recipe.schedule, ()->eliminate!(Reverse, recipe.jacobians[J_idx], identity, recipe.dag.edges[edge_idx], chunk))
      end
    end # end if op
  end # end for

  #println(jac_dict)
  #println(edge_dict)

  return recipe
end



# compute the jacobian from a recipe via the AD mission plan stored inside | this is the main function used!
function adm_jacobian(recipe)
  # zero the jacobian matrices in the recipe
  for jac in recipe.jacobians; jac .= 0; end;
  # execute all steps in the schedule
  for func in recipe.schedule; func(); end
  return recipe.jacobians[1] # NOTE: returns 'reference' to recipe jacobian
end






# include sparse recipe stuff here to define joint functions below
include("sparsity.jl")



# collector of config options for adm generation
struct ADmConfig
  chunk::Int # maximum chunk size for vector forward and reverse mode
  depth::Int # depth of trace to generate dag
  mode::String # {dense, sparse, both} what type of recipe to create
  elim_method::String # {GreedyMinFill, BranchAndBound} admission elimination method/heuristic
  print_schedule::Bool # whether to print admission result to console
  bench_params::BenchmarkParams # annotation benchmark config
  temp_dir::Bool # whether to alloc temp dir at /tmp or at ./temp
  function ADmConfig(;chunk=16,
                      depth=2,
                      mode="dense",
                      elim_method="GreedyMinFill",
                      print_schedule=true,
                      bench_params=BenchmarkParams(1000, 10),
                      temp_dir=false)
    return new(chunk, depth, mode, elim_method, print_schedule, bench_params, temp_dir)
  end
end


# generate a recipe for the ADM jacobian accumulation for a given function and arguments
function adm_generate(func, args, config::ADmConfig=adm_config(); schedule=nothing)
  @info "Tracing function"
  trace = trace_function(config.depth, func, deepcopy(args)) # copy args to avoid modification outside of this function

  @info "Taping trace to dag"
  dag = tape(trace)

  if isnothing(schedule)
    # factor *2 since every edge may need to take a MVM benchmark for cost factor
    @info "Annotating dag, this will take < $(Int(2 * ceil(config.bench_params.seconds * length(dag.edges)))) seconds + compile time"
    annotation = annotate_dag(dag, config.bench_params) # fixed chunk=1 for cost comparability with MVM & MMM | hopefully reasonable
    @info "AD mission planing"
    schedule = admission(annotation, method=config.elim_method, temp_dir=config.temp_dir)
  elseif schedule isa String
    schedule = parse_schedule(split(schedule, "\n"))
  end
  
  if config.print_schedule
    println("AD mission planning schedule: ")
    println("op   mode  target")
    for (op, mode, target) in zip(schedule.operations, schedule.modes, schedule.targets)
      println("$op  $mode   $target")
    end
  end

  @info "Generating $(config.mode) recipe(s)"
  if config.mode=="dense"; return create_recipe(func, dag, schedule, trace, config.chunk)
  elseif config.mode=="sparse"; return create_sparse_recipe(func, dag, schedule, trace, config.chunk, config.temp_dir)
  else # both
  return create_recipe(func, dag, schedule, trace, config.chunk), create_sparse_recipe(func, dag, schedule, trace, config.chunk, config.temp_dir)
  end
end






######### high level functions for jacobian+primal evaluation


function adm_jacobian_with_primal!(recipe, args)
  # execute primal and update arguments for recipe
  primal_with_trace!(recipe.retrace, recipe.func, args)
  # compute jacobian with ADM elimination
  return adm_jacobian(recipe)
end

#= FINAL WRAPPER
Want to be able to use a function with the signature jacobian(func, args...).
We can then pass this function to methods that need a jacobian calculation! => seamless use
For this purpose, we make the recipe struct callable as function.

Note that this function returns a reference to the jacobian of the recipe (same as the other jacobian function with the recipe).
Thus, reevaluation of this function will alter outside jacobians if they are not copied!
=#
function (recipe::Union{Recipe, SparseRecipe})(func, args...)
  if recipe.func != func; @error "ADM Jacobian recipe called on wrong function!";end
  return adm_jacobian_with_primal!(recipe, args)
end

function (recipe::Union{Recipe, SparseRecipe})(arg)
  return adm_jacobian_with_primal!(recipe, (arg,))
end


# ==================================== NOTES ==================================== #
#=
1) text: the schedule is augmented (use that word) with information (list stuff: dag, jacobian storage) to create a recipe struct (oder so)

2) i do not care whether a seed is only used once, it is copied into the jacobian function (done in the jacobian function)

3) !! Vastly inefficient recipe execution, since all the access (e.g. dictionary lookups) are done in the function calls
   -> assume this is high-level and negligible and gets amortized away

4) see notes in recipe_retrace.jl design considerations

5) seamless use is facilitated with calling the recipe -> useable as a jacobian function


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
6) some jacobians are final in a step (e.g. ELI MUL always has two final jacobians as input)
   => bzw. more generally: when a jacobian is used in ADM, it is final (no further changes)

7) one could detect whether a jacobian is used after a given step and then not copy-seed, but overwrite seed it
   but since we tuple-ize the matrix-valued seed for Enzyme, it is always copy anyways. 
   (implementation would require to store the seeds as vectors of vectors to pass the object vec references into autodiff via the tuple oder so)
8) furthermore, if seeding is of a unmodified / pure input argument, it also does not need to be copied
  -> bunch of smaller/micro optimizations
  NOTE: check if these cases even exist (if i want to mention them)


9) Branch and bound with multiple threads for variable dt yields a elimination as first step, but we require an accumulation so we have something to eliminate with.
   This seems to be an issue with the admission utility. For now, just do all the accumulation steps first by storing them in a separate function.


=#

