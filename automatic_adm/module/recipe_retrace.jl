
#= Some notes:
To allow accumulation of a jacobian we need to execute at least part of the primal to get function arguments to the edges.
Optimally, this would be done during the elimination procedure. The cost of computing the primal arguments should then be factored in.
Note that during th edge elimination, the first pass of an AD model could then also compute the primal result of that edge.
Note that this makes the elimination search space larger / more complex.

In our case, we assume the cost of the primal to be insignificant compared to the jacobian elimination.
Hence we perform a primal run to record the required elemental/edge arguments.
This assumption is not guaranteed in the sparse case, where the jacobian accumulation time is of the same asymptotic order as the primal,
and optimally is ony a small constant factor more expensive. Then accounting for primal execution in the ADM scheduling/planing may yield significant benefits.

The recipe accesses the arguments through the dag edges. Note that these get their arguments from the trace. Since they are stored in a pair in the trace,
they cannot be modified (overwriting the pair changes object identity in trace, but not in recipe).
See e.g. with
trace.current_deepcopy[1].second[16].first
recipe.dag.edges[1].args

Writing the arguments from an updated trace to the dag edges would require traversing the nested trace structure (as done as pre-step in taping).
On the other hand, the dag does not know the full picture (skips e.g. zero() functions), hence running the primal out of context is a bit tricky.

Design: In a nested structure, store the edges of the dag to write the arguments to. Then trace with this structure as metadata and write args into dag when needed.

Every edge stores its input arguments. Since a function can induce multiple edges, we may need to write the updated args to multiple edges.
=#





using Cassette

# include("taping.jl") <- included by the parent recipe.jl that includes this


# writes arguments into dag edges when used to overdub (the correct) function
mutable struct ReTrace
  # list of corresponding edges in dag for all functions that are called during tracing
  edge_indices::Vector{Vector{Int}}
  # iterator to keep track of position in edge_indices (current function) during nested cassette overdub
  iter::Int

  # for overdub depth control
  max_depth::Int
  current_depth::Int

  dag::DAG # (reference to the) dag to write edge arguments into

  # generate a ReTrace from an existing trace and associated DAG
  function ReTrace(dag, trace)
    max_depth = trace.max_depth
    edge_indices = []
    retrace_trace(dag, trace.current, trace.current_deepcopy, edge_indices)
    return new(edge_indices, 0, max_depth, 0, dag)
  end
end


# flatten the nested trace structure into edge_indices with lists of indices of induced edges in dag for each function
function retrace_trace(dag, trace_current, trace_current_copy, edge_indices)
  for ((element, children), (args_copy, copy_children)) in zip(trace_current, trace_current_copy)

    # construct edge list from the dag
    edge_list = Int[]
    for i_edge in eachindex(dag.edges)
      edge = dag.edges[i_edge]
      # an edge belongs is induced by a function if the function and its input arguments are the same
      # this assumes no function is called with the same inputs twice (why should it, if inputs the same -> same result)
      # (could also check === here, I think)
      if (edge.func == element[1]) && (edge.args == args_copy)
        push!(edge_list, i_edge)
      end
    end
    push!(edge_indices, edge_list)

    if !isempty(children)
      retrace_trace(dag, children, copy_children, edge_indices)
    end
  end
  return nothing
end





######### Cassette tracing mechanism, see trace.jl for details

Cassette.@context ReTraceCtx

function enter!(t::ReTrace, args...)
  t.current_depth += 1
  
  t.iter += 1
  for idx in t.edge_indices[t.iter]
    t.dag.edges[idx].args = deepcopy(args[2:end])
  end

  return nothing
end

function exit!(t::ReTrace)
  t.current_depth -= 1
  return nothing
end

Cassette.prehook(ctx::ReTraceCtx, args...) = enter!(ctx.metadata, args...)
Cassette.posthook(ctx::ReTraceCtx, args...) = exit!(ctx.metadata)

function Cassette.overdub(ctx::ReTraceCtx, args...)
  if (ctx.metadata.current_depth < ctx.metadata.max_depth) && Cassette.canrecurse(ctx, args...)
    Cassette.recurse(ctx, args...)
  else
    Cassette.fallback(ctx, args...)
  end
end



# executes the primal and updates dag edge arguments
function primal_with_trace!(trace::ReTrace, func::F, args::X) where{F, X}
  trace.iter = 0; trace.current_depth=0 # current depth setting not needed in working code, but if something breaks, this often causes after-issues
  enter!(trace, func, args...)
  Cassette.overdub(ReTraceCtx(metadata = trace), func, args...)
  exit!(trace)
end


