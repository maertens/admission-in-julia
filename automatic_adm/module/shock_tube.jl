
using Plots
using LinearAlgebra


# for now set discretization parameters dt as constant # domain is [0, 1]
const dt::Float64 = 0.00001#0.001
const gamma::Float64 = 5/3


function pressure(rho, rho_v, rho_E)
  v = rho_v / rho
  return (gamma-1)*(rho_E - 0.5*rho_v*v)
end

function inverse_eos(rho, v, p)
  rho_E = p/(gamma-1) + 0.5*rho*v*v
  return rho_E
end


const U_left_cond::Vector{Float64} = [1.0, 0.0, inverse_eos(1.0, 0.0, 1.0)]
const U_right_cond::Vector{Float64} = [0.125, 0.0, inverse_eos(0.125, 0.0, 0.1)]




# set initial conditions to the boundary conditions split in the middle
function set_initial_conditions!(u)
  N = Int(length(u)/3)
  midpoint = Int(floor(N/2))*3
  u[1:3:midpoint] .= U_left_cond[1]
  u[3:3:midpoint] .= U_left_cond[3]
  u[midpoint+1:3:end] .= U_right_cond[1]
  u[midpoint+3:3:end] .= U_right_cond[3]
  u[2:3:end] .= 0 # momentum
  return nothing
end


function flux(U)
rho, rho_v, rho_E = U
  v = rho_v / rho
  #return [rho_v, 2/3*(rho_v*v + rho_E), (5/3*rho_E - 1/3*rho_v*v)*v]
  p = pressure(rho, rho_v, rho_E)
  return [rho_v, rho_v*v + p, (rho_E + p)*v]
end

function max_wavespeed(U)
  rho, rho_v, rho_E = U
  v = abs(rho_v / rho)
  c = sqrt(gamma * pressure(rho, rho_v, rho_E) / rho)
  return v + c # max wave speed estimate
end


function lax_friedrichs(U_left, U_right)
  # classical lax-friedrichs for FVM
  #return 0.5*(flux(U_right) + flux(U_left)) - dx/(2*dt)*(U_right - U_left)
  # Local Lax-Friedrichs (Rusanov) (as in Trixi)
  λ_max = max(max_wavespeed(U_left), max_wavespeed(U_right)) # max wave speed estimate
  return 0.5*(flux(U_right) + flux(U_left)) - 0.5*λ_max*(U_right - U_left)
end



# get the rhs (i.e. the balance of fluxes)
function rhs!(y, dy)
  N::Int = Int(length(y)/3)
  dx::Float64 = 1/N

  # for all inner interfaces, calculate fluxes and add to rhs
  for i=1:N-1
    left_idx = i*3-2 #1+(i-1)*3
    right_idx = left_idx+3
    U_left = y[left_idx:left_idx+2]
    U_right = y[right_idx:right_idx+2]
    flux = lax_friedrichs(U_left, U_right)
    dy[left_idx:left_idx+2]   -= flux/dx
    dy[right_idx:right_idx+2] += flux/dx
  end
  # boundary fluxes
  dy[1:3] += lax_friedrichs(U_left_cond, y[1:3])/dx
  dy[end-2:end] -= lax_friedrichs(y[end-2:end], U_right_cond)/dx
  return nothing
end



function plotsol(u)
  dx::Float64 = 1/Int(length(u)/3)
  rho = u[1:3:end]
  v = u[2:3:end] ./ rho
  p = pressure.(rho, u[2:3:end], u[3:3:end])
  Plots.default(fontfamily = ("computer modern"))
  plot(dx/2:dx:1-dx/2, [rho, v, p], label=["Density" "Velocity" "Pressure"], show=true, xlabel="x", ylabel="solution magnitude", title="Sod shock tube solution quantities", size=(900, 450))
  # savefig("plots_ouptut.png")
end

# alternatively write to file with
#=
using DelimitedFiles
dx = 1/Int(length(u)/3)
rho = u[1:3:end]
v = u[2:3:end] ./ rho
p = pressure.(rho, u[2:3:end], u[3:3:end])
writedlm("plots/shock_tube_output_t02.tsv", hcat(collect(dx/2:dx:1-dx/2), rho, v, p))
=#


# one time step, du should be initialized to zero
function euler_step!(u, du)
  rhs!(u, du)
  u[:] += dt*du[:]
  return
end


# time loop
function solve!(u, nts)
  du = zeros(size(u))
  for i=1:nts
    euler_step!(u, du)
    du .= zeros(size(du)) # reset increment to zero
  end
end



# SSPRK3 time integration | allocates temporaries, no regard for storage optimization
function SSPRK3_step!(u)
  k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  rhs!(u, k1)
  rhs!(u + k1.*dt, k2)
  rhs!(u + 0.25*dt.*(k1+k2), k3)
  u[:] += dt*(1/6*k1 + 1/6*k2 + 2/3*k3)
end

function solve_RK3!(u, nts)
  for i=1:nts
    SSPRK3_step!(u)
  end
end



function plotloop(u, frames, time=0.2)
  if frames>0
    nts = Int((time / dt) / frames);
    for i=1:frames
      solve_RK3!(u, nts)
      plotsol(u)
      #sleep(time / frames)
    end
  else; solve_RK3!(u, Int(time/dt))
  end
end



############## CFL controlled RK3 ##############

# compute the time step based on  | note: wrap dt in a vector! to accommodate all the tools I have written / am writing
function calc_dt!(dt::Vector{T}, u) where {T}
  N::Int = Int(length(u)/3)
  dx::Float64 = 1/N
  lambda_max = 0.0
  for i=1:N
    lambda_max = max(max_wavespeed(u[i*3-2:i*3]), lambda_max)
  end
  dt[1] = dx / lambda_max * 0.5 # CFL = 0.5
  return nothing
end

function SSPRK3_step!(u, dt::Vector{T}) where {T}
  k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  rhs!(u, k1)
  rhs!(u + k1.*dt[1], k2)
  rhs!(u + 0.25*dt[1].*(k1+k2), k3)
  u[:] += dt[1]*(1/6*k1 + 1/6*k2 + 2/3*k3)
end

function solve_RK3_dt!(u, nts)
  dt = zeros(eltype(u), 1) # init dt as vector
  for i=1:nts
    calc_dt!(dt, u)
    SSPRK3_step!(u, dt)
  end
end



# factor 100 slowdown / more time needed stemmed from lacking factor dx in front of rhs!
# (stemming form spatial volume integral over the cell)
# this would yield a factor 1/100 less for N=100 in the update than it should be! -> 100x slower



###################################################################################################################
############################ Functions for ADM | better function / call tree structure ############################
###################################################################################################################


# initially from taping_test.jl

function helper1!(y, u, k1)
  y[:] = u + k1.*dt
  return nothing
end
function helper2!(y, u, k1, k2)
  y[:] = u + 0.25*dt.*(k1+k2)
  return nothing
end
function helper3!(y, k1, k2, k3)
  y[:] += dt*(1/6*k1 + 1/6*k2 + 2/3*k3)
  return nothing
end

# simpler SSPRK3 function that includes less arithmetic operation function calls
function SSPRK3!(u)
  k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  y1, y2 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  rhs!(u, k1)
  helper1!(y1, u, k1)
  rhs!(y1, k2)
  helper2!(y2, u, k1, k2)
  rhs!(y2, k3)
  helper3!(u, k1, k2, k3)
  return nothing
end


# RK3 solver including the variable time-step

function helper1_dt!(y, u, k1, dt)
  y[:] = u + k1.*dt[1] # dont forget the dt[1] brackets, or else: "Function to differentiate `MethodInstance for SSPRK3_dt!(::Vector{Float64})` is guaranteed to return an error and doesn't make sense to autodiff. Giving up"
  return nothing
end
function helper2_dt!(y, u, k1, k2, dt)
  y[:] = u + 0.25*dt[1].*(k1+k2)
  return nothing
end
function helper3_dt!(y, k1, k2, k3, dt)
  y[:] += dt[1]*(1/6*k1 + 1/6*k2 + 2/3*k3)
  return nothing
end

function SSPRK3_dt!(u)
  k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  y1, y2 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  dt = zeros(eltype(u), 1) # init dt as vector
  calc_dt!(dt, u)
  rhs!(u, k1)
  helper1_dt!(y1, u, k1, dt)
  rhs!(y1, k2)
  helper2_dt!(y2, u, k1, k2, dt)
  rhs!(y2, k3)
  helper3_dt!(u, k1, k2, k3, dt)
  return nothing
end





############################ Preallocating storage version and functions for comparisons ############################

function SSPRK3_prealloc!(u, k1, k2, k3, y1, y2)
  rhs!(u, k1)
  helper1!(y1, u, k1)
  rhs!(y1, k2)
  helper2!(y2, u, k1, k2)
  rhs!(y2, k3)
  helper3!(u, k1, k2, k3)
  return nothing
end


function n_steps_primal_prealloc!(u, n)
  k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  y1, y2 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  for i=1:n
    k1 .= 0.0; k2 .= 0.0; k3 .= 0.0; y1 .= 0.0; y2 .= 0.0;
    SSPRK3_prealloc!(u, k1, k2, k3, y1, y2)
  end
  return nothing
end

function n_steps_primal!(u, n)
  for i=1:n
    SSPRK3!(u)
  end
  return nothing
end


