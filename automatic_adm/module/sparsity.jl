
#= Important initial notes
A good starting point is the ColPack website https://cscapes.cs.purdue.edu/coloringpage/software.htm

We restrict ourselves to (numerically) nonsymmetric Jacobians.
Furthermore, we want to obtain the recover the jacobian from its compressed form directly (opposed to indirectly by successive substitution).
Lastly, we use unidirectional matrix partitioning, meaning for adjoint a row-partitioning while for tangent a column partitioning.
NOTE: This is a complex topic and for more complex Jacobian structures mixed tangent&adjoint methods could be considered.

text: in our case, sparsity patterns are quite simple and amount to CPR (Curtis-Powell-Reid seeding) -> look up a ref for this.
Limit ourselves to a basic applications of ColPack to explore the benefits of exploiting sparsity.

These methods are designed for Jacobian accumulation. Hence in the future, these should replace the tangent
or adjoint accumulation steps with an optimal (possible mixed tangent and adjoint) seeding .
In addition to accumulation, we also consider evaluation of AD models (and multiplication) for the edge elimination steps.
A self-evident/plain view is to express such an elimination by an accumulation of (A*J) where A is the edge jacobian provided
by an AD model and J is the seeding jacobian. The combined sparsity pattern can then be used to calculate a seeding matrix.
The seeding matrix is first applied to the known seeding jacobian, which is then propagated though the AD model.
In this way, in practice mixed methods could also yield benefits for the model evaluations.
(For example a few dense rows in a otherwise sparse matrix may be accumulated with adjoint, while the rest with tangent.)

Side note: now also have MMM cost differences: multiplication of sparse matrices is highly pattern dependent.

In general, all costs now depend on the sparsity patterns, necessitating a more complex cost model. This is for future work.
-> on the fly compute/benchmark costs of steps -> can blow up total planning runtime

Side note, but important: An implementation detail is the conversion between column-major and row-major.
Adjoint favors row major. So routines multiplying or converting major-ness could speed up the elimination steps.
These intermediate in-between procedures are currently not accounted for by the admission planning utility.
=> Another reason for a more sophisticated/realistic cost model.
Here, do everything column-major and neglect performance differences.
=#


#= 
General design:
A model evaluation (ELI TAN, ADJ) is performed as follows
B += (A*J)*S => B += A*(J*S)
1) compute seeding for combined sparsity pattern of A*J
2) 'multiply' J*S, where column sets of J are just added into one column each
3) compute the compressed jacobian with (J*S) as an input
4) recover the original jacobian and += into B

We now store the jacobians as sparse matrices, since we can compute the patterns for a fixed ADM schedule / sequence.
Given a dense recipe, we can convert is to a sparse one. Bzw. given a fixed sequence we can compute the patterns.

Do we want to update the cost model? Let's not!
=> Any modification of the cost model should include sparse MMM.
Since the AD mission planner always does n*m*k, the sizes would need to be faked to change the costs accordingly.
Whether this can be done in a way to yield better results is unknown.
But we argue that a more accurate cost model should rather be developed instead of adjusting the approximations.
For our simple case of banded matrices, we assume all costs just scale down approximately equally and use the dense schedule.
=#


############################### Other notes

# NOTE: the chunk remainder portion of the jacobian functions automatically reduces the chunk size to fit smaller seedings!!! (cool)

# Store all jacobians in sparse CSC, meaning adjoint compressed jacobians are reconstructed into column-major matrices, disregarding optimal efficiency

# Allocate temporary matrices for the compressed seed (in elimination) and compressed jacobians (acc and eli) -> could be centrally pre-allocated in recipe

# The reconstruction patterns and the seedings could be directly passed to the functions as inline-arguments, instead of accessing them through the recipe
# Left the way it is (stored explicitly in recipe) way to be less obscure

# during sparse recipe creation, the jacobians are initialized/updated with the edge patterns to ensure all structural entries are there, even if the
# starting data has numeric zeros at some entries

# NOTE: since patterns are known on ELI MUL, very efficient multiplication routines can be devised


# NOTE the tracking of sparsity patterns of the jacobians through using the recipe.jacobians themselves (see create_sparse_recipe())

# !!! NOTE colpack seems to be dependent on randomness, so we run it 100 times and take best result
# -> is this tiebreaker dependent?
# ==============> is due to OpenMP being compiled even if set to disabled, causes race on heuristic.


# NOTE: increment the jacobians/patterns during spares recipe creation, since we add together the patterns (eli and acc can increment on an existing jacobian!)

#= IMPORTANT NOTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
TLDR: currently all sparse (intermediate + final) jacobians are inefficiently accessed and reallocated in multiply!()

Addition of two sparse matrices  drops zeros.
This causes performance hits due to addition of different sparsity patterns onto a single matrix and also numeric zeros.
E.g. A += B + C, with A already owning the combined pattern of B and C, but A += B drops all zeros in C, which is an issue.
Here, we ignore this for execution and circumvent this in recipe creation by tracking the patterns extra.

In general great performance can be gained by using better matrix formats, since the matrix patterns are known pre-execution.
For example banded matrices could be employed. Together with a type that stores multiple overlapping matrices, even the dt-case
could be handled very efficiently.


# previous todos for reference:
 there is an issue with the current way jacobian patterns are propagated, that is J[i,k] += 0 does not induce a new entry!
  -> resulting pattern can inherit arithmetic/numeric zeros as structural zeros, which can contaminate the pattern computations
  -> make sure to init jacobians with pattern
=## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!






#= !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Sparsity reveals another important aspect of practical application that may be overlooked in theory: storage formats.
While accessing a dense column-major format matrix row-wise is slow, it is still bearable and barely noticeable for the modestly sized matrices inspected herein.
Jet, for the sparse matrices, slicing a row out of a column compressed storage format takes considerable time.
In practice it was observed to take orders of magnitude more time than the entire rest of the elimination sequence. For larger matrices this only becomes worse.


=#


#=
A jacobian becomes 'final' in the edge elimination when it is used in a step.
This means it is not modified after it is used once. Only re-used in other steps.
=#





using SparseArrays # also used already in from dependency analysis



############################################## seed matrix computation with ColPack ##############################################

# container for ColPack output
struct Coloring
  n_colors::Int
  color_groups::Vector{Int}
  seed::Matrix{Float64}
end


# write column-compressed sparse matrix into a matrix market format file
function write_mtx(filename::String, mat::SparseMatrixCSC{Float64, Int64})
  io = open(filename, "w")
  write(io, "%%MatrixMarket matrix coordinate real general\n")
  write(io, "$(mat.m) $(mat.n) $(length(mat.nzval))\n") # rows then cols (leading dim first)
  for j=1:mat.n # columns
    for idx in mat.colptr[j]:(mat.colptr[j+1]-1)
      write(io,"$(mat.rowval[idx]) $j $(mat.nzval[idx])\n")
    end
  end
  close(io)
  return nothing
end

# call ColPack on matrix file to get Coloring (-> seed matrix) | NOTE: requires the modified (!) ColPack executable in path
function ColPack(filename::String, mode::String="TAN")
  if mode == "TAN"; mode = "COLUMN_PARTIAL_DISTANCE_TWO"; else mode = "ROW_PARTIAL_DISTANCE_TWO"; end
  str = read(`./module/colpack/ColPack $filename $mode`, String)
  
  rows = split(str, "\n")
  n_rows, n_cols = parse.(Int, split(rows[4], " "))
  S = zeros(n_rows, n_cols)

  for i=5:n_rows+4
    cols = split(rows[i], " ")
    for j=1:n_cols
      S[i-4,j] = parse(Float64, cols[j])
    end
  end

  n_colors, n_vertices = parse.(Int, split(rows[n_rows+6], " "))
  groups = parse.(Int, split(rows[n_rows+7], " ")[1:end-1]) .+ 1
  # n_colors == compressed dim size, n_vertices == uncompressed dim size
  if mode == "COLUMN_PARTIAL_DISTANCE_TWO"
    if n_colors != n_cols && n_vertices != n_rows; println("ERROR: mismatch between seed and color groups"); end
  else
    if n_colors != n_rows && n_vertices != n_cols; println("ERROR: mismatch between seed and color groups"); end
  end


  return Coloring(n_colors, groups, S)
end


# computes color groups and resulting compressed seed matrix for a given sparsity pattern
function compute_coloring(pattern::SparseMatrixCSC{Bool, Int64}, mode="TAN"; temp_dir=false)
  if temp_dir
    path = mktempdir()
  else
    path ="./temp"
  end
  filename = path * "/colpack_target_matrix.mtx"
  write_mtx(filename, 1.0*pattern)
  return ColPack(filename, mode)
end




############################################## sparse recipe and schedule functions ##############################################

# Note: the test benchmarks show that the dense reconstruction pattern version is the best, so I will use that.


#=
Compute a reconstruction pattern for a column seeding of a sparse edge.
The idea is to store in each entry of the sparsity pattern the index of its column.
When this pattern is multiplied with the seed matrix, the resulting matrix has the same form as the compressed jacobian.
Each entry of this reconstruction pattern stores the column of the decompressed matrix (has pattern as sparsity pattern) it belongs to.
The rows are the same for column-compression.
=#
function compute_reconstruction_pattern(pattern::SparseMatrixCSC{Bool, Int64}, seed::Matrix)
  # convert to int
  pattern = 1*pattern
  seed = 1*(seed .> 0)

  for col=1:pattern.n
    for idx in pattern.colptr[col]:(pattern.colptr[col+1]-1)
      pattern.nzval[idx] = col
    end
  end
  return pattern*seed
end


# reconstruct a compressed jacobian and increment it onto a sparse jacobian matrix
function reconstruct!(target::SparseMatrixCSC{Float64, Int64}, compressed_jacobian::Matrix{Float64}, reconstruction_pattern::Matrix{Int})
  # for optimal efficiency the target matrix should already have the right structure
  # (nonzero entries everywhere that is written to, but not necessarily exclusively there)

  for col=1:size(reconstruction_pattern)[2]
    for row=1:size(reconstruction_pattern)[1]
      if  reconstruction_pattern[row, col] != 0 # kind of inefficient, but seems unavoidable with this method
        target[row, reconstruction_pattern[row, col]] += compressed_jacobian[row, col]
      end
    end
  end
  return nothing
end

function reconstruct!(target::SparseMatrixCSC{Float64, Int64}, compressed_jacobian::Matrix{Float64}, reconstruction_pattern::SparseMatrixCSC{Int, Int64})
  # for optimal efficiency the target matrix should already have the right structure
  # (nonzero entries everywhere that is written to, but not necessarily exclusively there)

  for col=1:size(reconstruction_pattern)[2]
    for idx=reconstruction_pattern.colptr[col]:(reconstruction_pattern.colptr[col+1]-1)
      row = reconstruction_pattern.rowval[idx]
      target_col =  reconstruction_pattern.nzval[idx]
      target[row, target_col] += compressed_jacobian[row, col]
      #same: target[row, reconstruction_pattern[row, col]] += compressed_jacobian[row, col]
    end
  end
  return nothing
end

# alternative version of reconstruct!() using the color groups instead of the reconstruction pattern
# given an edge sparsity pattern and the seed color groups reconstructs a compressed jacobian and increments it onto the target
function reconstruct!(target::SparseMatrixCSC{Float64, Int64}, compressed_jacobian::Matrix{Float64}, pattern::SparseMatrixCSC{Bool, Int64}, color_groups::Vector{Int})
  #n_colors = color_groups.n_colors # == size(compressed_jacobian)[2] == n_cols compressed == n_cols seed
  for col=1:pattern.m # == cols in target
    ccol = color_groups[col] # compressed col
    for idx in pattern.colptr[col]:(pattern.colptr[col+1]-1)
      row = pattern.rowval[idx]
      # instead of []-access iteration over target.rowval could be faster, using the fact that successive accesses are ascending in order
      target[row, col] += compressed_jacobian[row, ccol]
    end
  end
  return nothing
end


function compute_reconstruction_pattern_rowwise(seed::Matrix, pattern::SparseMatrixCSC{Bool, Int64})
  pattern = 1*pattern
  seed = 1*(seed .> 0)
  for col=1:pattern.m
    for idx in pattern.colptr[col]:(pattern.colptr[col+1]-1)
      pattern.nzval[idx] = pattern.rowval[idx]
    end
  end
  return seed*pattern
end

# row based version for adjoint compressed jacobian reconstruction
function reconstruct_rowwise!(target::SparseMatrixCSC{Float64, Int64}, compressed_jacobian::Matrix{Float64}, reconstruction_pattern::Matrix{Int})
  for col=1:size(reconstruction_pattern)[2]
    for row=1:size(reconstruction_pattern)[1]
      if  reconstruction_pattern[row, col] != 0
        # reconstruction pattern now has the row-indices and the column stays the same
        target[reconstruction_pattern[row, col], col] += compressed_jacobian[row, col]
      end
    end
  end
  return nothing
end





# compute compressed jacobian seed = J*S by addition of columns (faster than matrix product)
function compress_jacobian_seed(jacobian::SparseMatrixCSC{Float64, Int64}, color_groups::Vector{Int}, n_colors::Int)
  n_rows = jacobian.m
  seed = zeros(n_rows, n_colors)
  for j in eachindex(color_groups)
    seed[:, color_groups[j]] .+= jacobian[:, j]
  end
  return seed
end

# row based compression for adjoint mode
function compress_jacobian_seed_rowwise(jacobian::SparseMatrixCSC{Float64, Int64}, color_groups::Vector{Int}, n_colors::Int)
  n_cols = jacobian.n
  seed = zeros(n_colors, n_cols)
  for i in eachindex(color_groups)
    seed[color_groups[i], :] .+= jacobian[i, :]
  end
  return seed
end





# this is for some reason slower than the default version
# function multiply!(lhs::SparseMatrixCSC{Float64, Int}, rhs1::SparseMatrixCSC{Float64, Int}, rhs2::SparseMatrixCSC{Float64, Int})
#   mul!(lhs, rhs1, rhs2, 1.0, 1.0) # mul!(A,B,C, 1.0, 1.0) computes C = 1.0*A*B + 1.0*C (C=lhs, A=rhs1, B=rhs2)
#   return nothing
# end

function accumulate!(::ForwardMode, jacobian, edge, seed, rp, ::Val{chunk}) where{chunk}
  # allocate temporary storage | may be more efficient to keep allocated in recipe and just init to zero at the beginning
  jac = zeros(size(rp)...)
  edge_jacobian!(Forward, edge, jac, seed, Val(chunk))
  reconstruct!(jacobian, jac, rp)
  return nothing
end


function eliminate!(::ForwardMode, jacobian, edge, jac_seed, color_groups, n_colors, combined_rp, ::Val{chunk}) where{chunk}
  # allocate temporary compressed seed matrix
  seed = compress_jacobian_seed(jac_seed, color_groups, n_colors)
  accumulate!(Forward, jacobian, edge, seed, combined_rp, Val(chunk))
  return nothing
end


function accumulate!(::ReverseMode, jacobian, edge, seed, rp, ::Val{chunk}) where{chunk}
  jac = zeros(size(rp)...)
  edge_jacobian!(Reverse, edge, jac, seed, Val(chunk))
  reconstruct_rowwise!(jacobian, jac, rp)
  return nothing
end
function eliminate!(::ReverseMode, jacobian, edge, jac_seed, color_groups, n_colors, combined_rp, ::Val{chunk}) where{chunk}
  seed = compress_jacobian_seed_rowwise(jac_seed, color_groups, n_colors)
  accumulate!(Reverse, jacobian, edge, seed, combined_rp, Val(chunk))
  return nothing
end








############################################## sparse recipe creation from schedule and dag ##############################################


mutable struct SparseRecipe
  func::Any
  dag::DAG
  schedule::Vector{Any}
  jacobians::Vector{SparseMatrixCSC{Float64, Int}}

  seeds::Vector{Matrix{Float64}}
  color_groups::Vector{Vector{Int}}
  reconstructions::Vector{Matrix{Int}}

  retrace::ReTrace # retrace object used to update primal arguments in dag

  SparseRecipe(f, dag, trace) = new(f, dag, [], [], [], [], [], ReTrace(dag, trace))
end







function create_sparse_recipe(f, dag::DAG, schedule::Schedule, trace::Trace, chunksize::Int, temp_dir=false)
  recipe = SparseRecipe(f, dag, trace)

  # see comments in non-sparse version create_recipe() for details on core functionality

  jac_dict = Dict()
  register_jacobian(jac_dict, (1, dag.n_vertices), recipe)

  edge_dict = Dict()
  for i in eachindex(dag.edges); edge_dict[(dag.edges[i].conn.first, dag.edges[i].conn.second)] = i; end;

  for (op, mode, target) in zip(schedule.operations, schedule.modes, schedule.targets)
    i = target[1]; j = target[end]
    J_idx = register_jacobian(jac_dict, (i, j), recipe)
    #=
    The jacobians in the recipe are used here to propagate the structural(!) sparsity pattern of the jacobian matrices.
    These are needed for elimination steps in TAN or ADJ mode, for which the seeding/coloring is computed using the edge and jacobian combined pattern.
    Hence the recipe.jacobians here are sparsity patterns with Float64 entries {1.0, 0.0}. Assigning a bool pattern to it converts to float automatically.
    Multiplication of these structural patterns does not drop needed entires, since all structural entries are 1.0 or 1==true.
    Afterwards Jacobians are not reset to zero, since adm_jacobian() does it anyway.
    =#

    if op == "ELI"
      if mode == "TAN"
        edge_idx = edge_dict[(target[end-1], j)]
        seed_idx = jac_dict[(i, target[end-1])] # jacobian of path before last edge is seed
        csp = (dag.edges[edge_idx].pattern * recipe.jacobians[seed_idx]).>0 # get the combined sparsity pattern | .>0 converts to bool type
        recipe.jacobians[J_idx] += csp # update the result jacobian pattern
        coloring = compute_coloring(csp, mode, temp_dir=temp_dir)
        rp = compute_reconstruction_pattern(csp, coloring.seed)
        push!(recipe.color_groups, coloring.color_groups); groups_idx = length(recipe.color_groups)
        push!(recipe.reconstructions, rp); rp_idx = length(recipe.reconstructions)
        push!(recipe.schedule, ()->eliminate!(Forward, recipe.jacobians[J_idx], recipe.dag.edges[edge_idx], recipe.jacobians[seed_idx],
                                              recipe.color_groups[groups_idx], coloring.n_colors, recipe.reconstructions[rp_idx], Val(chunksize)))

      elseif mode == "ADJ"
        seed_idx = jac_dict[(target[2], j)]
        edge_idx = edge_dict[(i, target[2])]
        csp = (recipe.jacobians[seed_idx] * dag.edges[edge_idx].pattern).>0 # get the combined sparsity pattern
        recipe.jacobians[J_idx] += csp # update the result jacobian pattern
        coloring = compute_coloring(csp, mode, temp_dir=temp_dir)
        rp = compute_reconstruction_pattern_rowwise(coloring.seed, csp)
        push!(recipe.color_groups, coloring.color_groups); groups_idx = length(recipe.color_groups)
        push!(recipe.reconstructions, rp); rp_idx = length(recipe.reconstructions)
        push!(recipe.schedule, ()->eliminate!(Reverse, recipe.jacobians[J_idx], recipe.dag.edges[edge_idx], recipe.jacobians[seed_idx],
                                              recipe.color_groups[groups_idx], coloring.n_colors, recipe.reconstructions[rp_idx], Val(chunksize)))

      else # mode == "MUL"
        k = target[2]
        rhs1_idx = jac_dict[(k, j)]
        rhs2_idx = jac_dict[(i, k)]
        recipe.jacobians[J_idx] += recipe.jacobians[rhs1_idx]*recipe.jacobians[rhs2_idx] # update the result jacobian pattern
        push!(recipe.schedule, ()->multiply!(recipe.jacobians[J_idx], recipe.jacobians[rhs1_idx], recipe.jacobians[rhs2_idx]))
      end

    else # op == "ACC"
      edge_idx = edge_dict[(i, j)]
      # note that as above, we increment. This is because the accumulation and elimination steps can increment the result
      # we do not want to loose the previous pattern, but add the patterns together.
      recipe.jacobians[J_idx] += dag.edges[edge_idx].pattern # update the result jacobian pattern
      coloring = compute_coloring(dag.edges[edge_idx].pattern, mode, temp_dir=temp_dir)
      push!(recipe.seeds, coloring.seed); seed_idx = length(recipe.seeds)
      # note, that the rp and seeds could be directly passed to the functions instead of via recipe, since they are const
      if mode == "TAN"
        rp = compute_reconstruction_pattern(dag.edges[edge_idx].pattern, coloring.seed)
        push!(recipe.reconstructions, rp); rp_idx = length(recipe.reconstructions)
        push!(recipe.schedule, ()->accumulate!(Forward, recipe.jacobians[J_idx], recipe.dag.edges[edge_idx], 
                                               recipe.seeds[seed_idx], recipe.reconstructions[rp_idx], Val(chunksize)))
      else # mode == "ADJ"
        rp = compute_reconstruction_pattern_rowwise(coloring.seed, dag.edges[edge_idx].pattern)
        push!(recipe.reconstructions, rp); rp_idx = length(recipe.reconstructions)
        push!(recipe.schedule, ()->accumulate!(Reverse, recipe.jacobians[J_idx], recipe.dag.edges[edge_idx], 
                                               recipe.seeds[seed_idx], recipe.reconstructions[rp_idx], Val(chunksize)))
      end
    end # end if op
  end # end for

  return recipe
end




######################################## compressed (dense) tangent jacobian accumulation ########################################

# via coloring + reconstruction method

struct CAccumulate # compressed accumulation
  mode::Union{ForwardMode, ReverseMode}
  func::Any
  chunk::Int
  seed::Matrix{Float64}
  reconstruction::Matrix{Int}
  jacobian::SparseMatrixCSC{Float64, Int}
  compressed_jacobian::Matrix{Float64}

  function CAccumulate(func, arg, mode; temp_dir=true, chunk=32)
    _, dict = analyze_dependencies(func, (arg,))
    pattern = dict[(1,1)]
    J = 1.0 * pattern # alloc and init internal storage
    if mode==Forward
      coloring = compute_coloring(pattern, "TAN", temp_dir=temp_dir)
      rp = compute_reconstruction_pattern(pattern, coloring.seed)
      cJ = zeros(size(pattern)[1], coloring.n_colors)
    else # mode==Reverse
      coloring = compute_coloring(pattern, "ADJ", temp_dir=temp_dir)
      rp = compute_reconstruction_pattern_rowwise(coloring.seed, pattern)
      cJ = zeros(coloring.n_colors, size(pattern)[1])
    end

    # enzyme crashes for chunk too large, allow for max passing ## old:# set chunk = n_colors for single AD evaluation accumulate
    return new(mode, func,  min(coloring.n_colors, chunk), coloring.seed, rp, J, cJ)
  end
end

function (acc::CAccumulate)(arg, func=acc.func) # func only for error testing (apply to wrong function)
  acc.jacobian .= 0.0
  acc.compressed_jacobian .= 0
  jacobian!(acc.mode, func, (arg,), Val(1), Val(1), Val(acc.chunk), acc.compressed_jacobian, acc.seed)
  if acc.mode==Forward; reconstruct!(acc.jacobian, acc.compressed_jacobian, acc.reconstruction)
  else reconstruct_rowwise!(acc.jacobian, acc.compressed_jacobian, acc.reconstruction)
  end
  return acc.jacobian
end



