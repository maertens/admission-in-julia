

####################################################################################################################################################
########################################################## TAGGING IS DEPRECATED / UNUSED ##########################################################
####################################################################################################################################################

#=
Based on my tracing.jl which is an augmentation and extension of the Cassette tracing example in the docs.
For more detailed documentation on the tracing part, see the tracing.jl, where I commented extensively.
Furthermore, an initial getting started and evaluation of the tagging mechanism can be found in experiments/variable_tagging.jl
=#


using Cassette



mutable struct Tag
  name::String

  Tag(str) = new(str)
end
# since the fields of a struct are compared with object identity === (https://discourse.julialang.org/t/surprising-struct-equality-test/4890)
# and since strings are mutable, the object identities of two tags are not the same, even if the string matches
# we overload the == operator to not do this!
Base.:(==)(lhs::Tag, rhs::Tag) = (lhs.name == rhs.name)


# an entry in the trace produced by a function call, first part of the 'current' vector in the trace
mutable struct TraceEntry
  func::Any # the called function
  args::Tuple # copy of the input/call arguments
  tags::Tuple # tags of the input/call arguments

  # also want to record the return after exiting the function
  ret::Any
  ret_tag::Tag

  # construct with only the enter quantities
  TraceEntry(f, a, t) = new(f, a, t, nothing, Tag("nothing"))
end


mutable struct Trace
  current::Vector{Any} # stores pairs of (Trace Entry => nested child vector)
  stack::Vector{Any} # scope stack to store and retrieve the current list when entering or exiting a child function

  max_depth::Int # truncation level: the maximum depth of the call tree to trace
  current_depth::Int # the current depth of the 'current' variable
  tag_counter::Int # keep track of what tags/ids are already assigned by sequential assignment

  Trace() = new(Any[], Any[], 1, 0, 0)
  Trace(depth) = new(Any[], Any[], depth, 0, 0)
end



# context type for tagged tracing
Cassette.@context TaggedTraceCtx
# allow all values (::DataType) in the context to be tagged with metadata=Tag
Cassette.metadatatype(::Type{<:TaggedTraceCtx}, ::DataType) = Tag






# returns a tuple of untagged values | if not tagged to begin with, does nothing | preserves === status
get_args(tpl, ctx) = map((arg)->Cassette.untag(arg, ctx), tpl)
get_tags(tpl, ctx) = map((arg)->Cassette.metadata(arg, ctx), tpl)


# return a tagged version of an input if it is untagged and a vector
function tag_vector(arg, t, ctx)
  if !Cassette.hasmetadata(arg, ctx) && arg isa Vector # only tag untagged vectors
    t.tag_counter += 1
    return Cassette.tag(arg, ctx, Tag("v"*string(t.tag_counter)))
  else
    return arg
  end
end


function enter!(ctx, args...)
  #println("Entering ", args[1], " with ", length(args)-1, " arguments ", args[2:end])
  t::Trace = ctx.metadata
  t.current_depth += 1
  if t.current_depth <= t.max_depth
    # store the called function and copies (to preserve input status) of the arguments and their tags
    entry = TraceEntry(args[1], deepcopy(get_args(args[2:end], ctx)), deepcopy(get_tags(args[2:end], ctx)))
    pair = entry => Any[]
    push!(t.current, pair) 
    push!(t.stack, t.current)
    t.current = pair.second
  end
  return nothing
end



function exit!(ctx, ret, args...) # note: the first argument is the return value
  #println("Exiting ", args[2])
  t::Trace = ctx.metadata
  t.current_depth -= 1
  if t.current_depth < t.max_depth
    t.current = pop!(t.stack)

    # log the return of the function in the trace
    t.current[end].first.ret = ret
    if Cassette.hasmetadata(ret, ctx)
      t.current[end].first.ret_tag = Cassette.metadata(ret, ctx)
    end

    # perform a sanity check: after the call all tags of the arguments should be the same
    if get_tags(args[2:end], ctx) != t.current[end].first.tags
      println("ERROR: Tag mismatch after a function call with ", length(args)-1, " argument(s)")
      println(ret, args)
      println(get_tags(args[2:end], ctx))
      println(t.current[end].first.tags)
    end
  end
  return nothing
end

#Cassette.prehook(ctx::TaggedTraceCtx, args...) = enter!(ctx, args...)
#Cassette.posthook(ctx::TaggedTraceCtx, args...) = exit!(ctx, args...)


function Cassette.overdub(ctx::TaggedTraceCtx, args...)

  # since the arguments are passed as tuples and tuples are immutable, enter!() cannot modify/tag the arguments
  # therefore, we do it here by creating a copy of the args | give all untagged argument a tag
  args = ntuple((i)->tag_vector(args[i], ctx.metadata, ctx), length(args))
  # since we want to store the new tags, we perform the prehook function here (probably to that best practice....)
  enter!(ctx, args...)

  if (ctx.metadata.current_depth < ctx.metadata.max_depth) && Cassette.canrecurse(ctx, args...)
    ret = Cassette.recurse(ctx, args...)
  else
    ret = Cassette.fallback(ctx, args...)
  end

  # also call exit here to get the correct tags (the posthook still operates on the old tag set outside of this function)
  exit!(ctx, ret, args...)

  return ret
end


function tagged_tracing(f, args; argnames=nothing, depth=1)
  trace = Trace(depth)
  ctx = Cassette.enabletagging(TaggedTraceCtx(metadata = trace), f)
  # tag the input values
  if argnames == nothing; argnames = ntuple((i)->("x"*string(i)), length(args)); end
  tagged_args = ntuple((i)->(Cassette.tag(args[i], ctx, Tag(argnames[i]))), length(args))
  
  #enter!(ctx, f, tagged_args...) # not needed if called inside overdub
  ret = Cassette.overdub(ctx, f, tagged_args...)
  #exit!(ctx, ret, f, tagged_args...) # posthook also gets the return: args=(ret, f, args...)
  return trace
end




