

# use the old tracing that captures references+copies
include("tracing.jl")
include("dependencies.jl")


mutable struct Edge
  conn::Pair{Int, Int} # connectivity source => target
  func::Any # function that induces this edge in the DAG
  args::Any # just store all arguments for all functions
  arg_ind::Pair{Int, Int} # argument indices in the function: input idx => output idx
  pattern::SparseMatrixCSC{Bool, Int} # sparsity pattern of the edge
end

mutable struct DAG
  n_vertices::Int # counter for the number of vertices
  vertex_sizes::Vector{Int} # vector lengths of the nodes/variables
  edges::Vector{Edge}

  # for plotting / information
  vertex_labels::Vector{String}
  edge_labels::Vector{String}

  DAG() = new(0, [], [], [], [])
end

# adds a vertex to the dag | returns the id of the added vertex
function add_vertex!(dag::DAG, size::Int, name=nothing)
  dag.n_vertices += 1
  if name == nothing; name = "v"*string(dag.n_vertices); end
  push!(dag.vertex_labels, name)
  push!(dag.vertex_sizes, size)
  return dag.n_vertices
end


function add_edge_dag!(dag::DAG, func, args, source_arg_j, target_arg_i, source, target, sp) # _dag in name to not conflict with Graphs add_edge!
  push!(dag.edges, Edge(source => target, func, args, source_arg_j => target_arg_i, sp))
  push!(dag.edge_labels, generate_edge_name(dag, func, length(args), source_arg_j, target_arg_i, source, target))
  return nothing
end


# creates a string from a function and the position of the relevant arguments (e.g. f(., x, ., . z) -> for dependency between x and z)
function generate_edge_name(dag, f, n_args, source_j, target_i, source_id, target_id)
  name = string(f) * "("
  for i=1:n_args
    if i > 1; name *= ", "; end
    if i==source_j
      name *= dag.vertex_labels[source_id]
    elseif i==target_i
      name *= dag.vertex_labels[target_id] 
    else
      name *= "."
    end
  end
  name *= ")"
  return name
end




# fill the leaves vector with the leaf nodes of the tree spanned by the trace
function get_trace_leaves(trace_current, trace_current_copy, leaves, leaves_copy)
  for ((current, children), (args_copy, args_children)) in zip(trace_current, trace_current_copy)
    if isempty(children)
      push!(leaves, current)
      push!(leaves_copy, args_copy)
    else
      get_trace_leaves(children, args_children, leaves, leaves_copy) # recursion to traverse the trace
    end
  end
end



mutable struct ActiveVar
  ref::Any
  vertex::Int
end


function get_output_arg_indices(dep_matrix)
  res::Vector{Int} = []
  n_out, n_in = size(dep_matrix)
  for i=1:n_out
    if count(dep_matrix[i, :]) > 0 # depends on any of the arguments
      push!(res, i)
    end
  end
  return res
end


function get_input_arg_indices(dep_matrix)
  res::Vector{Int} = []
  n_out, n_in = size(dep_matrix)
  for j=1:n_in
    if count(dep_matrix[:, j]) > 0 # entry in col -> influences another argument (or itself)
      push!(res, j)
    end
  end
  return res
end


# register an output in the tape/list of active variables | if the argument is already known, its dag vertex reference id is updated
function register!(actives, argument, new_id)
  # check if known via object identity (===) equals
  for a in actives
    if a.ref === argument
      a.vertex = new_id
      return nothing
    end
  end
  # not found -> add to list
  push!(actives, ActiveVar(argument, new_id))
  return nothing
end

# returns the vertex id of an active variable, or -1 if the argument is not active
function active_id(actives, argument)
  for a in actives
    if a.ref === argument
      return a.vertex
    end
  end
  return -1
end



# checks whether all arguments are vectors, used to skip leaf functions that we cannot handle
function check_vector_arguments(args)
  for arg in args
    if !(arg isa Vector)
      return false
    end
  end
  return true
end


#=
note that we assume one of the following top level function signatures:
f(x,y) where one is a pure input and the other a pure output
f(x) where x is a in-output
In principle all signatures (without returns) works, but we focus on these!
=#


# create a dag from a trace | uses 'old' tracing.jl trace format, as the new tagging format seems meh
function tape(trace)
  # the leave nodes are our elementals, all non-leaf functions are unrolled by the trace!
  leaves = Any[]
  leaves_arg_copies = Any[]
  get_trace_leaves(trace.current, trace.current_deepcopy, leaves, leaves_arg_copies)
  println("Tape: processing ", length(leaves), " trace leaves")

  ##### require a vector of active variables | analogously to the tape in traditional operator overloading AD
  ##### every active variable consists of a reference of an object (to check === with encountered arguments)
  ##### and a reference/id of a vertex in the dag (its current value state)
  active::Vector{ActiveVar} = []

  # init the dag with top level function arguments as vertices
  dag = DAG();
  func, args... = trace.current[1].first

  ## we want to make the top level arguments active, as we differentiate w.r.t to them -> add vertices in dag
  # only add non pure output (i.e. input) arguments to the dag, since pushing pure output vars into dag now creates orphan vertices
  dep_matrix, patterns = analyze_dependencies(func, args)
  input_arg_ind = get_input_arg_indices(dep_matrix)
  output_args = []
  for j in eachindex(args)
    if j in input_arg_ind
      id = add_vertex!(dag, length(args[j]), "x")
      push!(active, ActiveVar(args[j], id))
    else
      # stash a reference to the output args for later | re-identify the output args at the end and set node labels accordingly
      push!(output_args, args[j])
    end
  end
  #println("Tape: starting active vec: ", active)


  for (leaf, arg_copy) in zip(leaves, leaves_arg_copies)
    func, args... = leaf

    # the function can be anything, such as a call to zeros(Float, 3), which are not input vector arguments!
    # therefore we just ignore all non pure vector functions, assuming they are not active / do not influence jacobian calculation!
    if !check_vector_arguments(args); continue; end

    # dependency analyze the function to identify input and output arguments
    dep_matrix, patterns = analyze_dependencies(func, args)
    output_arg_ind = get_output_arg_indices(dep_matrix)
    #println("Tape: output argument indices: ", output_arg_ind)

    # we need fixed j_id=previous for self-dependent args (i.e. sources of edges are arguments before function call)
    j_ids = ntuple((j)->active_id(active, args[j]), length(args))

    # for all args that are written to (output or in/output)
    for i in output_arg_ind
      # create a new node in the dag for the argument
      i_id = add_vertex!(dag, length(args[i]))
      # check if we know it / it is active
      # if yes, set the id to a the value (new node in dag) -> update of what node the shadow/derivative/variable is currently at
      # if no, push a new element into the activity list with new node id
      register!(active, args[i], i_id)

       # for all arguments (treated as input) | swapped loop order with output arg,
      for j in eachindex(args)
        j_id = j_ids[j] # retrieve input argument vertex id
        if j_id < 0; continue; end # only create edges in dag if the input argument is known/active, else this is a const dependency (e.g. initialization)
        # if the output argument depends on the input arg
        if dep_matrix[i, j] == true
          # push an edge into the dag: function, (j,i) indices of the arguments, corresponding vertices in the dag (identification required)
          add_edge_dag!(dag, func, arg_copy, j, i, j_id, i_id, patterns[(i, j)])
        end
      end
    end # for output_arg_ind
  end # for trace leaves

  # fetch the id of the pure output arguments and rename the vertex labels
  for arg in output_args
    id = active_id(active, arg)
    dag.vertex_labels[id] = "z"
  end

  return dag
end


### plot a DAG by converting it to a graph
using Graphs, GraphRecipes, Plots

function dag_plot(dag::DAG)
  g = SimpleDiGraph(0, 0)
  add_vertices!(g, dag.n_vertices)

  # fill edges and labels
  edgelabel_dict = Dict()
  for e in eachindex(dag.edges)
    edge = dag.edges[e].conn
    add_edge!(g, edge.first, edge.second)
    edgelabel_dict[(edge.first, edge.second)] = dag.edge_labels[e]
  end
  graphplot(g, names=dag.vertex_labels, edgelabel=edgelabel_dict, nodeshape=:ellipse, markersize = 0.3, fontsize=10, size=(1200, 900), curves=false, show=true)
end



