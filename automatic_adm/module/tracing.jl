#=
Starting from the Cassette trace example in https://julia.mit.edu/Cassette.jl/stable/contextualdispatch/
we use a stack in the metadata to create a trace through a function.
The first extension of this is to only trace up to a certain specified depth.
Note that I chose the prehook and posthook stack method instead of recursion, as I find the former more intuitive.
=#


using Cassette

using Graphs, GraphRecipes, Plots

# a context (type) that signifies that we are performing trace operations | functions are overloaded for this context type
Cassette.@context TraceCtx


#= General outline
Every function gets a pair (args => Any[]) where the first argument of args is the function itself.
The second element of the pair is a vector of Any, which is used to store the pairs of the functions that are called by this function.
To generate this nested structure/trace, the prehook and posthook are augmented.
In a function body (the 'current' trace level) multiple other functions are called. Each of which triggers a pre- and posthook.

In the prehook, the pair for the called function is created and pushed into the children vector.
Then, since we now enter into the child function, we store the current trace on the stack.
To go down a level, the current trace then becomes/gets set to the second argument of the pair for the child function.
This means, that current stores the list of the functions&arguments (plus the nested => Any[]) of the current scope/level.

In the posthook, after exiting the function the current trace is retrieved from the stack.

Note, for any level, the current list is a reference to the Any[] vector of the pair belonging to the current function,
that is stored in the current list of the parent function. Therefore, all data is retained and the resulting top level current
holds all nested information.
=#


#=
We can out of order execute the functions via the trace:
for i=1 and highest level current (trace.current) we get the top level function:
func = trace.current[i].first[1]
args = trace.current[i].first[2:end]
func(args...) # execute the function
=#



#= IMPORTANT NOTE:
the trace stores references to values/objects such as vectors. This allows us to find out whether two inputs are the same object.
BUT: this also means that calling a modifying function will modify the output arguments in the trace!
==> Cannot simply re-execute a function in the trace, since we don't know its input argument state!
We can also store input argument copies to fix this issue.
=#


# container to trace the program and gets its (truncated) call tree
mutable struct Trace
    # list of current level/scope pairs of functions&args => nested child vector
    current::Vector{Any}
    # scope stack to store and retrieve the current list when entering or exiting a child function
    stack::Vector{Any}
    # truncation level: the maximum depth of the call tree to trace
    max_depth::Int
    # the current depth of the 'current' variable
    current_depth::Int

    # mimics current vector, but stores copies of arguments instead of references | standalone, to keep trace.current as is
    current_deepcopy::Vector{Any}
    stack_deepcopy::Vector{Any}

    Trace() = new(Any[], Any[], 1, 0, Any[], Any[])
    Trace(depth) = new(Any[], Any[], depth, 0, Any[], Any[])
end


# returns a tuple of untagged values | if not tagged to begin with, does nothing | preserves === status
#tuple_untag(tpl) = map((arg)->Cassette.untag(arg, ctx), tpl) # care this works only for global tagging ctx!


# log the current function and its args (both stored in args...) in the trace
function enter!(t::Trace, args...)
  t.current_depth += 1
  # only add to the trace, if the depth is higher than the truncation threshold
  if t.current_depth <= t.max_depth
    pair = args => Any[] # args is a tuple -> creates a pair args => Any[] (vector of any)
    #pair = tuple_untag(args) => Any[]
    push!(t.current, pair) # append the pair to the current trace
    push!(t.stack, t.current) # push current trace into stack
    t.current = pair.second # set current to the second element of the current pair (which is Any[])
    # -> the empty Any passed to the next lower call will collect information of those calls in a nested structure!
    # at the end, current contains a full call-stack, since pair.second is a reference (or whatever in julia)

    # do the same for the current_deepcopy vector
    pair_two = deepcopy(args[2:end]) => Any[]
    #pair_two = deepcopy(tuple_untag(args[2:end])) => Any[]
    push!(t.current_deepcopy, pair_two)
    push!(t.stack_deepcopy, t.current_deepcopy)
    t.current_deepcopy = pair_two.second
  end
  return nothing
end

# when exiting a context, the current state should be restored to the higher scope
function exit!(t::Trace)
  t.current_depth -= 1
  # only restore the trace, if we are in the depth where we track the trace
  if t.current_depth < t.max_depth # only "<" since we do not pop when exiting a function at lower depth (current depth == max depth)
    t.current = pop!(t.stack) # retrieve the trace of the next higher call (the callers current)

    t.current_deepcopy = pop!(t.stack_deepcopy)
  end
  return nothing
end

Cassette.prehook(ctx::TraceCtx, args...) = enter!(ctx.metadata, args...)
Cassette.posthook(ctx::TraceCtx, args...) = exit!(ctx.metadata)


# overload overdub for our trace context to stop overdubbing and call the fallback once the max depth of the trace is reached
#  this significantly decreases the tracing time, since the lower level function forest is left unaltered
function Cassette.overdub(ctx::TraceCtx, args...)
  if (ctx.metadata.current_depth < ctx.metadata.max_depth) && Cassette.canrecurse(ctx, args...)
    Cassette.recurse(ctx, args...)
  else
    Cassette.fallback(ctx, args...)
  end
end


function trace_function(depth, f, args)
  # Note: to capture the highest level function, it is wrapped in another function (i.e. ()->f(x,y,z) instead of (ctx, f, x,y,z))
  trace = Trace(depth)
  #Cassette.overdub(TraceCtx(metadata = trace), () -> f(args...)) # this results in the ... operator being traced !
  #Cassette.overdub(TraceCtx(metadata = trace), f, args...) # this does not pre and posthook the function f !
  # but, we can simply manually call pre and posthook here
  enter!(trace, f, args...) # note: args... in enter! should contain the function at the first position
  Cassette.overdub(TraceCtx(metadata = trace), f, args...)
  exit!(trace)
  return trace
end




# helper function for trace_to_graph()
function trace_to_graph_recursion!(parent_id::Int, current::Vector{Any}, g::SimpleDiGraph, node_labels::Vector{String})
  n_funcs = length(current)
  # add all children (elements in current) to the graph
  for i=1:n_funcs
    # collect function as a node in the graph
    fname = string(current[i].first[1]) # function name
    aname = string(typeof.(current[i].first[2:end])) # tuple of typenames of arguments
    add_vertices!(g, 1) # add_vertex!(g) <- gets overriden by my add_vertex! function (julia quirk, kinda awful)
    current_id = nv(g)
    add_edge!(g, parent_id, current_id) # connect the i-th function to its parent
    push!(node_labels, fname*aname)
    # recurse to get to the child functions
    trace_to_graph_recursion!(current_id, current[i].second, g, node_labels)
  end
end

# recursively unpack the trace into a directed graph | used for visualization
function trace_to_graph(t::Trace)
  g = SimpleDiGraph(0, 0)
  node_labels = String[]
  # note: it is expected, that the highest level current only has a single entry, the top level function
  trace_to_graph_recursion!(-1, t.current, g, node_labels)
  return g, node_labels
end

# visualize a trace as a tree
function visualize(t::Trace)
  g, nodelabels = trace_to_graph(t)
  graphplot(g, names=nodelabels, method=:tree, nodeshape=:ellipse, markersize = 0.15, fontsize=5, size=(1200, 900), curves=false, show=true)
end

