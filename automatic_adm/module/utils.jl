

#################### Sparse matrix plotting ####################

using Plots
using LinearAlgebra # for I

function plot_spy(sparse_matrix)
  # with the default window scaling/size, a 100x100 matrix with markersize=2 will gives hole-less markers
  N = size(sparse_matrix)[1]
  ms = 100.0 / N * 2.0
  display(plot(spy(sparse_matrix, markersize=ms, markershape=:square))) # or just spy()
  #display(plot(spy(sparse_matrix, markersize=ms, markershape=:square, legend=nothing))) # or just spy()
end


function gen_sp(n)
  return sparse(Matrix(1.0I, n, n))
end

function test_spy(n)
  plot_spy(gen_sp(n))
  return nothing
end
