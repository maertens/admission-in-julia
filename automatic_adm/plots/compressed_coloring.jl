
include("../module/recipe.jl")
include("../module/shock_tube.jl")

using SparseArrays


function rgb_float_to_int(c)
  r = Int(floor(c.r >= 1.0 ? 255 : c.r * 256.0))
  g = Int(floor(c.g >= 1.0 ? 255 : c.g * 256.0))
  b = Int(floor(c.b >= 1.0 ? 255 : c.b * 256.0))
  return (r, g, b)
end


function write_tikz(mat, cmat, filename; cm=5)
  io = open(filename, "w")
  write(io, "\\begin{tikzpicture}\n")

  size_cm = cm
  elem_size = size_cm / mat.m # assume m=n, #cols=#rows

  # outside bracket
  buffer = elem_size # 3 buffer sizes long 'corner indents'
  write(io, "\\draw[black, very thick] (-$buffer, -$buffer) -- (-$buffer, $(size_cm+buffer));\n")
  write(io, "\\draw[black, very thick] ($(size_cm+buffer), -$buffer) -- ($(size_cm+buffer), $(size_cm+buffer));\n")
  write(io, "\\draw[black, very thick] (-$buffer, -$buffer) -- ($(2*buffer), -$buffer);\n")
  write(io, "\\draw[black, very thick] (-$buffer, $(size_cm+buffer)) -- ($(2*buffer), $(size_cm+buffer));\n")
  write(io, "\\draw[black, very thick] ($(size_cm+buffer), -$buffer) -- ($(size_cm-2*buffer), -$buffer);\n")
  write(io, "\\draw[black, very thick] ($(size_cm+buffer), $(size_cm+buffer)) -- ($(size_cm-2*buffer), $(size_cm+buffer));\n")

  for col=1:mat.n
    for idx in mat.colptr[col]:(mat.colptr[col+1]-1)
      row = mat.rowval[idx]
      # note: ticz starts lower left with its coordinate system
      upper_left = ((col-1)*elem_size, size_cm - (row-0)*elem_size)
      upper_right = (upper_left[1]+elem_size, upper_left[2]+elem_size)
      red, green, blue = rgb_float_to_int(mat.nzval[idx]) # mat.nzval[idx].r, mat.nzval[idx].g, mat.nzval[idx].b
      # to avoid tikz color-ratio-ing (i.e. (1,1,1) == (10,10,10) -> no light/dark-ness), define colors via xcolor first
      write(io, "\\definecolor{tempColor}{RGB}{$red,$green,$blue}\n")
      #color = "{rgb:red,$(red);green,$(green);blue,$(blue)}"# "orange"
      #color = "{red!$(red)!green!$(green)!blue!$(blue)!}"# "orange"
      write(io, "\\draw[black, thick, fill=tempColor] $upper_left rectangle $upper_right;\n")
    end
  end


  ##### compressed matrix #####

  # connecting arrow
  write(io, "\\draw[<-, black, ultra thick] ($(1.2*size_cm), $(0.45*size_cm)) -- ($(1.4*size_cm), $(0.45*size_cm)) node [midway, below=0.1cm, fill=white] {Reconstruct};\n")
  write(io, "\\draw[->, black, ultra thick] ($(1.2*size_cm), $(0.55*size_cm)) -- ($(1.4*size_cm), $(0.55*size_cm)) node [midway, above=0.1cm, fill=white] {Compress};\n")
  
  # right matrix offset
  x_offset = 1.6*size_cm
  # size of compressed matrix
  x_length = elem_size*(cmat.n+0) # n=cols

  # compressed matrix | brackets
  write(io, "\\draw[black, very thick] ($(x_offset-buffer), -$buffer) -- ($(x_offset-buffer), $(size_cm+buffer));\n")
  write(io, "\\draw[black, very thick] ($(x_offset+x_length+buffer), -$buffer) -- ($(x_offset+x_length+buffer), $(size_cm+buffer));\n")
  write(io, "\\draw[black, very thick] ($(x_offset-buffer), -$buffer) -- ($(x_offset+2*buffer), -$buffer);\n")
  write(io, "\\draw[black, very thick] ($(x_offset-buffer), $(size_cm+buffer)) -- ($(x_offset+2*buffer), $(size_cm+buffer));\n")
  write(io, "\\draw[black, very thick] ($(x_offset+x_length+buffer), -$buffer) -- ($(x_offset+x_length-2*buffer), -$buffer);\n")
  write(io, "\\draw[black, very thick] ($(x_offset+x_length+buffer), $(size_cm+buffer)) -- ($(x_offset+x_length-2*buffer), $(size_cm+buffer));\n")

  for col=1:cmat.n
    for idx in cmat.colptr[col]:(cmat.colptr[col+1]-1)
      row = cmat.rowval[idx]
      upper_left = (x_offset + (col-1)*elem_size, size_cm - (row-0)*elem_size)
      upper_right = (upper_left[1]+elem_size, upper_left[2]+elem_size)
      red, green, blue = rgb_float_to_int(cmat.nzval[idx]) #cmat.nzval[idx].r, cmat.nzval[idx].g, cmat.nzval[idx].b
      #color = "{rgb:red,$(red);green,$(green);blue,$(blue)}"
      #color = "{red!$(red)!green!$(green)!blue!$(blue)!}"
      write(io, "\\definecolor{tempColor}{RGB}{$red,$green,$blue}\n")
      write(io, "\\draw[black, thick, fill=tempColor] $upper_left rectangle $upper_right;\n")
    end
  end



  write(io, "\\end{tikzpicture}\n")
  close(io)
  return nothing
end



#= use RGB from ColorSchemes instead
struct rgb
  red::Int
  green::Int
  blue::Int
end
Base.zero(::Type{rgb}) = rgb(0,0,0)
to_rgb(x::Real) = rgb(1, 0, 0) # for structural matrix type conversion
Base.:+(lhs::rgb, rhs::rgb) = rgb(lhs.red+rhs.red, lhs.green+rhs.green, lhs.blue+rhs.blue)
=#
to_rgb(x::Real) = RGB{Float64}(x, 0.0, 0.0) 
Base.:+(lhs::RGB{Float64}, rhs::RGB{Float64}) = RGB(lhs.r+rhs.r, lhs.g+rhs.g, lhs.b+rhs.b)
Base.:*(lhs::Real, rhs::RGB{Float64}) = RGB(rhs.r*lhs, rhs.g*lhs, rhs.b*lhs)

function colorize(mat, colorscheme=:lighttest)
  csp = abs.(mat) .> 0
  coloring = compute_coloring(csp, "TAN")
  rp = compute_reconstruction_pattern(csp, coloring.seed)

  matrix = dropzeros!(sparse(to_rgb.(Matrix(mat))))
  compressed = spzeros(RGB, matrix.m, coloring.n_colors)
  # write pattern / compress matrix
  for j in eachindex(coloring.color_groups)
    compressed[:, coloring.color_groups[j]] .+= matrix[:, j]
  end

  # set colors of compressed matrix
  scheme = cgrad(colorscheme)
  colors = get(scheme, LinRange(0.0, 1.0, coloring.n_colors))
  for col=1:compressed.n
    for idx in compressed.colptr[col]:(compressed.colptr[col+1]-1)
      row = compressed.rowval[idx]
      min = 0.3
      fade = min*(compressed.m - row)/(compressed.m - 1) + (1 - min)
      compressed[row, col] = fade*colors[col]
    end
  end

  # reconstruct to broadcast colors
  for col=1:size(rp)[2]
    for row=1:size(rp)[1]
      if rp[row, col] != 0
        matrix[row, rp[row, col]] = compressed[row, col]
      end
    end
  end

  return matrix, compressed
end







# create a sparse test matrix
n = 50
bw = 8
a = sprand(n, n, 0.4)
for i=1:n
  left = max(0, i-bw)
  right= min(i+bw, n+1)
  for j=1:left
    a[i,j] = 0
  end
  for j=right:n
    a[i,j] = 0
  end
end
dropzeros!(a)


mat = a #sprand(3,3,1.0)
#matrix, compressed = colorize(mat)



# rhs! sparsity pattern as matrix
u = zeros(3*3*5); du = copy(u) # 15 cells
_, sp = analyze_dependencies(rhs!, (u, du))
matrix, compressed = colorize(sp[(2,1)], :rainbow1) # :lightrainbow  :viridis :thermal :rainbow1

filename = "./plots/tikz.tex"
write_tikz(matrix, compressed, filename)









#################################### sparsity pattern plotting ####################################

# based on write_tikz from above
function write_tikz_sparsity_pattern(pattern, filename; cm=5)
  io = open(filename, "w")
  write(io, "\\begin{tikzpicture}\n")

  size_cm = cm
  elem_size = size_cm / pattern.m # assume m=n, #cols=#rows

  # outside bracket
  buffer = elem_size # 3 buffer sizes long 'corner indents'
  write(io, "\\draw[black, very thick] (-$buffer, -$buffer) -- (-$buffer, $(size_cm+buffer));\n")
  write(io, "\\draw[black, very thick] ($(size_cm+buffer), -$buffer) -- ($(size_cm+buffer), $(size_cm+buffer));\n")
  write(io, "\\draw[black, very thick] (-$buffer, -$buffer) -- ($(2*buffer), -$buffer);\n")
  write(io, "\\draw[black, very thick] (-$buffer, $(size_cm+buffer)) -- ($(2*buffer), $(size_cm+buffer));\n")
  write(io, "\\draw[black, very thick] ($(size_cm+buffer), -$buffer) -- ($(size_cm-2*buffer), -$buffer);\n")
  write(io, "\\draw[black, very thick] ($(size_cm+buffer), $(size_cm+buffer)) -- ($(size_cm-2*buffer), $(size_cm+buffer));\n")

  for col=1:pattern.n
    for idx in pattern.colptr[col]:(pattern.colptr[col+1]-1)
      row = pattern.rowval[idx]
      # note: ticz starts lower left with its coordinate system
      upper_left = ((col-1)*elem_size, size_cm - (row-0)*elem_size)
      upper_right = (upper_left[1]+elem_size, upper_left[2]+elem_size)
      write(io, "\\draw[black, thick, fill=gray] $upper_left rectangle $upper_right;\n")
    end
  end

  write(io, "\\end{tikzpicture}\n")
  close(io)
  return nothing
end



############## numerical (not structural) sparsity of the test case functions at t=0.2 ##############

#_, sp = analyze_dependencies(SSPRK3!, (u,))
#pattern = sp[(1,1)]
u = zeros(3*3*10)
set_initial_conditions!(u); plotloop(u, 0)
jac = jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(32))
pattern = sparse(abs.(jac) .> 0)

filename = "./plots/sparsity_pattern_dt_const.tex"
write_tikz_sparsity_pattern(pattern, filename)

#_, sp = analyze_dependencies(SSPRK3_dt!, (u,))
#pattern = sp[(1,1)]
set_initial_conditions!(u); plotloop(u, 0)
jac = jacobian(Forward, SSPRK3_dt!, (u,), Val(1), Val(1), Val(32))
pattern = sparse(abs.(jac) .> 0)

filename = "./plots/sparsity_pattern_dt_varying.tex"
write_tikz_sparsity_pattern(pattern, filename)


