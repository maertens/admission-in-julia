#set terminal pngcairo size 800,400 enhanced
#set terminal tikz size 8cm,4cm font ",7"


set terminal tikz size 12cm,6cm font ",7" # presentation


set key top left

#set yrange [0:1.2]
set grid lt 0 lw 1

set logscale x 2
set logscale y 10

set xlabel "Number of cells N"
set ylabel "Mean runtime [ms]" offset 2,0

# dense recipe comparisons for dt=const function (including sparse/compressed outside accumulation functions)
#set output "runtime_const_dt_dense.png"
set output "runtime_const_dt_dense.tex"
plot "./bench_data/runtime_const_dt.dat" u 1:2 w l title "Jacobian Forward" lw 2,\
     "./bench_data/runtime_const_dt.dat" u 1:3 w l title "Jacobian Reverse" lw 2,\
     "./bench_data/runtime_const_dt.dat" u 1:4 w l title "Compressed Forward" lw 2,\
     "./bench_data/runtime_const_dt.dat" u 1:5 w l title "Compressed Reverse" lw 2,\
     "./bench_data/runtime_const_dt.dat" u 1:6 w l title "Dense Recipe" lc "blue" lw 2,\
     x*x/400 w l title "Quadratic" lc "grey50" lw 2 dashtype 2,\
     x/10 w l title "Linear" lc black lw 2 dashtype 2


# dense recipe comparison for dt!=const function (including sparse/compressed outside accumulation functions)
#set output "runtime_varying_dt_dense.png"
set output "runtime_varying_dt_dense.tex"
plot "./bench_data/runtime_varying_dt.dat" u 1:2 w l title "Jacobian Forward" lw 2,\
     "./bench_data/runtime_varying_dt.dat" u 1:3 w l title "Jacobian Reverse" lw 2,\
     "./bench_data/runtime_varying_dt.dat" u 1:4 w l title "Compressed Forward" lw 2,\
     "./bench_data/runtime_varying_dt.dat" u 1:5 w l title "Compressed Reverse" lw 2,\
     "./bench_data/runtime_varying_dt.dat" u 1:6 w l title "Dense Recipe" lc "blue" lw 2,\
     x*x/400 w l title "Quadratic" lc "grey50" lw 2 dashtype 2
#     x/100 w l title "Linear" lc black lw 2 dashtype 2




# sparse recipe comparison for dt=const including custom schedule
#set output "runtime_const_dt_sparse.png"
set output "runtime_const_dt_sparse.tex"
plot "./bench_data/runtime_const_dt.dat" u 1:2 w l title "Jacobian Forward" lw 2,\
     "./bench_data/runtime_const_dt.dat" u 1:4 w l title "Compressed Forward" lw 2,\
     "./bench_data/runtime_const_dt.dat" u 1:7 w l title "Sparse Recipe" lw 2,\
     x*x/400 w l title "Quadratic" lc "grey50" lw 2 dashtype 2,\
     x/30 w l title "Linear" lc black lw 2 dashtype 2
   
#    x*x/100 w l title "Quadratic" lc black lw 2 dashtype 2,\
#     x/100 w l title "Linear" lc black lw 2 dashtype 2


# sparse recipe comparison for dt=varying including custom schedule
#set output "runtime_varying_dt_sparse.png"
set output "runtime_varying_dt_sparse.tex"
plot "./bench_data/runtime_varying_dt.dat" u 1:2 w l title "Jacobian Forward" lw 2,\
     "./bench_data/runtime_varying_dt.dat" u 1:4 w l title "Compressed Forward" lw 2,\
     "./bench_data/runtime_varying_dt.dat" u 1:7 w l title "Sparse Recipe" lw 2,\
     "./bench_data/runtime_varying_dt.dat" u 1:9 w l title "Sparse Recipe+" lw 2,\
     x*x/300 w l title "Quadratic" lc "grey50" lw 2 dashtype 2,\
     x/20 w l title "Linear" lc black lw 2 dashtype 2

#     "./bench_data/runtime_varying_dt.dat" u 1:6 w l title "Dense Recipe" lw 2,\
#     "./bench_data/runtime_varying_dt.dat" u 1:8 w l title "Dense Recipe+" lw 2,\
