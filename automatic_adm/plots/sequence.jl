

# taken an admission output schedule and get a latex align block

include("../module/annotation.jl")



function texify(schedule, vertex_sizes)
  result = "\\begin{align*}\n"
  s = parse_schedule(split(schedule, "\n"))
  jac_dict = Dict()

  for (op, mode, target, idx) in zip(s.operations, s.modes, s.targets, eachindex(s.operations))
    i, j = target[1], target[end] # (i, j) key gives dvj/dvi jacobian
    sizes = vertex_sizes[target[[1, end]]]

    target .-= 1 # decrement the increment from parse_schedule off again (0-indexed output)
    target_str = "($("$(target)"[2:end-1]))" # strip [] and add ()
    result *= "  F'$target_str"

    if haskey(jac_dict, (i,j))
      result *= " &\\mathrel{+}= " # += increment existing jacobian
    else
      jac_dict[(i,j)] = true
      result *= " &\\coloneqq\\,\\,\\, " # := assign to new jacobian | \,\,\, for alignment with +=
    end
    # TODO:  keep target / lhs F the same as currently? (full target is listed, not just in and output jacobian)

    if op=="ACC"
      if mode == "TAN"
        result *= "\\dot{F}$target_str \\cdot I_{$(sizes[1])}"
      else # ADJ
        result *= "I_{$(sizes[end])} \\cdot \\bar{F}$target_str"
      end

    else # op ELI
      if mode == "TAN"
        seed = "($("$(target[1:end-1])"[2:end-1]))"
        result *= "\\dot{F}$target_str \\cdot F'$seed"

      elseif mode =="ADJ"
        seed = "($("$(target[2:end])"[2:end-1]))"
        result *= "F'$seed \\cdot \\bar{F}$target_str"

      else # mode == MUL
        lhs = "($("$(target[1:2])"[2:end-1]))"
        rhs = "($("$(target[2:3])"[2:end-1]))"
        result *= "F'$lhs \\cdot F'$rhs"
      end # end mode
    end # end op

    if idx < length(s.operations); result *= " \\\\\n"
    else result *= "\n"; end
    end # end for op, mode, target
  
  result *= "\\end{align*}\n"
  return result
end













# greedy min fill schedule for the 0.5 cost random dag
schedule=
"ACC TAN (3 6) 34816
ELI ADJ (0 3 6) 217600
ACC TAN (1 6) 179928
ELI ADJ (0 1 6) 285600
ACC TAN (2 6) 155142
ELI ADJ (0 2 6) 265200
ACC TAN (4 6) 45968
ELI ADJ (0 4 6) 176800
ACC TAN (4 5) 63544
ELI TAN (4 5 9) 366600
ACC TAN (4 7) 99372
ELI TAN (4 7 9) 254800
ACC TAN (4 8) 50700
ELI TAN (4 8 9) 130000
ACC ADJ (0 4) 135200
ELI MUL (0 4 9) 260000
ACC TAN (6 9) 231200
ELI MUL (0 6 9) 340000"
vertex_sizes = [100, 42, 39, 32, 26, 47, 34, 49, 25, 100]


tex = texify(schedule, vertex_sizes)
println(tex)





################ schedules from other stuff


branch_and_bound_schedule=
"ACC TAN (0 1)
ACC TAN (0 2)
ACC TAN (0 4)
ACC TAN (0 6)
ELI TAN (0 1 2)
ELI TAN (0 1 4)
ELI TAN (0 1 6)
ELI TAN (0 2 3)
ELI TAN (0 3 4)
ELI TAN (0 4 5)
ELI TAN (0 3 6)
ELI TAN (0 5 6)"


baseline_schedule=
"ACC TAN (0 2)
ACC TAN (0 3)
ACC TAN (0 5)
ACC TAN (0 7)
ACC TAN (1 7)
ACC ADJ (0 1)
ELI TAN (0 1 3)
ELI TAN (0 1 5)
ELI TAN (0 2 3)
ELI TAN (0 2 5)
ELI TAN (0 2 7)
ELI TAN (0 3 4)
ELI TAN (0 4 5)
ELI TAN (0 5 6)
ELI TAN (0 4 7)
ELI TAN (0 6 7)
ELI MUL (0 1 7)"


delay_dt_schedule=
"ACC TAN (1 3)
ACC TAN (1 4)
ACC TAN (1 6)
ACC TAN (1 8)
ACC ADJ (1 2)
ACC TAN (2 4)
ACC TAN (2 6)
ACC TAN (2 8)
ELI TAN (1 3 8)
ELI TAN (1 3 4)
ELI TAN (1 3 6)
ELI TAN (1 4 5)
ELI TAN (1 5 6)
ELI TAN (1 5 8)
ELI TAN (1 6 7)
ELI TAN (1 7 8)
ELI TAN (2 4 5)
ELI TAN (2 5 6)
ELI TAN (2 5 8)
ELI TAN (2 6 7)
ELI TAN (2 7 8)
ELI MUL (1 2 8)"
# decrement all node indices, since we expect zero-start (admission output is zero indexed)
temp = collect(delay_dt_schedule) # convert to mutable vector
for i in eachindex(temp)
  if isdigit(temp[i])
    temp[i] -= 1
  end
end
delay_dt_schedule = String(temp)

n = 3*128
non_dt_vertex_sizes = [n, n, n, n, n, n, n]
varying_dt_vertex_sizes = [n, 1, n, n, n, n, n, n]

println("Branch and Bound (dense tangent) schedule for dt=const\n", texify(branch_and_bound_schedule, non_dt_vertex_sizes))
println("Banch and Bound (dense tangent) schedule for dt varying\n", texify(baseline_schedule, varying_dt_vertex_sizes))
println("Custom order to exploit dt sparsity\n", texify(delay_dt_schedule, varying_dt_vertex_sizes))

