#set terminal pngcairo size 800,400 enhanced
set terminal tikz size 8cm,4cm font ",7"

#set output "shock_tube_result_t02.png"
set output "shock_tube_result_t02.tex"



set key center left

set yrange [0:1.2]

set grid lt 0 lw 1
set xlabel "Grid coordinate x"
set ylabel "Solution quantity magnitude" offset -1,0

plot "./shock_tube_output_t02.tsv" u 1:2 w l title "Density" lw 2,\
     "./shock_tube_output_t02.tsv" u 1:3 w l title "Velocity" lw 2,\
     "./shock_tube_output_t02.tsv" u 1:4 w l title "Pressure" lw 2

# same for initial conditions
set output "shock_tube_result_t00.tex"
plot "./shock_tube_output_t00.tsv" u 1:2 w l title "Density" lw 2,\
     "./shock_tube_output_t00.tsv" u 1:3 w l title "Velocity" lw 2,\
     "./shock_tube_output_t00.tsv" u 1:4 w l title "Pressure" lw 2
