
# visualize the trace and dag to manually create tikz pictures

include("../module/recipe.jl")
include("../module/shock_tube.jl")

u = zeros(3*128); set_initial_conditions!(u)


##### try out more level dags
function dt_calc_wrapper(dt, u)
  return calc_dt!(dt, u)
end

function step1(y1, u, k1, dt)
  rhs!(u, k1)
  helper1_dt!(y1, u, k1, dt)
end

function step2(y2, y1, u, k1, k2, dt)
  rhs!(y1, k2)
  helper2_dt!(y2, u, k1, k2, dt)
end

function step3(y2, y1, u, k1, k2, k3, dt)
  rhs!(y2, k3)
  helper3_dt!(u, k1, k2, k3, dt)
end


function foo(u)
  k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  y1, y2 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  dt = zeros(eltype(u), 1) # init dt as vector
  dt_calc_wrapper(dt, u)
  step1(y1, u, k1, dt)
  step2(y2, y1, u, k1, k2, dt)
  step3(y2, y1, u, k1, k2, k3, dt)
  return nothing
end


# plot call tree / trace and the dag of the SSPRK3_dt! function
trace = trace_function(3, foo, (u,))
dag = tape(trace)
println("foo dag edges: ", length(dag.edges))
visualize(trace)
#dag_plot(dag)

println("two level ")

trace = trace_function(2, SSPRK3_dt!, (u,))
dag = tape(trace)
println("SSPRK3_dt! dag edges: ", length(dag.edges))



# using OrdinaryDiffEq
# f(u, p, t) = 1.01 * u
# u0 = 1 / 2
# tspan = (0.0, 1.0)
# prob = ODEProblem(f, u0, tspan)

# wrapper(problem) = solve(problem, Tsit5(), reltol = 1e-8, abstol = 1e-8)

# trace = trace_function(2, wrapper, (prob,))


