

include("..module/shock_tube.jl")
include("..module/annotation.jl")


println("======================================== ANNOTATION TESTING ========================================")

u = zeros(3*128)
set_initial_conditions!(u)
trace = trace_function(2, SSPRK3!, (u,))
dag = tape(trace)


params = BenchmarkParams(1000, 10) # 1k samples or 10 seconds max
annotation = annotate_dag(dag, params)
# the matrix*vector factor is 37.4904 for size = (384, 384)
# => matches 512/64 AVX * 5GHZ = 40 FMA/nanosecond (I'm currently not clocked at 5ghz, but overall size checks out)

schedule = admission(annotation)



println("======================================== BENCHMARKS ================================================")
using BenchmarkTools
set_initial_conditions!(u)
plotloop(u, -1)
w = deepcopy(u)

println("============ Primal Benchmark ============")
@benchmark SSPRK3!(u) setup=(u[:] .= w)

println("=========== Jacobian Benchmark ===========") # note: since this is dense, it should be way slower
@benchmark jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16)) setup=(u[:] = w)

println("== Primal temporaries external Benchmark ==")


function zero_temps!(k1, k2, k3, y1, y2)
  k1[:] .= 0; k2[:] .= 0; k3[:] .= 0; y1[:] .= 0; y2[:] .= 0
  return nothing
end

function ext_SSPRK3!(u, k1, k2, k3, y1, y2)
  zero_temps!(k1, k2, k3, y1, y2)
  rhs!(u, k1)
  helper1!(y1, u, k1)
  rhs!(y1, k2)
  helper2!(y2, u, k1, k2)
  rhs!(y2, k3)
  helper3!(u, k1, k2, k3)
  return nothing
end

k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
y1, y2 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u))

@benchmark ext_SSPRK3!(u, k1, k2, k3, y1, y2) setup=(u[:] = w)




#= Notes
1) for the primal evaluation to not matter (collect primal results before, then execute ADM), its runtime should be very small compared to the jacobian runtime
--- result of the benchmarks:
comparing the minima of the benchmarks: 133.974 μs primal vs. 77.847 ms jacobian (518x more expensive, -> ~1.5x still for /384)
in general: primal is less important! (not that much later, considering sparse, but ok, since sparse will be hugely better than dense)


2) the inside allocation of temporaries runtime should be comparable to that of the outside allocation. Else the allocation dominates the runtime and skews jacobian comparisons, since most of the runtime is dead code (the allocations)
--- result of the benchmarks:
minimums: 133.974 μs ms jacobian with allocation, 131.077 μs μs without allocation => barely faster!



3)
#Reverse mode not re-interpreted. -> way higher cost than actually the case
# -> erledigt sich dann aber, wenn man zu sparse & real benchmarks geth (all single vector run suffices for sparse jacobian)


4)
# how to correctly measure time?
# -> run jacobian, then divide by size of input dimension
# but what chunk-size?does not matter! (should cancel out with the MVM benchmark time)
# -> all the same chunk size means the same speedup factor... ---> not so sure about that
# BUT: for a chunk!=1 we need to divide the cost by (length(in or out) / chunk) -> less evals needed
# just keep chunksize at 1 for now

=#





