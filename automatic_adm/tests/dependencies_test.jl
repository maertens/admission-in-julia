
include("../module/dependencies.jl")
include("../module/utils.jl")
using Test



#################### Initializers ####################

# test initializers
x = rand(3)
xt = track_vector(x)
yt = rand(Tracker{Float64}, 3)
zt = zeros(eltype(xt), 4)
track_vector(rand(7), collect(1:7)) # vector{Int} version
track_vector(rand(7), 1:7) # unit range version




#################### Basic pure assignment functions ####################

function bar(z,x)
  z .= x
  return nothing
end

function bar2(z,x)
  z[:] = x
  return nothing
end

# test tracking dependencies of functions
z, x = zeros(3), rand(3)
zt, xt = track_args(z, x)
seed_trackers(zt, xt)
bar(zt, xt) # will simply set the values, even the tracker values! -> require overload of .= for Tracker type


# one dependency from x to z with identity pattern
bar_correct_res = (sparse([1], [2], Bool[1], 2, 2), Dict{Any, Any}((1, 2) => sparse([1, 2, 3], [1, 2, 3], Bool[1, 1, 1], 3, 3)))
@test analyze_dependencies(bar, (z, x)) == bar_correct_res
@test analyze_dependencies(bar2, (z, x)) == bar_correct_res



#################### More complex dependency behavior ####################

# each z should depend on every y
function foo(z, x, y)
  z .= x .* minimum(y)
  return nothing
end

# x should depend on its own input seeding and y | y depends on nothing (not even its own input seeding, since unmodified)
function self_dep(x, y)
  x[:] = x .* x + y
  return nothing
end

# this function has asymmetry in the sparsity patterns (x and y depend on each other in different patterns)
function two_way(x, y)
  z = copy(x) # keep dependence of original x on y (next line overwrites input x)
  x[:] = 3*ones(eltype(y), length(y))*y[1] # also tests vec*scalar trackers and Real*vec of trackers
  y[:] = z .* z
  return nothing
end


# x should be one longer than z
function different_dims(z, x)
  for i in eachindex(z)
    z[i] += x[i] * x[i+1]
  end
  return nothing
end


# foo: first arg is only output, and depends on x (identity) and y (full) -> [1,2] == [1,3] == 1 rest is zero
foo_correct_res = (sparse([1, 1], [2, 3], Bool[1, 1], 3, 3), Dict{Any, Any}((1, 2) => sparse([1, 2, 3], [1, 2, 3], Bool[1, 1, 1], 3, 3), (1, 3) => sparse([1, 2, 3, 1, 2, 3, 1, 2, 3], [1, 1, 1, 2, 2, 2, 3, 3, 3], Bool[1, 1, 1, 1, 1, 1, 1, 1, 1], 3, 3)))
z, x , y = zeros(3), rand(3), rand(3)
@test analyze_dependencies(foo, (z, x, y)) == foo_correct_res

# self_dep: first arg x depends on x (identity) and y (identity) -> full row. second arg y is pure input -> zero row
self_dep_correct_res = (sparse([1, 1], [1, 2], Bool[1, 1], 2, 2), Dict{Any, Any}((1, 2) => sparse([1, 2, 3], [1, 2, 3], Bool[1, 1, 1], 3, 3), (1, 1) => sparse([1, 2, 3], [1, 2, 3], Bool[1, 1, 1], 3, 3)))
x , y = zeros(3), zeros(3)
@test analyze_dependencies(self_dep, (x, y)) == self_dep_correct_res


# two_way: first arg x depends on y (y[1] -> first column) and y depends on x (identity)
two_way_correct_res = (sparse([2, 1], [1, 2], Bool[1, 1], 2, 2), Dict{Any, Any}((1, 2) => sparse([1, 2, 3], [1, 1, 1], Bool[1, 1, 1], 3, 3), (2, 1) => sparse([1, 2, 3], [1, 2, 3], Bool[1, 1, 1], 3, 3)))
x , y = zeros(3), rand(3)
@test analyze_dependencies(two_way, (x, y)) == two_way_correct_res


# different_dims: overall pattern 2x2 one entry at [1,2] (z depends on x) and one at [1,1] (z depends on itself via +=, identity pattern)
#                 sparsity pattern of x->z should be identity + upper diagonal above that also ones (2x3 matrix, 4 entries == 1)
different_dims_correct_res = (sparse([1, 1], [1, 2], Bool[1, 1], 2, 2), Dict{Any, Any}((1, 2) => sparse([1, 1, 2, 2], [1, 2, 2, 3], Bool[1, 1, 1, 1], 2, 3), (1, 1) => sparse([1, 2], [1, 2], Bool[1, 1], 2, 2)))
z, x = rand(2), rand(3)
@test analyze_dependencies(different_dims, (z, x)) == different_dims_correct_res





#################### Full shock tube solver ####################

include("../module/shock_tube.jl")

u = zeros(3*N)
set_initial_conditions!(u)

# get sparsity pattern sp and list of argument-wise patterns li | inline nts parameter, since i can only deal with pure vector functions
sp, li = analyze_dependencies((u)->solve_RK3!(u, 1), (u,))

# visualize the patterns
println(sp) # should be 1x1 with the entry = 1 (self dependence of in/out variable u)
plot_spy(li[(1,1)]) # sparse matrix from u_in to u_out


# clearly, the band becomes thicker as the dependency spreads in every time-step
sp, li = analyze_dependencies((u)->solve_RK3!(u, 5), (u,))
plot_spy(li[(1,1)])


# test time-control solver version | the pattern is dense (!) since max makes every value (potentially) depend on the others
sp, li = analyze_dependencies((u)->solve_RK3_dt!(u, 1), (u,))
plot_spy(li[(1,1)])

