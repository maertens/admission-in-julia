
include("../module/shock_tube.jl")
include("../module/utils.jl")

using Enzyme
using BenchmarkTools


# try to differentiate the shock tube time steps with the simple jacobian method
# use this to check that my code is differentiable


function setup()
  n = 128
  u = zeros(3*n)
  set_initial_conditions!(u)
  return u
end

function wrapper_RK3!(u)
  w = copy(u) # input should be const (Enzyme forward jacobian re-uses same input args, I believe)
  SSPRK3!(w)
  return w
end

function wrapper_RK3_dt!(u)
  w = copy(u) 
  SSPRK3_dt!(w)
  return w
end

# works
# function test!(u)
#   SSPRK3!(u)
#   return u
# end

# function test!(u)
#   w = zeros(eltype(u), length(u))
#   w[:] = u
#   SSPRK3!(w)
#   return w
# end
# # function SSPRK3!(u)
# #   k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
# #   y1, y2 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
# #   rhs!(u, k1)
# #   helper1!(y1, u, k1)
# #   rhs!(y1, k2)
# #   helper2!(y2, u, k1, k2)
# #   rhs!(y2, k3)
# #   helper3!(u, k1, k2, k3)
# #   return nothing
# # end

# # function SSPRK3!(u)
# #   k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
# #   y1, y2 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
# #   return nothing
# # end

# function test!(u)
#   w = deepcopy(u)
#   SSPRK3!(w)
#   return w
# end





u = setup()

# compute vector mode Jacobians in forward and reverse mode | using wrapper_RK3!
println("Computing RK3 forward Jacobian")
jac_fwd = jacobian(Forward, wrapper_RK3!, u, Val(16)) # no shadow=onehot(u) in batch mode!
plot_spy(jac_fwd)
println("Computing RK3 backward Jacobian")
jac_bwd = jacobian(Reverse, wrapper_RK3!, u, Val(length(u)), Val(16)) # also seems to crash now
plot_spy(jac_bwd)

# compute vector mode Jacobians in forward and reverse mode | using wrapper_RK3_dt!
println("Computing RK3 dt control forward Jacobian")
jac_fwd = jacobian(Forward, wrapper_RK3_dt!, u, Val(16)) # no shadow=onehot(u) in batch mode!
plot_spy(jac_fwd)
println("Computing RK3 dt control backward Jacobian")
#jac_bwd = jacobian(Reverse, wrapper_RK3_dt!, u, Val(length(u)), Val(16)) # NOTE: this crashes for some reason
plot_spy(jac_bwd)

println("Benchmarking Enzyme Jacobian functions")
u = setup()
display(@benchmark wrapper_RK3!(u) setup=(u=setup()))
display(@benchmark jacobian(Forward, wrapper_RK3!, u, Val(16)) setup=(u=setup()))
#display(@benchmark jacobian(Reverse, wrapper_RK3!, u, Val(length(u)), Val(16)) setup=(u=setup())) # this crashes for some inexplicable reason
# look at https://enzyme.mit.edu/index.fcgi/julia/stable/api/#Enzyme.jacobian-Tuple{ForwardMode,%20Any,%20Any}

#=
Reverse and forward mode advance the primal solution state by 24 steps (tested on reverse, 384/16=24, checks out).
Together with longer runtimes in re-tests, it seems that reverse also just re-executes. But the source code of reverse jacobian is still a mystery.

=#


