
include("../module/shock_tube.jl")
include("../module/taping.jl")
include("../module/jacobian.jl")


u = zeros(3*128)
set_initial_conditions!(u)
w = deepcopy(u)
trace = trace_function(2, SSPRK3!, (u,))
dag = tape(trace)



println("======================================== PATH ELIMINATION JACOBIAN ========================================")
# ==> "Test of completeness of DAG" -> check if we have extracted all necessary information to run OUT OF CONTEXT (!! formulate like this) primal and jacobian calculations)

# we can compute the jacobian through applying the chain rule on the dag. In most simple terms, we just add up all jacobian chain products along all paths from input to output.
#= By hand, I get
dv2/dx = rhs!/dx
dv3/dx = helper1/dx
dv7/dx = helper3/dx
dv5/dx = helper2/dx

dv3/dx += helper1/dv2 * dv2/dx
dv4/dx = rhs/dv3 * dv3/dx
dv5/dx += helper2/dv4 * dv4/dx
dv5/dx += helper2/dv2 * dv2/dx
dv6/dx = rhs/dv5 * dv5/dx
dv7/dx += helper3/dv2 * dv2/dx
dv7/dx += helper3/dv4 * dv4/dx
dv7/dx += helper3/dv6 * dv6/dx

The sought jacobian is then fully accumulated in dv7/dx!
=#

## init all jacobians
# easy access to sizes by name | note: aLL the same size, also unneeded when not zero init!
# sizes = Dict()
# for i=1:dag.n_vertices
#   sizes[dag.vertex_labels[i]] = dag.vertex_sizes[i]
# end
# dv2dx = zeros(sizes["v2"], sizes["x"])
# dv3dx = zeros(sizes["v3"], sizes["x"])
# dv3dx = zeros(sizes["v3"], sizes["x"])


arg_ind_to_vals(edge) = (Val(edge.arg_ind.first), Val(edge.arg_ind.second))
function edge_jacobian(edge)
  return jacobian(Forward, edge.func, deepcopy(edge.args), arg_ind_to_vals(edge)..., Val(16)) # deepcopy important (I think)! no one should touch the primal args
end

# forward pass jacobian accumulation and multiplication | vertex elimination that corresponds to tangent mode
function vertex_elimination(dag)
  dv2dx =  edge_jacobian(dag.edges[1])        # dv2/dx =  rhs!/dx
  dv3dx =  edge_jacobian(dag.edges[2])        # dv3/dx =  helper1/dx
  dv3dx += edge_jacobian(dag.edges[3])*dv2dx  # dv3/dx += helper1/dv2 * dv2/dx
  dv4dx =  edge_jacobian(dag.edges[4])*dv3dx  # dv4/dx =  rhs/dv3 * dv3/dx
  dv5dx =  edge_jacobian(dag.edges[5])        # dv5/dx =  helper2/dx
  dv5dx += edge_jacobian(dag.edges[6])*dv2dx  # dv5/dx += helper2/dv2 * dv2/dx
  dv5dx += edge_jacobian(dag.edges[7])*dv4dx  # dv5/dx += helper2/dv4 * dv4/dx
  dv6dx =  edge_jacobian(dag.edges[8])*dv5dx  # dv6/dx =  rhs/dv5 * dv5/dx
  dv7dx =  edge_jacobian(dag.edges[9])        # dv7/dx =  helper3/dx
  dv7dx += edge_jacobian(dag.edges[10])*dv2dx # dv7/dx += helper3/dv2 * dv2/dx
  dv7dx += edge_jacobian(dag.edges[11])*dv4dx # dv7/dx += helper3/dv4 * dv4/dx
  dv7dx += edge_jacobian(dag.edges[12])*dv6dx # dv7/dx += helper3/dv6 * dv6/dx
  return dv7dx
end

dv7dx = vertex_elimination(dag)

set_initial_conditions!(u)
full_jac = jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16))
println("u unmodified: ", u == w)


# require non-modified input for the enzyme wrapper
function wrapper(u)
  w = zeros(eltype(u), length(u))
  w[:] = u
  SSPRK3!(w)
  return w
end
enzyme_forward = Enzyme.jacobian(Forward, wrapper, u, Val(16))
println("u unmodified: ", u == w)
# way to long to compile
#enzyme_reverse = Enzyme.jacobian(Reverse, wrapper, u, Val(length(u)), Val(16)) # takes like hours to compile
println("u unmodified: ", u == w)

println("Matching jacobians: ", dv7dx ≈ full_jac == enzyme_forward) # finally, this works so far!


#### testing of reverse mode
rev_jac = jacobian(Reverse, SSPRK3!, (u,), Val(1), Val(1), Val(16))
println("u unmodified: ", u == w)
println("Reverse mode equality: ", rev_jac ≈ full_jac)

#### testing of left-over jacobian chunk | 384/16 = 24, but 384/13 = 29 rest 7
full_jac2 = jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(13))
rev_jac2 = jacobian(Reverse, SSPRK3!, (u,), Val(1), Val(1), Val(13))
println("Handling of non evenly divisible sizes by chunk sizes: ", full_jac ≈ full_jac2 ≈ rev_jac ≈ rev_jac2)



println("======================================== BENCHMARKING ========================================")
using BenchmarkTools
##### benchmarking of my jacobian vs enzyme

function setup_u()
  set_initial_conditions!(u)
  return u
end
println("Benchmark of jacobian (forward)")
display(@benchmark jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16)) setup=(u=setup_u()))
println("Benchmark of Enzyme jacobian (forward)")
display(@benchmark Enzyme.jacobian(Forward, wrapper, u, Val(16))             setup=(u=setup_u())) # makes more allocations (wrapper issue i think)
println("Benchmark of vertex elimination (forward jacobian)")
display(@benchmark vertex_elimination(dag))



##### benchmarking of smaller changes

# re-executed these in REPL with changes in jacobian.jl
#display(@benchmark jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16)) setup=(u=setup_u())) # slightly faster
#display(@benchmark jacobian(Reverse, SSPRK3!, (u,), Val(1), Val(1), Val(16)) setup=(u=setup_u())) #

# BatchDuplicatedNoNeed instead of BatchDuplicated
#-> seems to have a slight benefit for forward mode -> keep
#-> reverse seems to be actually slower -> keep simple BatchDuplicated. (although measurement noise is quite large!) -> no, use NoNeed as in theory its better
# args::X with some type X -> seems to be doing little
# n_rows, n_cols as Val() pass -> wrapper around the jacobian that fills this out
### all are hard to measure with little samples, but keep these optimizations, as they are not-worse


println("======================================== TIME CONTROL JACOBIAN ERROR ========================================")

u = zeros(3*21*3) # divisible by 21 to allow for easy seeding | factor 3 for 3 values per cell, 21 for batches, 3 extra padding factor (larger u)

# want to determine the error at t=0.2
set_initial_conditions!(u)
plotloop(u, 1, 0.2)
w = deepcopy(u)

# NOTE: the bandwidth of the jacobian of the RK3 solver is 21, since the repeated calls to the rhs will propagate two additional neighbor dependencies on both sides
function diff_CPR(func, u)
  # CPR seeding for batch size 21 -> single eval gives entire jacobian
  seeding = zeros(length(u), 21)
  for i=1:21:length(u)
    seeding[i:i+20, :] = Matrix(1.0*I, 21, 21)
  end
  seeding_tpl = tuple_from_matrix(seeding)
  autodiff(Forward, func, Const, BatchDuplicatedNoNeed(u, seeding_tpl))
  
  # reverse the tuple-ization
  compressed_jacobian = zeros(length(u), 21)
  for i=1:21
    compressed_jacobian[:,i] = seeding_tpl[i]
  end

  # unpack the compressed jacobian
  Jacobian = zeros(length(u), length(u))

  for batch=1:9 # for every batch of columns (seeded by one batch 'location' in seed)
    first_col = (batch-1)*21 + 1
    last_col = first_col + 20 # not +21, since the +1 is in the first_col
    for col=first_col:last_col # every column in the batch (globally indexed)
      # for every column, get the row-span of the columns (index range of nonzeros)
      row_center_left = col - ((col-1) % 3) # band is in steps of 3 | left center means 9 values left/top and 11 values right/bottom
      span = max(1, row_center_left-9) => min(length(u), row_center_left+11) # min and max truncate the first and last few columns to matrix bounds
      # NOTE: this span is the same row-range found in the compressed jacobian
      # this is due to the fact, that the compressed jacobian is like a column shift/merge of the full once
      # -> row locations are preserved
      
      # the column counter for the compressed jacobian is wrapped around
      ccol = (col-1) % 21 + 1 # compressed column | -1 +1 to shift to null-indexed space to perform correct modulo wrapping

      # copy the column into the full jacobian
      Jacobian[span.first:span.second, col] = compressed_jacobian[span.first:span.second, ccol]
    end
  end

  return Jacobian, compressed_jacobian
end

# check jacobian correctness for fixed time-step
Jacobian, compressed_jacobian = diff_CPR(SSPRK3!, deepcopy(u))
true_jacobian = jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16))
println("Fixed dt (compressed) jacobian match: ", Jacobian ≈ true_jacobian) # there are two elements with machine precision deviation (assumed some internal shenanigans)
println("u unmodified: ", w == u)
spy(compressed_jacobian) # NOTE: nice picture
spy(Jacobian)

# test non-fixed time-step difference
Jacobian, compressed_jacobian = diff_CPR(SSPRK3_dt!, deepcopy(u))
true_jacobian = jacobian(Forward, SSPRK3_dt!, (u,), Val(1), Val(1), Val(16))
println("u unmodified: ", w == u)

max_abs_diff = maximum(abs.(Jacobian - true_jacobian))
println("Maximum absolute error: ", max_abs_diff)

function print_max_rel(tol)
  exclude_infs(val, ref) = if ref > tol; return abs.((val - ref)/ref); else return 0.0; end;
  max_rel_diff = maximum(exclude_infs.(Jacobian, true_jacobian))
  println("Maximum relative error (relative w.r.t. true jacobian values) of elements ", tol, ": ", max_rel_diff)
  return nothing
end
print_max_rel(1e-12)
print_max_rel(1e-6)
print_max_rel(1e-3)
print_max_rel(1e-1)

norm_rel_err = norm(true_jacobian - Jacobian, 2) / norm(true_jacobian, 2)
println("2-Norm relative error ||J - J_true|| / ||J_true|| = ", norm_rel_err)



###### NOTES FOR OBSIDIAN / LATER:


# my jacobian wrapper is faster due to several reasons:
# most importantly (I), also more convenient for our purposes!
# Enzyme jacobian functions:
# 1) they require input unmodified!
# 2) they need long to compile
# 3) reverse does not have seeding (also does tape-reevaluation, I think)
# -> for sparse seeding, comparable runtime between forward and backward and reduce compile time, I make my own wrapper.
# also able to differentiate w.r.t. to specific variables (enzyme i would need to create wrapper functions, but also possible) 
# 4) enzyme reverse jacobian has issues with deepcopy in wrapper of in-out function
# I) wrapper for in-out variable is more expensive than my code!!!!!
# II) need incremental jacobian function (pass jacobian in and add to it)
# ==============> many reasons for own function (could of course wrap enzyme for everything, but that is too slow for us (especially compile time))
# text-> Together, this yields that our implementation is faster than the enzyme jacobian for a given edge evaluation



# NOTE: the bandwidth of the jacobian of the RK3 solver is 21, since the repeated calls to the rhs will propagate two additional neighbor dependencies on both sides


# error between dt-dependency ignored jacobian and true jacobian is significant. Some entries changed a lot, overall 2-norm error is small, but noticeable
# -> while this error may be acceptable for some applications, it may cause convergence slowdown of inverse methods.
#    Using ADM it is (hopefully) possible to compute the true Jacobian without taking the dense jacobian performance hit.
# NOTE: possible extension/opt: H-Matrix jacobian (the dt part should not be multiplied to a dense matrix, keep Jacobian in H-matrix format)

