
include("../module/jacobian.jl")



function f(z, x, y)
  z[:] = (x .* y) .* x
  return nothing
end


Z= zeros(3)
X, Y = rand(3), rand(3)

z, x, y = copy(Z), copy(X), copy(Y)
dz = zeros(3)
dx = zeros(3); dx[1] = 1.0

# signature: (function, activity of return value, activity of all arguments...)
autodiff(Forward, f, Const, Duplicated(z, dz), Duplicated(x, dx), Const(y))


println("======================================== FORWARD TEST ========================================")

dz = tuple_from_matrix(zeros(3,3))
dx = chunkedonehot(x, Val(3))[1]
autodiff(Forward, f, Const, BatchDuplicated(z, dz), BatchDuplicated(x, dx), Const(y))
display(hcat(dz...))

# doc links:
# forward autodiff
# https://enzyme.mit.edu/index.fcgi/julia/stable/api/#EnzymeCore.autodiff-Union{Tuple{A},%20Tuple{FA},%20Tuple{RABI},%20Tuple{ForwardMode{RABI},%20FA,%20Type{A},%20Vararg{Any}}}%20where%20{RABI%3C:EnzymeCore.ABI,%20FA%3C:Annotation,%20A%3C:Annotation}
# reverse autodiff
#https://enzyme.mit.edu/index.fcgi/julia/stable/api/#EnzymeCore.autodiff-Union{Tuple{RABI},%20Tuple{ReturnPrimal},%20Tuple{A},%20Tuple{FA},%20Tuple{ReverseMode{ReturnPrimal,%20RABI},%20FA,%20Type{A},%20Vararg{Any}}}%20where%20{FA%3C:Annotation,%20A%3C:Annotation,%20ReturnPrimal,%20RABI%3C:EnzymeCore.ABI}


## This is my own function and it gives the same results!
z, x, y = copy(Z), copy(X), copy(Y)
jac = jacobian(Forward, f, (z, x, y), Val(2), Val(1), Val(3))
display(jac)


############# test the reverse mode | NOW WE SEED THE OUTPUT!
println("======================================== Reverse TEST ========================================")

#z, x, y = copy(Z), copy(X), copy(Y)
dz = chunkedonehot(x, Val(3))[1]
dx = tuple_from_matrix(zeros(3,3))
autodiff(Reverse, f, Const, BatchDuplicated(z, dz), BatchDuplicated(x, dx), Const(y))
display(hcat(dx...))

# my function
jac = jacobian(Reverse, f, (z, x, y), Val(2), Val(1), Val(3))
display(jac)




# benchmark the functions to see how big of a performance difference there is
println("======================================== BENCHMARKS ========================================")

z, x, y = copy(Z), copy(X), copy(Y)
dz = tuple_from_matrix(zeros(3,3))
dx = chunkedonehot(x, Val(3))[1]
display(@time autodiff(Forward, f, Const, BatchDuplicated(z, dz), BatchDuplicated(x, dx), Const(y)))
z, x, y = copy(Z), copy(X), copy(Y)
display(@time jacobian(Forward, f, (z, x, y), Val(2), Val(1), Val(3)))

using BenchmarkTools
N = 16
println("======================================== FORWARD ========================================")
### NOTE: the reset of dz and dx is important, else it will propagate additional information (factor 30 slower)
display(@benchmark autodiff(Forward, f, Const, BatchDuplicated(z, dz), BatchDuplicated(x, dx), Const(y)) setup=(z=rand(N);x=rand(N);y=rand(N);dz=tuple_from_matrix(zeros(N,N));dx = chunkedonehot(x, Val(N))[1]))
display(@benchmark jac=jacobian(Forward, f, (z, x, y), Val(2), Val(1), Val(N)) setup=(z=rand(N);x=rand(N);y=rand(N)))
### Result: (looked at median) factor 10 slower for N=3=chunksize, for N=8 and N=16 mine is about 5-6 times slower

println("======================================== REVERSE ========================================")
display(@benchmark autodiff(Reverse, f, Const, BatchDuplicated(z, dz), BatchDuplicated(x, dx), Const(y)) setup=(z=rand(N);x=rand(N);y=rand(N);dx=tuple_from_matrix(zeros(N,N));dz = chunkedonehot(x, Val(N))[1]))
display(@benchmark jac=jacobian(Forward, f, (z, x, y), Val(2), Val(1), Val(N)) setup=(z=rand(N);x=rand(N);y=rand(N)))
# Result: (looked at median) reverse is factor 3-4 slower for N=16


#=
Tests: (maybe)
1) enzyme jacobian vs my jacobian for a) one batch, b) multiple batches
2) individual jacobians of argument relations vs all at once (also one input (tangent) or output(adjoint) w.r.t. to all in/outputs)
3) Val{} of argument indices faster, since full precompile possible?
4) Self-dependence derivative. The input should be identity! (should jacobian function handle this, or should the outside caller handle it?)
=#

