

include("../module/shock_tube.jl")
include("../module/recipe.jl")


println("======================================== RECIPE EXECUTION TEST ========================================")

u = zeros(3*128)
set_initial_conditions!(u)
w = deepcopy(u)

recipe = adm_generate(SSPRK3!, (u,), ADmConfig(depth=2, bench_params=BenchmarkParams(10, 1)))
println("u unmodified: ", u == w)

jac = copy(adm_jacobian(recipe)) # without the copy, 'jac' changes with changes to recipe.jacobian[1] via future evaluations below

display(jac)
display(spy(jac))

ref_jac = jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16))
println("ADM jacobian matches dense tangent jacobian: ", jac ≈ ref_jac)
println("u unmodified: ", u == w)




##### test the primal+jacobian functionality

adm_jac1 = copy(adm_jacobian_with_primal!(recipe, (u,))) # modifies u
adm_jac2 = copy(recipe(recipe.func, u)) #adm_jacobian_with_primal!(recipe, (u,))
adm_u_after = deepcopy(u)
u[:] .= w # reset u
ref_jac1 = jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16))
SSPRK3!(u) # manually advance u
ref_jac2 = jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16))
SSPRK3!(u)
ref_u_after = deepcopy(u)
u[:] .= w
println("Equality of first jacobians:  ", adm_jac1 ≈ ref_jac1)
println("Equality of second jacobians: ", adm_jac2 ≈ ref_jac2)
println("Equality of primal result:    ", adm_u_after == ref_u_after)




##### test jacobian recipe wrapper

function n_steps_jacobian!(jacobian_func, n, u)
  J = Matrix(1.0I, length(u), length(u))
  for i=1:n
    J *= jacobian_func(SSPRK3!, u)
  end
  return J
end

function jac_wrapper(func, arg)
  J = jacobian(Forward, SSPRK3!, (arg,), Val(1), Val(1), Val(16))
  func(arg)
  return J
end

n_steps = 10
adm_j_steps = n_steps_jacobian!(recipe, n_steps, u)
adm_u_steps = deepcopy(u)
u[:] .= w
ref_j_steps = n_steps_jacobian!(jac_wrapper, n_steps, u)
ref_u_steps = deepcopy(u)
u[:] .= w
println("Equality of jacobian after ", n_steps, " steps:      ", adm_j_steps ≈ ref_j_steps)
println("Equality of primal result after ", n_steps, " steps: ", adm_u_steps == ref_u_steps)




using BenchmarkTools
n_samples = 1e7 # let seconds dominate
seconds = 10

# comparison of primal, traced primal and retraced primal
println("\n========================================================================\nPrimal benchmark:")
display(@benchmark SSPRK3!(u) setup=(u[:] .= w) samples=n_samples seconds=seconds) # min~131.880 μs , med~150.244 μs
println("\n========================================================================\nTracing benchmark:")
display(@benchmark trace_function(2, SSPRK3!, (u,)) setup=(u[:] .= w) samples=n_samples seconds=seconds) # min~166.746 μs , med~183.849 μs
println("\n========================================================================\nRe-Tracing benchmark:")
display(@benchmark primal_with_trace!(recipe.retrace, recipe.func, (u,)) setup=(u[:] .= w) samples=n_samples seconds=seconds) # min~132.000 μs , med~150.335 μs
# => barely any difference between retrace and primal

# comparison of ADM, ADM+primal, and pure jacobian functions
println("\n========================================================================\nADM jacobian accumulation benchmark:")
display(@benchmark adm_jacobian(recipe) samples=n_samples seconds=seconds) # min~107.551 ms , med~128.015 ms
println("\n========================================================================\nADM primal retrace + jacobian accumulation benchmark:")
display(@benchmark recipe(recipe.func, u) setup=(u[:] .= w) samples=n_samples seconds=seconds) # min~105.944 ms , med~127.164 ms
println("\n========================================================================\nDense Forward jacobian accumulation benchmark:")
display(@benchmark jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16)) setup=(u[:] .= w) samples=n_samples seconds=seconds) # min~79.601 ms , med~91.698 ms







# ================================ NOTES ================================
#=
1) the recipe is trimmed to a single argument input of the primal function
   allowing for arbitrary arguments would require a function that out of context executes the primal and refreshes the dag edges args
   or alternatively the expensive route is to re-trace everything and then write the trace args into the dag edges args
   The out of context execution is a bit tricky, since we do not save all functions (e.g. k=zeros(...) -> k unknown at input of a tracked function)

2) tracing is less than 2x the cost of the primal -> affordable to re-trace!

==> I implemented a retrace


3) ADM is slower than pure tangent (as expected from casts) -> the cost ratio (expected cost ratio to real cost ratio) could be explored

colpack: J*I -> compress J*I*P with P decided for the sparsity pattern of J
for seeding a matrix S => J*S*P => P decided for the sparsity pattern of J*S
-> decompress S, then recompress with seeding P, AD eval for compressed J, then decompress J and store/increment´


=#
