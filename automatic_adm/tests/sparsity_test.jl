
include("../module/recipe.jl") # includes all other stuff
include("../module/shock_tube.jl")

using BenchmarkTools

################################ basic compression reconstruction tests ################################

u = zeros(3*128)
set_initial_conditions!(u)
trace = trace_function(2, SSPRK3!, (u,))
dag = tape(trace)
w = deepcopy(u)

sp = dag.edges[1].pattern

coloring = compute_coloring(sp, "TAN")
rp =  compute_reconstruction_pattern(sp, coloring.seed)

#### test reconstruction methods
A = deepcopy(1.0*sp); # stand in for an edge evaluation (A*x is tangent eval of A with seed x)
A.nzval .= rand(length(A.nzval));
target = deepcopy(A); target .= 0.0
reconstruct!(target, A*coloring.seed, rp)
println("Compression-Reconstruction invariance: ", A == target)
target .= 0.0
reconstruct!(target, A*coloring.seed, sp, coloring.color_groups)
println("Color group based reconstruction works: ", A == target)


target .= 0.0
sparse_rp = sparse(rp)
reconstruct!(target, A*coloring.seed, sparse_rp)
println("Compression-reconstruction with sparse reconstruction pattern works: ", A == target)

#### test jacobian compression method
println("Jacobian compression working: ", coloring.seed == compress_jacobian_seed(sparse(1.0*I, size(A)...), coloring.color_groups, coloring.n_colors))


#### test reconstruct((A*J)*S) == reconstruct(A*(J*S)), i.e. can use compression of combined sparsity pattern (seed jacobian * seed)
spJ = copy(sp) # let the seed jacobian sparsity pattern be the same as the edge sparsity pattern
J = deepcopy(1.0*sp); J.nzval .= rand(length(J.nzval))
combined_pattern = sp*spJ .> 0
combined_coloring = compute_coloring(combined_pattern, "TAN")
target1 = A*J; target1 .= 0.0
reconstruct!(target1, (A*J)*combined_coloring.seed, combined_pattern, combined_coloring.color_groups) # treat edge*jacobian as one jacobian to accumulate
target2 = A*J; target2 .= 0.0
cJs = compress_jacobian_seed(J, combined_coloring.color_groups, combined_coloring.n_colors)
reconstruct!(target2, A*cJs, combined_pattern, combined_coloring.color_groups) # first compress the jacobian seed, then evaluate the edge model with it
println("Jacobian as seed compression-reconstruction test: ", A*J == target1 == target2)



################################ sparse recipe jacobian accumulation tests ################################


config = ADmConfig(mode="both", depth=2, bench_params=BenchmarkParams(10, 1), elim_method="GreedyMinFill", chunk=16)
dense_recipe, sparse_recipe = adm_generate(SSPRK3!, (u,), config)
println("u unmodified: ", u == w)

jac_sparse = copy(sparse_recipe(sparse_recipe.func, u)); u[:] .= w
jac_dense  = copy(dense_recipe(dense_recipe.func, u));  u[:] .= w
ref_jac = jacobian(Forward, SSPRK3!, (u,), Val(1), Val(1), Val(16))
println("ADM sparse, dense and dense tangent jacobian match: ", jac_sparse ≈ jac_dense ≈ ref_jac)



# used to compare dense and sparse intermediate jacobians to discover problems
function compare_steps(dense, sparse, u)
  for jac in dense.jacobians; jac .= 0; end;
  for jac in sparse.jacobians; jac .= 0; end;  
  for (fd, fs) in zip(dense.schedule, sparse.schedule)
    println("SCHEDULE STEP-------------------")
    fd(); fs()
    for (jd, js) in zip(dense.jacobians, sparse.jacobians)
      println(jd ≈ js)
    end
  end
  return nothing
end


function n_steps_jacobian!(jacobian_func, n, u)
  J = Matrix(1.0I, length(u), length(u))
  for i=1:n
    J *= jacobian_func(SSPRK3!, u)
  end
  return J
end

function forward(func, arg)
  J = jacobian(Forward, SSPRK3!, (arg,), Val(1), Val(1), Val(config.chunk))
  func(arg)
  return J
end
function reverse(func, arg)
  J = jacobian(Reverse, SSPRK3!, (arg,), Val(1), Val(1), Val(config.chunk))
  func(arg)
  return J
end


n_steps = 10
u[:] .= w
adm_j_steps = n_steps_jacobian!(sparse_recipe, n_steps, u)
adm_u_steps = deepcopy(u); u[:] .= w
ref_j_steps = n_steps_jacobian!(forward, n_steps, u)
ref_u_steps = deepcopy(u); u[:] .= w
println("Equality of jacobian after      ", n_steps, " steps: ", adm_j_steps ≈ ref_j_steps)
println("Equality of primal result after ", n_steps, " steps: ", adm_u_steps == ref_u_steps)




#### test compressed accumulation functions

acc_fwd = CAccumulate(SSPRK3!, u, Forward)
acc_bwd = CAccumulate(SSPRK3!, u, Reverse)
J_fwd = acc_fwd(u); u[:] .= w
J_bwd = acc_fwd(u); u[:] .= w
println("Correctness of global compressed jacobian accumulation: ", ref_jac ≈ J_fwd ≈ J_bwd)


######################################################## Benchmarks ########################################################

#### benchmark reconstruct!() versions
samples = 1e7
seconds = 1 # 100
input = A*coloring.seed
display(@benchmark reconstruct!(target, input, rp) setup=(target .= 0.0; A.nzval .= rand(length(A.nzval)); input = A*coloring.seed;) samples=samples seconds=seconds)
display(@benchmark reconstruct!(target, input, sp, coloring.color_groups) setup=(target .= 0.0; A.nzval .= rand(length(A.nzval)); input = A*coloring.seed;) samples=samples seconds=seconds)
display(@benchmark reconstruct!(target, input, sparse_rp) setup=(target .= 0.0; A.nzval .= rand(length(A.nzval)); input = A*coloring.seed;) samples=samples seconds=seconds)
#= NOTE: surprisingly, the dense reconstruction pattern is a tad faster
This could be due to the density and no gaps in the test case compressed pattern.
As a result the branch prediction works well and the dense pattern matrix is less storage than the sparse one.
-> Use the dense for our case, since this benchmark should be representative.
Mention sparse and color groups method in the report as options.
=#


#### benchmark sparse vs dense recipes
println("==============================================================================")
println("========================== Sparse recipe benchmarks ==========================")
println("==============================================================================")
println("Forward dense jacobian: ")
display(@benchmark forward(sparse_recipe.func, u) setup=(u[:] .= w) samples=samples seconds=seconds)
println("Reverse dense jacobian: ")
display(@benchmark reverse(sparse_recipe.func, u) setup=(u[:] .= w) samples=samples seconds=seconds)
println("Dense recipe:")
display(@benchmark dense_recipe(dense_recipe.func, u) setup=(u[:] .= w) samples=samples seconds=seconds)
println("Sparse recipe:")
display(@benchmark sparse_recipe(sparse_recipe.func, u) setup=(u[:] .= w) samples=samples seconds=seconds)
println("Color-compressed jacobian forward:")
display(@benchmark acc_fwd(u) setup=(u[:] .= w) samples=samples seconds=seconds)
println("Color-compressed jacobian reverse:")
display(@benchmark acc_bwd(u) setup=(u[:] .= w) samples=samples seconds=seconds)

# used to compare default multiply! with one that uses sparse mul! under the hood
# display(@benchmark sparse_recipe(sparse_recipe.func, u) setup=(u[:] .= w) samples=1e7 seconds=200)

#=
Sparse luckily way faster than dense. (note: 10x slower than currently for explicit mul! use, so sparse multiplication methods are important)
!!! Although note also 15x less allocations, so memory management is definitely something to look out for in future designs.
Future work: profiling of these methods should tell how much of the runtime is actual AD evaluation and how much is sparse MMM etc. overhead.

=#
