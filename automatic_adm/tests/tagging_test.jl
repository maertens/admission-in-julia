include("../module/tagged_tracing.jl")



####################################################################################################################################################
########################################################## TAGGING IS DEPRECATED / UNUSED ##########################################################
####################################################################################################################################################



function tag_g(z, x)
  z[:] = x .* x
  return nothing
end
function tag_f(y, x)
  z = zeros(eltype(y), length(y))
  tag_g(z, x) # z should not have a tag at enter!()
  y[:] = z # does this preserve the output y tag? | [:] does
  # y .= z # this calls with arguments (identity, z), the former not having a tag! ||| -> do not use this for now (and probably for ever)!
  return nothing
end

x, y = rand(3), zeros(3)
trace = tagged_tracing(tag_f, (y, x), depth=2, argnames=("y", "x"))



#= (3)
argument modification through new tuple in overdub yields that the tag is not visible to the next function!
i.e.
prehook, overdub, posthook func1 -> new tag only visible inside this func1 overdub
prehook, overdub, posthook func2 -> does not have the tag when it uses the same argument!
=> we would require an argument overwriting procedure.
Although i am sure this can be somehow realized elegantly, I want a working code.
==> use object identity to establish flow of vector variables!
  Let taping handle the check for "do i know this variable from somewhere?"
=#


#= (1)
note: a crucial issue with our system may be all the unnecessary shit that is also tagged and considered in the call tree
=> that is a drawback of call tree based dag generation. In the future, dags should be based on some sort of static source code / IR analysis to get good points of active variables. then hook the adm stuff into that part.
Marking certain functions as high-level functions (fallback, do not trace further), e.g. zeros, eltype, length etc. would be required for more arbitrary code.
This is in line with the tree pruning idea, where the trace is processed before taping the dag from it.
(side note: functions with symbolic tangent and adjoint models should should as leaf nodes and not recursed further in)

=#


# how to handle eltype() and length() calls on active variables? -> should produce no edges in dag


