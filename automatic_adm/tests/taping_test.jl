

include("../module/taping.jl")

################## test functions ##################

# dot product
function h1(z, x, y)
  z[1] = x'*y
  return nothing
end
# element wise product
function h2(z, x, y)
  z[:] += x .* y
  return nothing
end
# scalar times vector
function h3(z, x, y)
  z[:] = y[1] .* x
  return nothing
end

# z is pure output, x is pure input
function g(z, x)
  v = copy(x)
  w = zeros(eltype(x), 1)
  h1(w, x, x)
  h2(v, w, x)
  h3(z, v, w)
  return nothing
end

function g2(z, x)
  h2(z, x, x)
  return nothing
end

# one level higher
function f(z, x)
  g(z, x)
  g2(z, z)
  return nothing
end



################## perform a basic test ##################

function h(z, x)
  z[:] = x .* x
  return nothing
end
function simple(z, x)
  h(z, x)
  h(z, z)
  return nothing
end

z, x = zeros(3), rand(3)
trace = trace_function(2, simple, (z, x))
dag = tape(trace)
dag_plot(dag)


################## depth 1 DAG from g ##################

z = zeros(3)
x = rand(3)
trace = trace_function(2, g, (z, x))
dag = tape(trace)
dag_plot(dag)


################## depth 2 DAG from f ##################

z = zeros(3)
x = rand(3)
trace = trace_function(3, f, (z, x))
dag = tape(trace)
dag_plot(dag)


################## shock tube RK3 test ##################

include("../module/shock_tube.jl")


function helper1(y, u, k1)
  y[:] = u + k1.*dt
  return nothing
end
function helper2(y, u, k1, k2)
  y[:] = u + 0.25*dt.*(k1+k2)
  return nothing
end
function helper3(y, k1, k2, k3)
  y[:] += dt*(1/6*k1 + 1/6*k2 + 2/3*k3)
  return nothing
end


# simpler SSPRK3 function that includes less arithmetic operation function calls
function SSPRK3!(u)
  k1, k2, k3 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  y1, y2 = zeros(eltype(u), length(u)), zeros(eltype(u), length(u))
  rhs!(u, k1)
  helper1(y1, u, k1)
  rhs!(y1, k2)
  helper2(y2, u, k1, k2)
  rhs!(y2, k3)
  helper3(u, k1, k2, k3)
  return nothing
end


u = zeros(3*128)
set_initial_conditions!(u)
trace = trace_function(2, SSPRK3!, (u,))
dag = tape(trace)
dag_plot(dag)




#= Notes on current bugs / restrictions
1) h1(w,x,x) produces two edges from x to w -> these should be merged if possible (and their sparsity patterns then too)
2) v = copy(x) does not track the dependency, since copy(x) only has one input which remains unmodified -> v appears out of nowhere
(the no returns restriction is quite harsh it seems, but i can just write my code in that way)

Currently, as long as my intended use-case works, I am fine with leaving things as is.
=#


# note: a heuristic for (maybe) good pruning of the trace / call tree could be to have a minimum reverse depth. For example, if a function only has three lower levels, then do not go deeper. -> Avoid low level calls in high level functions. E.g. when an assignment is called in a higher level function, then we do not trace through that!



