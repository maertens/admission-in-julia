
include("../module/tracing.jl")

# test functions to trace
h(a, b) = a+b
f(x, y, z) = x*y + y*h(z,x)

# trace the function
x, y, z = rand(3)
trace = trace_function(3, f, (x,y,z))
visualize(trace)


# trace the Cassette docs example function
f(x, y, z) = x*y + y*z
trace = trace_function(3, f, (x,y,z))
# returns `true`
trace.current == Any[
    (f,x,y,z) => Any[
        (*,x,y) => Any[(Base.mul_float,x,y)=>Any[]]
        (*,y,z) => Any[(Base.mul_float,y,z)=>Any[]]
        (+,x*y,y*z) => Any[(Base.add_float,x*y,y*z)=>Any[]]
    ]
]

