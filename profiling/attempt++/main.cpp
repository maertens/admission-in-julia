#include <iostream>
#include <vector>
#include <array>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <string>
#include <functional>
#include <type_traits>
#include <tuple>
#include <utility>
#include <cmath>

#include <fstream>
#include <ostream>
#include <sstream>

#include <dco.hpp>


/* Contains several successively more complex stages of code in my attempt to work towards a profiling mechanism. */


template<typename CTpl>
std::vector<CTpl> foo(std::vector<CTpl> const& x) {
  //return std::vector<CTpl> {std::accumulate(x.begin(), x.end(), 0.0)}; // gives conversion warnings if called on float
  CTpl sum = 0.0;
  for(int i = 0; i < x.size(); i++) sum += x[i];
  return std::vector<CTpl> (1, sum);
}

template<typename CTpl>
std::vector<CTpl> bar(std::vector<CTpl> const& x, std::vector<CTpl> const& y) {
  assert(x.size() == y.size());
  CTpl sum = 0.0;
  for(size_t i = 0; i < x.size(); i++) sum += x[i]*y[i];
  return std::vector<CTpl> {sum};
}




// += addition of two vectors | helper function for the edge functions
template<typename CTpl>
std::vector<CTpl> const& operator+=(std::vector<CTpl>& lhs, std::vector<CTpl> const& rhs) {
  assert(lhs.size() == rhs.size());
  for(int i = 0; i < lhs.size(); i++) lhs[i] += rhs[i];
  return lhs;
}
// same for +
template<typename CTpl>
std::vector<CTpl> operator+(std::vector<CTpl> const& lhs, std::vector<CTpl> const& rhs) {
  assert(lhs.size() == rhs.size());
  std::vector<CTpl> result (lhs.size(), 0.0);
  for(int i = 0; i < lhs.size(); i++) result[i] = lhs[i] + rhs[i];
  return result;
}


// implements a single dag edge with a dense jacobian
template<typename CTpl>
std::vector<CTpl> edge_func(std::vector<CTpl> const& x, size_t const& Ny, long const& cost_factor) {
  std::vector<CTpl> y (Ny, 0.0);
  for(int i = 0; i < Ny; i++) {
    for(int j = 0; j < x.size(); j++) {
      for(int k = 0; k < cost_factor; k++) {
        y[i] += std::pow(x[j], k);
      }
    }
  }
  return y;
}


/* note: gf is unused, superseded by gff */
// gf='general function' | implements the function that generates in the dag an edge set with multiple inputs and one output
// array of input vectors used, since it seems easier than variadic template (due to the accommodating cost factor)
template<typename CTpl, size_t N>
std::vector<CTpl> gf(std::array<std::vector<CTpl>, N> args, size_t const& Ny, std::array<long, N> const& cost_factors) {
  //assert(args.size() == cost_factors.size()); // not needed when using arrays
  std::vector<CTpl> y (Ny, 0.0);
  for(int i = 0; i < args.size(); i++) y += edge_func(args[i], Ny, cost_factors[i]);
  return y;
}


// this function fixes all template deduction and lambda auto issues of the above attempts
template<typename... Args>
auto gff(int Ny, Args... args) {
  return (edge_func(args.first, Ny, args.second) + ...);
}






template<typename CTpl, typename Functor>
std::vector<std::vector<CTpl>> jacobian_t(Functor func, std::vector<CTpl> const& x) {
  const int n = x.size();

  using tangent_t = dco::gt1s<CTpl>::type; // c++ < 20 requires typename
  std::vector<tangent_t> ad_x (n);
  for(int i = 0; i < n; i++) ad_x[i] = x[i];

  // first row separately to get size of output with little overhead
  dco::derivative(ad_x[0]) = 1.0;
  //std::vector<tangent_t> ad_y = func(ad_x); // errors at compile time; I think func still returns only vector<double> even with AD input
  auto ad_y = func(ad_x);
  dco::derivative(ad_x[0]) = 0.0;
  // create result jacobian and fill the first column
  const int m = ad_y.size();
  std::vector<std::vector<CTpl>> jac (n, std::vector<CTpl> (m, 0.0));
  for(int j = 0; j < m; j++) jac[0][j] = dco::derivative(ad_y[j]);

  for(int i = 0; i < n; i++) {
    dco::derivative(ad_x[i]) = 1.0;
    ad_y = func(ad_x);
    for(int j = 0; j < m; j++) jac[i][j] = dco::derivative(ad_y[j]);
    dco::derivative(ad_x[i]) = 0.0;
  }
  return jac; // is column-major (since faster for tangent mode) bzw. transposed
}



// tuple magic
/* see https://devblogs.microsoft.com/oldnewthing/20200625-00/?p=103903 */

template<std::size_t N, typename Seq> struct offset_sequence;
template<std::size_t N, std::size_t... Ints>
struct offset_sequence<N, std::index_sequence<Ints...>> {
 using type = std::index_sequence<Ints + N...>;
};
template<std::size_t N, typename Seq>
using offset_sequence_t = typename offset_sequence<N, Seq>::type;


template<typename Seq1, typename Seq> struct cat_sequence;
template<std::size_t... Ints1, std::size_t... Ints2>
struct cat_sequence<std::index_sequence<Ints1...>,
                    std::index_sequence<Ints2...>>
{
 using type = std::index_sequence<Ints1..., Ints2...>;
};
template<typename Seq1, typename Seq2>
using cat_sequence_t = typename cat_sequence<Seq1, Seq2>::type;


template<typename Tuple, std::size_t... Ints>
std::tuple<std::tuple_element_t<Ints, Tuple>...>
select_tuple(Tuple&& tuple, std::index_sequence<Ints...>) {
 return { std::get<Ints>(std::forward<Tuple>(tuple))... };
}


template<std::size_t N, typename Tuple>
auto remove_Nth(Tuple&& tuple) {
  constexpr auto size = std::tuple_size_v<Tuple>;
  using first = std::make_index_sequence<N>;
  using rest = offset_sequence_t<N+1, std::make_index_sequence<size-N-1>>;
  using indices = cat_sequence_t<first, rest>;
  return select_tuple(std::forward<Tuple>(tuple), indices{});
}
// alternative way (more involved, without cat_sequence_t)
template<std::size_t N, typename Tuple>
auto remove_Nth_alt(Tuple&& tuple) {
  constexpr auto size = std::tuple_size_v<Tuple>;
  using first = std::make_index_sequence<N>;
  using rest = offset_sequence_t<N+1, std::make_index_sequence<size-N-1>>;
  return std::tuple_cat(
    select_tuple(std::forward<Tuple>(tuple), first{}),
    select_tuple(std::forward<Tuple>(tuple), rest{}));
}

// copied tuple magic end

// now use the above to not remove, but replace the Nth entry!
template<std::size_t N, typename Tuple, typename Replacement>
auto replace_Nth(Tuple tuple, Replacement replacement) {
  constexpr auto size = std::tuple_size_v<Tuple>;
  using first = std::make_index_sequence<N>;
  static_assert(size-N-1 >= 0);
  using rest = offset_sequence_t<N+1, std::make_index_sequence<size-N-1>>;
  return std::tuple_cat(select_tuple(std::forward<Tuple>(tuple), first{}),
                        std::make_tuple(replacement),
                        select_tuple(std::forward<Tuple>(tuple), rest{}));
}

// replacing the Nth entry is what our auto function (hopefully) needs
template<std::size_t I, typename Functor, typename... Args>
auto single(Functor func, Args... args) {
  return [=](std::vector<auto>const& x)  {
    auto aux = std::make_tuple(args...);
    auto res = replace_Nth<I, decltype(aux), decltype(x)>(aux, x);
    return std::apply(func, res); };
}







// Tracker type to wrap the computation type (vector<double>) with a node id
template<typename CTpl>
struct Tracker {
  using CT = CTpl;

  CTpl value;
  int id = -1;
  Tracker() : value(), id(-1) {}
  Tracker(CTpl const& value) : value(value), id(-1) {}
};


// tape to capture functions of the form Tracker<vec<>> func(Tracker<vec<>>...)
template<typename CTpl = double>
struct Tape {
  using CT = CTpl;
  // list of (wrapped) functions stored in tape
  std::vector<std::function<std::vector<CTpl> ()>> functors;
  // list of jacobian functions (tangent mode only currently)
  std::vector<std::function<std::vector<std::vector<CTpl>> ()>> tangent_functors;

  // node sizes in the dag | acts as node list
  std::vector<long> nodes;
  // sets of edges from multiple input (predecessor) nodes to one output (successor) node (node stored indices == position in nodes vec)
  std::vector<std::pair<std::vector<int>, int>> edge_sets;
  // profiled tangent and adjoint costs for all stored edges in the edge sets
  std::vector<std::vector<std::pair<long, long>>> edge_costs; // [i_set][i_predecessor/i_edge][first=tangent, second=adjoint]


  int register_var(Tracker<std::vector<CTpl>>& in) {
    if(-1 == in.id) {
      in.id = nodes.size();
      nodes.push_back(in.value.size());
    }
    return in.id;
  }

  // captures a function with its inputs
  template<typename FunctorTpl, typename... InputTpl>
  auto capture(FunctorTpl func, InputTpl&... args) {
    // note: assume all inputs are Tracker<std::vector<CTpl>>

    // execute primal and store output to return at the end
    typedef typename std::result_of<FunctorTpl(typename InputTpl::CT...)>::type vec_type;
    Tracker<vec_type> output; //Tracker<std::vector<double>> output;
    output.value = func(args.value...);

    // register input and output variables | expand the dag by output node and edge set from inputs to output
    std::vector<int> input_ids;
    (input_ids.push_back(register_var(args)), ...);
    edge_sets.push_back(std::make_pair(input_ids, register_var(output)));

    // store the function for later use
    auto wrapper = [=] () { return func(args.value...); };
    functors.push_back(wrapper);
    // also store (tangent) models for the Jacobians of all edges
    //tangent_functors.push_back([=] () { return jacobian_t(single<0>(func, args.value...)); });

    return output;
  }

  // executes all (wrapped) functions in the tape with their stored inputs and prints the results
  void exec() const {
    for(auto& f : functors) { std::cout << f()[0] << "\n"; }
  }

  // dumps the dag into std::cout in simple format
  void print_dag() const {
    std::cout << "Node sizes: ";
    for(int i = 0; i < nodes.size(); i++) std::cout << nodes[i] << ((i < nodes.size()-1) ? ", " : "\n");
    std::cout << "Edge sets:\n";
    for(int i = 0; i < edge_sets.size(); i++) {
      for(auto const& pre : edge_sets[i].first) std::cout << pre << " ";
      std::cout << " --> " << edge_sets[i].second << "\n";
    }
  }



  // profiles all functions to acquire AD costs for the edges in the dag | well... if only the Jacobian capture would work...
  void profile() {
    
  }



  // writes the dag into a stream in xml format readable my ADMission code
  template<typename StreamTpl>
  StreamTpl& stream_xml(StreamTpl& stream) {
    // check general type safety | better would be a concept that says "supports operator<<"
    static_assert(std::derived_from<StreamTpl, std::ostream> && "Tape::stream_xml input should be a ostream (e.g. stringstream)");

      // write header into stream
    stream<< "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    stream<< "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">\n";
    stream<< "  <key id=\"has_jacobian\" for=\"edge\" attr.name=\"has_jacobian\" attr.type=\"boolean\" />\n";
    stream<< "  <key id=\"adjoint_cost\" for=\"edge\" attr.name=\"adjoint_cost\" attr.type=\"long\" />\n";
    stream<< "  <key id=\"tangent_cost\" for=\"edge\" attr.name=\"tangent_cost\" attr.type=\"long\" />\n";
    stream<< "  <key id=\"has_model\" for=\"edge\" attr.name=\"has_model\" attr.type=\"boolean\" />\n";
    stream<< "  <key id=\"index\" for=\"node\" attr.name=\"index\" attr.type=\"long\" />\n";
    stream<< "  <key id=\"size\" for=\"node\" attr.name=\"size\" attr.type=\"long\" />\n";
    stream<< "  <graph id=\"G\" edgedefault=\"directed\" parse.nodeids=\"free\" parse.edgeids=\"canonical\" parse.order=\"nodesfirst\">\n";

    // write nodes into stream
    for(int i = 0; i < nodes.size(); i++) {
      stream << "  <node id=\"" << i << "\">\n";
      stream << "    <data key=\"index\">" << i << "</data>\n";
      stream << "    <data key=\"size\">" << nodes[i] << "</data>\n";
      stream << "  </node>\n";
    }

    // write edges into stream
    stream<< "\n";
    int i_edge = 0;
    for(int i = 0; i < edge_sets.size(); i++) {
      for(int j = 0; j < edge_sets[i].first.size(); j++) {
        stream << "  <edge id=\"" << i_edge++
               << "\" source=\"" << edge_sets[i].first[j]
               << "\" target=\"" << edge_sets[i].second << "\">\n";
        stream << "    <data key=\"has_jacobian\">0</data>\n";
        long tangent_cost = 1, adjoint_cost = 7; // fixed arbitrary cost as placeholder | TODO: replace with profiled cost
        stream << "    <data key=\"adjoint_cost\">" << adjoint_cost << "</data>\n";
        stream << "    <data key=\"tangent_cost\">" << tangent_cost << "</data>\n";
        stream << "    <data key=\"has_model\">1</data>\n";
        stream << "  </edge>\n";
      }
    }

    // write tail / footer
    stream << "  </graph>\n";
    stream << "</graphml>\n";

    return stream;
  }

};





int main () {

  ////////////////////////////////////////////////////////////////////////
  /* basic function store test */

  std::vector<std::function<std::vector<double> ()>> functors;

  // save lambdas that call the functions
  std::vector<double> x (10, 1.0), y (10, 2.0);
  functors.push_back([=]() { return foo(x); });
  functors.push_back([=]() { return bar(x,y); });
  // modification of x does not change the results, as copies are captured
  x[2] = 3.0;

  // run the functions
  for (auto& f : functors) {
    std::cout << f()[0] << "\n";
  }

  ////////////////////////////////////////////////////////////////////////
  /* basic tape test */

  // test basic tape
  Tracker<std::vector<double>> xt {std::vector<double> (10, 1.0)}, yt {std::vector<double> (10, 2.0)};
  Tape tape;
  //tape.capture(foo<double>, xt);
  //tape.capture(bar<double>, xt, yt);
  tape.capture([=](std::vector<auto> const& in) {return foo(in); }, xt);
  tape.capture([=](std::vector<auto> const& in1, std::vector<auto> const& in2) {return bar(in1, in2); }, xt, yt);
  tape.exec();
  tape.print_dag();

  std::ofstream out("testfile.xml");
  tape.stream_xml(out);
  out.close();

  ////////////////////////////////////////////////////////////////////////
  /**/
  
  /* The auto lambdas can be called with different types! -> allows to pass functions that work on multiple types */
  auto func = [=](std::vector<auto> const& in) {return foo(in); };
  std::vector<double> y_out = func(x);
  std::vector<float> x2 (10, 1.0);
  std::vector<float> y_out2 = func(x2);
  tape.capture(func, xt); // capture foo with a tracked type
  // intuition (??): essentially the lambda function delays the type deduction to the point where it is called by hiding the types inside the autos


  std::vector<double> z1 (10, 1.0);
  std::vector<double> z2 (10, 2.0);
  std::vector<double> z3 (10, 3.0);
  std::cout << "GF test: " <<  gf<double, 3>({z1, z2, z3}, 1, {1, 2, 4})[0] << "\n";
  /*tape.capture([=](std::vector<auto> const& in_1,
                   std::vector<auto> const& in_2,
                   std::vector<auto> const& in_3) {
                    return gf({in_1, in_2, in_3}, 1, {1, 2, 4}); }); //*/
  // => deduction failure of the braced list requires explicit specification of template params, which does not allow auto
  // -> cannot capture how it is currently... (fixed by function gff)

  std::cout << "GF test: " <<  gff(1, std::make_pair(z1, 1), std::make_pair(z2, 2), std::make_pair(z3, 4))[0] << "\n";
  auto special_gff = [=](std::vector<auto> const& in_1, std::vector<auto> const& in_2, std::vector<auto> const& in_3) {
                         return gff(1, std::make_pair(z1, 1), std::make_pair(z2, 2), std::make_pair(z3, 4)); };

  Tracker<std::vector<double>> z1t {std::vector<double> (10, 1.0)};
  Tracker<std::vector<double>> z2t {std::vector<double> (10, 2.0)};
  Tracker<std::vector<double>> z3t {std::vector<double> (10, 3.0)};
  tape.capture(special_gff, z1t, z2t, z3t);
  tape.exec();


  ////////////////////////////////////////////////////////////////////////
  /* testing jacobian computation of a auto lambda function */


  // test the jacobian of a function functionality
  auto jac = jacobian_t([=](std::vector<auto> vec) { return vec; }, z2);

  // test the tuple element replacement functionality
  std::tuple<int, float, long, double> tup = std::make_tuple(1, 3.3, 2, 6.6);
  auto rt = replace_Nth<1, decltype(tup), int>(tup, 3);
  std::cout << std::get<0>(tup) << " " << std::get<1>(tup) << " " << std::get<2>(tup) << " " << std::get<3>(tup) << "\n";
  std::cout << std::get<0>(rt) << " " << std::get<1>(rt) << " " << std::get<2>(rt) << " " << std::get<3>(rt) << "\n";

  // test function wrapper to get a single input function
  auto singled_func = single<1>(special_gff, z1, z2, z3);
  std::vector<int> z2_int (10, 2);
  std::vector<float> z2_float (10, 2);
  std::cout << singled_func(z2_int)[0] << "\n";
  std::cout << singled_func(z2_float)[0] << "\n";
  // ==> we can call the singled function with arbitrary typed vectors

  // test the jacobian calculation
  std::cout << jacobian_t(singled_func, z2)[0][0] << "\n";
  // --> returns zero, since AD type is cast to double
  //     replacing the auto with the tangent_t in jacobian_t() yield type conversion error, confirming the return type issues.

  return 0;
}
