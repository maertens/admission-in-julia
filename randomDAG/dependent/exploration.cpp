// ################################ INCLUDES ################################ //

#include "admission_config.hpp"
#include "graph/DAG.hpp"
#include "graph/face_dag.hpp"
#include "graph/join.hpp"
#include "graph/read_graph.hpp"
#include "lower_bounds/lower_bound.hpp"
#include "lower_bounds/simple_min_acc_cost_bound.hpp"
#include "operations/elimination_algorithms.hpp"
#include "operations/global_modes.hpp"
#include "operations/op_sequence.hpp"
#include "optimizers/all_optimizers.hpp"
#include "optimizers/optimizer.hpp"
#include "util/factory.hpp"
#include "util/openmp.hpp"
#include "util/properties.hpp"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <stddef.h>
#include <stdexcept>
#include <string>


#include <random>
#include <vector>
#include <tuple>
#include <cassert>
#include <ranges>
#include <algorithm>
#include <sstream>
#include <filesystem>

#include <drag/drag.hpp>
#include <drag/drawing/draw.hpp>
#include <drag/types.hpp>

/* Combination of the stripped down admission app with the standalone random lag generator to yield an automatic generate-admission loop. */



// nodes in the DAG | represents a set/vec of values in code
template<typename IdxT = int>
struct Node {
  IdxT size; // number of values
  std::vector<IdxT> predecessors; // links F(predecessor values) = current values
  std::vector<IdxT> successors; // links F_i(current values) = (sucessor values)_i

  Node(IdxT const& size) : size(size) {}
};

// a small layered DAG
template<typename IdxT = int>
struct DAG {
  // list of layers, which are lists of nodes
  std::vector<std::vector<Node<IdxT>>> layers;


  // construct DAG with number of layers
  DAG(IdxT const& num_layers) : layers(num_layers) {}
  // get node list of a given layer
  std::vector<Node<IdxT>>& get_layer(size_t const& idx) { return layers[idx]; }
  std::vector<Node<IdxT>> const& get_layer(size_t const& idx) const { return layers[idx]; }


  // save as svg 
  void draw_svg(std::string const& filename = "dag.svg") const {
    drag::graph g;
    drag::drawing_options opts;

    // convert the DAG into a drag from the drag libary
    std::vector<std::vector<drag::vertex_t>> vertices (layers.size());
    for(int l = 0; l < layers.size(); l++) { // layers
      for(int n = 0; n < layers[l].size(); n++) { // nodes / vertices
        vertices[l].push_back(g.add_node());
        opts.labels[vertices[l][n]] = std::to_string(layers[l][n].size);
      }
    }
    for(int l = 0; l < layers.size()-1; l++) {
      for(int n = 0; n < layers[l].size(); n++) {
        for(int succ = 0; succ < layers[l][n].successors.size(); succ++) {
          g.add_edge(vertices[l][n], vertices[l+1][layers[l][n].successors[succ]]);
        }
      }
    }

    // save an image of the dag
    drag::sugiyama_layout layout(g);
    auto image = drag::draw_svg_image(layout, opts);
    image.save(filename);
  }
};



// A random generator to produce simple layered DAGs
class Generator {
  std::mt19937 rg; // random generator
  // input and output layer size
  int size_n;
  // size range of intermediate layers
  std::pair<int, int> node_size_bounds;
  // number of layers in the dag including in and output layer
  int n_layers;
  // maximum number of nodes per layer
  int max_n_nodes;
  // whether to fully connect nodes of adjacent layers (default false: random connectivity)
  bool dense_connectivity;
  // whether to always pick maximum number of nodes in each layer
  bool dense_nodes;


// initialize a layer with nodes
void set_nodes(DAG<int>& dag, int const& i_layer) {
  auto& layer = dag.get_layer(i_layer);
  std::uniform_int_distribution<> dist (1, max_n_nodes);
   /*
  To get an average sum of sizes that is inside the middle of the node-size range,
  we divide the effective range by the number of nodes (note: integer division, so some truncation errors happen).
  Then add this as offset to the minimum divided by the number of nodes.
  This may slightly not be within the range. For larger sizes, this will become more unlikely.
  */
  int n_nodes = dense_nodes ? max_n_nodes : dist(rg);
  int delta = node_size_bounds.second - node_size_bounds.first;
  std::uniform_int_distribution<> size_dist (0, delta / n_nodes);
  int base = std::ceil((double) node_size_bounds.first / (double) n_nodes); // converion, such that bounds=(1,1) works (scalar case)
  for(int i = 0; i < n_nodes; i++) {
    int size = base + size_dist(rg);
    layer.push_back(Node<int>(size));
  }
}

// link two layers in a DAG
void uplink_layers(DAG<int>& dag, int const& i_source, int const& i_target) {
  auto& sources = dag.get_layer(i_source);
  auto& targets = dag.get_layer(i_target);

  // every source node up-links to at least one target node
  for(int s = 0; s < sources.size(); s++) {
    // no dead ends: at least one target | otherwise uniform in number of targets
    std::uniform_int_distribution<> dist(1, targets.size());
    int n_edges = dense_connectivity ? targets.size() : dist(rg);
    // uniformly sample connections (ohne Zurücklegen) from targets
    std::vector<int> target_indices;
    std::vector<int> in (targets.size());
    std::iota(in.begin(), in.end(), 0);
    std::ranges::sample(in, std::back_inserter(target_indices), n_edges, rg);
    // link s to all targets t
    for(auto const t : target_indices) {
      sources[s].successors.push_back(t);
      targets[t].predecessors.push_back(s);
    }
  }

  /* every target node needs to be hit by at least one incoming edge
  => for every source-target combination we need to decide if the link exists
     also we need to ensure that every source and target has at least one link.
  Here, I simply check which target nodes are not hit and link them up to a random source node.
  */
  std::uniform_int_distribution<> dist (0, sources.size()-1); // index of source to link to => [0, size-1]
  for(int t = 0; t < targets.size(); t++) {
    if(targets[t].predecessors.empty()) {
      int s = dist(rg);
      sources[s].successors.push_back(t);
      targets[t].predecessors.push_back(s);
    }
  }
}


public:
  // init with random seed and parameters
  Generator(int const seed, int const n, int const num_layers, int const max_nodes_per_layer,
            int const intermediate_layer_total_node_size_lower,
            int const intermediate_layer_total_node_size_upper,
            bool const dense_conn = false, bool const dense_nodes = false)
    : rg(seed), size_n(n), n_layers(num_layers), max_n_nodes(max_nodes_per_layer),
      node_size_bounds(intermediate_layer_total_node_size_lower, intermediate_layer_total_node_size_upper), 
      dense_connectivity(dense_conn), dense_nodes(dense_nodes) {
    // initialization stuff
    assert(n_layers > 2);
    assert(intermediate_layer_total_node_size_lower <= intermediate_layer_total_node_size_upper);
  }

  // generate a random dag
  DAG<int> generate() {
    DAG<int> dag (n_layers);
    // first layer is single input node
    dag.get_layer(0).push_back(Node<int>(size_n));

    for(int i = 1; i < n_layers-1; i++) {
      // randomly determine number and size of nodes in the layer and set accordingly
      set_nodes(dag, i);
      // randomly link nodes in layer to previous layer
      uplink_layers(dag, i-1, i);
    }
    // last layer is single output node
    dag.get_layer(n_layers-1).push_back(Node<int>(size_n));
    uplink_layers(dag, n_layers-2, n_layers-1);

    return dag;
  }
};




/* generator for random edge costs */
class CostGenerator {
  std::mt19937 rg;
  std::uniform_int_distribution<> dist; // random function/jacobian complexity factor for cost
  int adjoint_factor;
  int upper_cost_factor; // maximal random factor (between 1 and upper_cost_factor) of edge cost

public:
  CostGenerator(int const seed, int const adjoint_factor, int const upper_cost_factor)
  : rg(seed), dist(1, upper_cost_factor), adjoint_factor(adjoint_factor) {}

  // compute a random cost based on size of edge/function in and outputs | out: (tangent cost, adjoint cost)
  std::pair<long, long> eval(int const& source_size, int const& target_size) {
    long base_cost = dist(rg) * source_size*target_size;
    return std::make_pair(base_cost, adjoint_factor*base_cost); // tangent and adjoint cost
  }
};



/* converts a dag to graphml xml format for admission read-in */
std::stringstream convert2graphml(DAG<int> const& dag, CostGenerator& cost) {
  std::stringstream ss;
  // write header into stream
  ss << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  ss << "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">\n";
  ss << "  <key id=\"has_jacobian\" for=\"edge\" attr.name=\"has_jacobian\" attr.type=\"boolean\" />\n";
  ss << "  <key id=\"adjoint_cost\" for=\"edge\" attr.name=\"adjoint_cost\" attr.type=\"long\" />\n";
  ss << "  <key id=\"tangent_cost\" for=\"edge\" attr.name=\"tangent_cost\" attr.type=\"long\" />\n";
  ss << "  <key id=\"has_model\" for=\"edge\" attr.name=\"has_model\" attr.type=\"boolean\" />\n";
  ss << "  <key id=\"index\" for=\"node\" attr.name=\"index\" attr.type=\"long\" />\n";
  ss << "  <key id=\"size\" for=\"node\" attr.name=\"size\" attr.type=\"long\" />\n";
  ss << "  <graph id=\"G\" edgedefault=\"directed\" parse.nodeids=\"free\" parse.edgeids=\"canonical\" parse.order=\"nodesfirst\">\n";

  // construct node index structure while writing the nodes
  std::vector<std::vector<int>> node_indices (dag.layers.size());

  // write nodes into stream
  int i_node = 0;
  for(int i = 0; i < dag.layers.size(); i++) {
    auto const& layer = dag.get_layer(i);
    for(int j = 0; j < layer.size(); j++) {
      ss << "  <node id=\"" << i_node << "\">\n";
      ss << "    <data key=\"index\">" << i_node << "</data>\n";
      ss << "    <data key=\"size\">" << layer[j].size << "</data>\n";
      ss << "  </node>\n";
      node_indices[i].push_back(i_node++); // i_node at position [i][j]
    }
  }

  // write edges into stream
  ss << "\n";
  int i_edge = 0;
  for(int i = 0; i < dag.layers.size()-1; i++) {
    auto const& layer = dag.get_layer(i);
    for(int j = 0; j < layer.size(); j++) {
      for(int k = 0; k < layer[j].successors.size(); k++) {
        ss << "  <edge id=\"" << i_edge++
           << "\" source=\"" << node_indices[i][j]
           << "\" target=\"" << node_indices[i+1][layer[j].successors[k]] << "\">\n";
        ss << "    <data key=\"has_jacobian\">0</data>\n";
        int source_size = layer[j].size;
        int target_size = dag.layers[i+1][layer[j].successors[k]].size;
        auto [tangent_cost, adjoint_cost] = cost.eval(source_size, target_size);
        ss << "    <data key=\"adjoint_cost\">" << adjoint_cost << "</data>\n";
        ss << "    <data key=\"tangent_cost\">" << tangent_cost << "</data>\n";
        ss << "    <data key=\"has_model\">1</data>\n";
        ss << "  </edge>\n";
      }
    }
  }

  // write tail / footer
  ss << "  </graph>\n";
  ss << "</graphml>\n";

  return ss;
}


// save graph as xml, image as svg and the elimination sequence in file
void save(std::string const& path, int id, DAG<int> const& dag, std::stringstream& ss_xml, std::string seq_str) {
  // get filenames
  std::string filename = path + "/dag_" + std::to_string(id);

  dag.draw_svg(filename + ".svg");
  std::ofstream out (filename + ".xml");
  out << ss_xml.str(); out.close();
  out.open(filename + "_seq.txt");
  out << seq_str << "\n";
}


void clear_directory(std::filesystem::path const& path) {
  for (const auto &entry : std::filesystem::directory_iterator(path)) { std::filesystem::remove_all(entry.path()); }
}



/******************************************************************************
 * Interface for solving
 * matrix free vector face elimination
 * m      f    v      f    e
 * on a DAG.

optimizer options:
BranchAndBound
GreedyMinFill
SparseAdjoint
SparseTangent
 ******************************************************************************/
std::tuple<long, long, long, std::string> face_elim(std::stringstream& dag_ss, std::string const& optimizer_name = "GreedyMinFill", bool const preaccumulate = false) {
  // config
  double solution_output_interval = 1.0;
  bool human_readable = true;

  // Create a DAG and parse it from the provided stream.
  admission::DAG g;
  try { admission::read_graphml(dag_ss, g); }
  catch (std::runtime_error& e) {
    std::cout << e.what() << std::endl;
  }

  // Build a FaceDAG out of the parsed graph.
  auto g_f = admission::make_face_dag(g);

  // global preaccumulation, if desired
  admission::OpSequence s_mm = admission::OpSequence::make_empty();
  if (preaccumulate) {
    s_mm = admission::global_preaccumulation_ops(*g_f);
    admission::preaccumulate_all(*g_f, true);
  }


  // Compute the cost of global modes.
  admission::flop_t c_t = admission::global_tangent_cost(*g_f);
  admission::flop_t c_a = admission::global_adjoint_cost(*g_f);

  // Create an optimizer.
  admission::Optimizer* op;
  auto& optimizer_factory = admission::OptimizerFactory::instance();

  try {
    op = optimizer_factory.construct(optimizer_name);
  }
  catch (admission::KeyNotRegisteredError& tnr) {
    std::cout << tnr.what() << std::endl;
  }

  // Create a lower bound if needed.
  if (op->has_lower_bound()) {
    op->set_lower_bound(new admission::SimpleMinAccCostBound());
    op->set_output_interval(solution_output_interval);
    op->set_output_mode(human_readable);
  }

  // Set the optimizer settings.
  if (op->is_parallel()) {
    op->set_parallel_depth(1);
    admission::set_num_threads(1);
  }
  op->set_diagnostics(false);

  // Solve face elimination problem and store cost
  auto seq = op->solve(*g_f);
  admission::flop_t c_fe = seq.cost();

  // if preaccumulation, add cost to eval modes
  if(preaccumulate) {
    long c_p = s_mm.cost();
    c_t += c_p; c_a += c_p; c_fe += c_p;
  }

  // get elim sequence
  std::stringstream ss;
  seq.write(ss);
  std::string str = ss.str();

  /* some random 'diagnositc' prints
  if(str.find("ADJ") == std::string::npos) {
    std::cout << "Best sequence has only tangent mode!!!\n";
  }*/

  if (op->has_lower_bound()) {
    delete op->get_lower_bound();
  }
  delete op;

  return std::make_tuple(c_t, c_a, c_fe, str);
}





int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cout << "Args: [#iterations] (DAGs to generate and test) [keywords] (optional: preaccumulate, dense_nodes, dense_conn, scalar)\n";
    return 1;
  }
  int num_iters = std::stoi(argv[1]);
  // configure additional options/modes
  bool preaccumulate = false, scalar = false, dense_nodes = false, dense_conn = false;
  for(int i = 2; i < argc; i++) {
    std::string keyword (argv[i]);
    if("preaccumulate" == keyword) preaccumulate = true;
    else if("scalar" == keyword) scalar = true;
    else if("dense_nodes" == keyword) dense_nodes = true;
    else if("dense_conn" == keyword) dense_conn = true;
  }
 
  // set some parameters
  std::string out_dir = "results";
  int seed = 1234;
  int n = scalar ? 1 : 100;
  int n_max = scalar ? 1 : 2*n;
  int layers = 4; //large: 8;// default: 5;
  int max_nodes_per_layer = 2; // large: 8;//default: 4;
  int adjoint_factor = 7;
  int max_cost_factor = 3; // default 5
  std::string optimizer = "GreedyMinFill";//"BranchAndBound";
  // create DAG generator and cost generator
  Generator gen (seed, n, layers, max_nodes_per_layer, n, n_max, dense_conn, dense_nodes);
  CostGenerator CostGen {123, adjoint_factor, max_cost_factor};

  // print config and parameters for clarity
  auto aux_decider = [&](bool val) { return val ? "true" : "false"; };
  std::cout << "Configuration:\n"
            << "preaccumulate = " << aux_decider(preaccumulate) << "\n"
            << "scalar        = " << aux_decider(scalar) << "\n"
            << "dense nodes   = " << aux_decider(dense_nodes) << "\n"
            << "dense conn    = " << aux_decider(dense_conn) << "\n";
  std::cout << "n                   = " << n << "\n"
            << "n max               = " << n_max << "\n"
            << "layers              = " << layers << "\n"
            << "max nodes per layer = " << max_nodes_per_layer << "\n"
            << "adj to tan factor   = " << adjoint_factor << "\n";
  std::cout << "Face Elimination Optimization Method: " << optimizer << "\n"
            << "Output directory: " << out_dir << "\n";



  std::cout < "Clearing output directory directory.\n";
  clear_directory(out_dir);

  // iterate random dags and print results
  double minimum = 2.0;
  for(int i = 0; i < num_iters; i++) {
    DAG<int> dag = gen.generate();
    std::stringstream ss = convert2graphml(dag, CostGen);
    auto [c_t, c_a, c_fe, seq_str] = face_elim(ss, optimizer, preaccumulate);
    // in scalar case, compare preaccumulated fe with 
    // determine the factor (or fraction) of fe runtime vs. dense tangent or adjoint runtime
    long min = std::min(c_t, c_a);
    double factor = (double) c_fe / (double) min; // is 1/ the improvement factor (=runtime fraction)
    if(factor < minimum) {
      minimum = factor;
      std::cout << "Found new minimum factor: " << minimum << "\tat i=" << i << "\n";
      save(out_dir, i, dag, ss, seq_str);
    }
  }

  return 0;
}

