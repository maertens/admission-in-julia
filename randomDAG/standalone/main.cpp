#include <iostream>
#include <random>
#include <vector>
#include <tuple>
#include <cassert>
#include <ranges>
#include <algorithm>
#include <string>
#include <fstream>

#include <drag/drag.hpp>
#include <drag/drawing/draw.hpp>
#include <drag/types.hpp>


/* randomly generate small simple layered DAGs to test with ADMission code */

/* A note on the randomness:
Although uniform distributions are used,
the structure of the code may yield non-uniform distributions in the quantities of interest.
Furthermore, the distribution objects themselves have states,
making  the distributions that are created new at every generate call maybe not very random.
*/




// nodes in the DAG | represents a set/vec of values in code
template<typename IdxT = int>
struct Node {
  IdxT size; // number of values
  std::vector<IdxT> predecessors; // links F(predecessor values) = current values
  std::vector<IdxT> successors; // links F_i(current values) = (sucessor values)_i

  Node(IdxT const& size) : size(size) {}
};

// a small layered DAG
template<typename IdxT = int>
struct DAG {
  // list of layers, which are lists of nodes
  std::vector<std::vector<Node<IdxT>>> layers;


  // construct DAG with number of layers
  DAG(IdxT const& num_layers) : layers(num_layers) {}
  // get node list of a given layer
  std::vector<Node<IdxT>>& get_layer(size_t const& idx) { return layers[idx]; }
  std::vector<Node<IdxT>> const& get_layer(size_t const& idx) const { return layers[idx]; }

  // save as svg 
  void draw_svg(std::string const& filename = "dag.svg") const {
    drag::graph g;
    drag::drawing_options opts;

    // convert the DAG into a drag from the drag libary
    std::vector<std::vector<drag::vertex_t>> vertices (layers.size());
    for(int l = 0; l < layers.size(); l++) { // layers
      for(int n = 0; n < layers[l].size(); n++) { // nodes / vertices
        vertices[l].push_back(g.add_node());
        opts.labels[vertices[l][n]] = std::to_string(layers[l][n].size);
      }
    }
    for(int l = 0; l < layers.size()-1; l++) {
      for(int n = 0; n < layers[l].size(); n++) {
        for(int succ = 0; succ < layers[l][n].successors.size(); succ++) {
          g.add_edge(vertices[l][n], vertices[l+1][layers[l][n].successors[succ]]);
        }
      }
    }

    // save an image of the dag
    drag::sugiyama_layout layout(g);
    auto image = drag::draw_svg_image(layout, opts);
    image.save(filename);
  }
};



// A random generator to produce simple layered DAGs
class Generator {
  std::mt19937 rg; // random generator
  // input and output layer size
  int size_n;
  // size range of intermediate layers
  std::pair<int, int> node_size_bounds;
  // number of layers in the dag including in and output layer
  int n_layers;
  // maximum number of nodes per layer
  int max_n_nodes;


// initialize a layer with nodes
void set_nodes(DAG<int>& dag, int const& i_layer) {
  auto& layer = dag.get_layer(i_layer);
  std::uniform_int_distribution<> dist (1, max_n_nodes);
  /*
  To get an average sum of sizes that is inside the middle of the node-size range,
  we divide the effective range by the number of nodes (note: integer division, so some truncation errors happen).
  Then add this as offset to the minimum divided by the number of nodes.
  This may slightly not be within the range. For larger sizes, this will become more unlikely.
  */
  int n_nodes = dist(rg);
  int delta = node_size_bounds.second - node_size_bounds.first;
  std::uniform_int_distribution<> size_dist (0, delta / n_nodes);
  int base = node_size_bounds.first / n_nodes;
  for(int i = 0; i < n_nodes; i++) {
    int size = base + size_dist(rg);
    layer.push_back(Node<int>(size));
  }
}

// link two layers in a DAG
void uplink_layers(DAG<int>& dag, int const& i_source, int const& i_target) {
  auto& sources = dag.get_layer(i_source);
  auto& targets = dag.get_layer(i_target);

  // every source node up-links to at least one target node
  for(int s = 0; s < sources.size(); s++) {
    // no dead ends: at least one target | otherwise uniform in number of targets
    std::uniform_int_distribution<> dist(1, targets.size());
    int n_edges = dist(rg);
    // uniformly sample connections (ohne Zurücklegen) from targets
    std::vector<int> target_indices;
    std::vector<int> in (targets.size());
    std::iota(in.begin(), in.end(), 0);
    std::ranges::sample(in, std::back_inserter(target_indices), n_edges, rg);
    // link s to all targets t
    for(auto const t : target_indices) {
      sources[s].successors.push_back(t);
      targets[t].predecessors.push_back(s);
    }
  }

  /* every target node needs to be hit by at least one incoming edge
  => for every source-target combination we need to decide if the link exists
     also we need to ensure that every source and target has at least one link.
  Here, I simply check which target nodes are not hit and link them up to a random source node.
  */
  std::uniform_int_distribution<> dist (0, sources.size()-1); // index of source to link to => [0, size-1]
  for(int t = 0; t < targets.size(); t++) {
    if(targets[t].predecessors.empty()) {
      int s = dist(rg);
      sources[s].successors.push_back(t);
      targets[t].predecessors.push_back(s);
    }
  }
}


public:
  // init with random seed and parameters
  Generator(int const seed, int const n, int const num_layers, int const max_nodes_per_layer,
            int const intermediate_layer_total_node_size_lower,
            int const intermediate_layer_total_node_size_upper)
    : rg(seed), size_n(n), n_layers(num_layers), max_n_nodes(max_nodes_per_layer),
      node_size_bounds(intermediate_layer_total_node_size_lower, intermediate_layer_total_node_size_upper) {
    // initialization stuff
    assert(n_layers > 2);
    assert(intermediate_layer_total_node_size_lower <= intermediate_layer_total_node_size_upper);
  }

  // generate a random dag
  DAG<int> generate() {
    DAG<int> dag (n_layers);
    // first layer is single input node
    dag.get_layer(0).push_back(Node<int>(size_n));

    for(int i = 1; i < n_layers-1; i++) {
      // randomly determine number and size of nodes in the layer and set accordingly
      set_nodes(dag, i);
      // randomly link nodes in layer to previous layer
      uplink_layers(dag, i-1, i);
    }
    // last layer is single output node
    dag.get_layer(n_layers-1).push_back(Node<int>(size_n));
    uplink_layers(dag, n_layers-2, n_layers-1);

    return dag;
  }
};


// execute a shell script (see https://stackoverflow.com/questions/478898/how-do-i-execute-a-command-and-get-the-output-of-the-command-within-c-using-po)
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <array>
std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

// run the admission optimizer
std::string admission(std::string const& infilename) {
  return exec(std::string("admission " + infilename).c_str());
}



/* generator for random edge costs */
class CostGenerator {
  std::mt19937 rg;
  std::uniform_int_distribution<> dist; // random function/jacobian complexity factor for cost

public:
  CostGenerator(int const seed) : rg(seed), dist(1,5) {}

  // compute a random cost based on size of edge/function in and outputs
  std::pair<long, long> eval(int const& source_size, int const& target_size) {
    long base_cost = dist(rg)*source_size * target_size;
    return std::make_pair(base_cost, 2*base_cost); // tangent and adjoint cost (assumed factor 2 more expensive)
  }
};



#include <sstream>
/* converts a dag to graphml xml format for admission read-in */
std::stringstream convert2graphml(DAG<int> const& dag, CostGenerator& cost) {
  std::stringstream ss;
  // write header into stream
  ss << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  ss << "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">\n";
  ss << "  <key id=\"has_jacobian\" for=\"edge\" attr.name=\"has_jacobian\" attr.type=\"boolean\" />\n";
  ss << "  <key id=\"adjoint_cost\" for=\"edge\" attr.name=\"adjoint_cost\" attr.type=\"long\" />\n";
  ss << "  <key id=\"tangent_cost\" for=\"edge\" attr.name=\"tangent_cost\" attr.type=\"long\" />\n";
  ss << "  <key id=\"has_model\" for=\"edge\" attr.name=\"has_model\" attr.type=\"boolean\" />\n";
  ss << "  <key id=\"index\" for=\"node\" attr.name=\"index\" attr.type=\"long\" />\n";
  ss << "  <key id=\"size\" for=\"node\" attr.name=\"size\" attr.type=\"long\" />\n";
  ss << "  <graph id=\"G\" edgedefault=\"directed\" parse.nodeids=\"free\" parse.edgeids=\"canonical\" parse.order=\"nodesfirst\">\n";

  // construct node index structure while writing the nodes
  std::vector<std::vector<int>> node_indices (dag.layers.size());

  // write nodes into stream
  int i_node = 0;
  for(int i = 0; i < dag.layers.size(); i++) {
    auto const& layer = dag.get_layer(i);
    for(int j = 0; j < layer.size(); j++) {
      ss << "  <node id=\"" << i_node << "\">\n";
      ss << "    <data key=\"index\">" << i_node << "</data>\n";
      ss << "    <data key=\"size\">" << layer[j].size << "</data>\n";
      ss << "  </node>\n";
      node_indices[i].push_back(i_node++); // i_node at position [i][j]
    }
  }

  // write edges into stream
  ss << "\n";
  int i_edge = 0;
  for(int i = 0; i < dag.layers.size()-1; i++) {
    auto const& layer = dag.get_layer(i);
    for(int j = 0; j < layer.size(); j++) {
      for(int k = 0; k < layer[j].successors.size(); k++) {
        ss << "  <edge id=\"" << i_edge++
           << "\" source=\"" << node_indices[i][j]
           << "\" target=\"" << node_indices[i+1][layer[j].successors[k]] << "\">\n";
        ss << "    <data key=\"has_jacobian\">0</data>\n";
        int source_size = layer[j].size;
        int target_size = dag.layers[i+1][layer[j].successors[k]].size;
        auto [tangent_cost, adjoint_cost] = cost.eval(source_size, target_size);
        ss << "    <data key=\"adjoint_cost\">" << tangent_cost << "</data>\n";
        ss << "    <data key=\"tangent_cost\">" << adjoint_cost << "</data>\n";
        ss << "    <data key=\"has_model\">1</data>\n";
        ss << "  </edge>\n";
      }
    }
  }

  // write tail / footer
  ss << "  </graph>\n";
  ss << "</graphml>\n";

  return ss;
}





int main() {
  int seed = 1234;
  int n = 100;
  int layers = 5;
  int max_nodes_per_layer = 4;
  Generator gen (seed, n, layers, max_nodes_per_layer, n, 2*n);
  DAG<int> dag = gen.generate();
  dag.draw_svg();

  CostGenerator CostGen {123};
  std::stringstream ss = convert2graphml(dag, CostGen);

  std::ofstream out {"dag.in"};
  /* possible optimizers
  BranchAndBound
  GreedyMinFill
  SparseAdjoint
  SparseTangent
  */
  out << "dag dag.xml\nmethod GreedyMinFill\n"; out.close();
  out.open("dag.xml");
  out << ss.str(); out.close();
  std::cout << admission("dag.in") << "\n";

  return 0;
}
