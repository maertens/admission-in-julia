% **************************************************************************** %
% Author: Lambert Theisen
% => lambert.theisen@rwth-aachen.de
% Based on: MathCCES template from Pascal Richter
% Created: April 2019
% **************************************************************************** %



% **************************************************************************** %
% BEAMER CLASS
% **************************************************************************** %
\LoadClassWithOptions{beamer} % pipe through options given in main document

% **************************************************************************** %
% MODULES
% => Don't load float, this fails with "not in outer par" using figure env
% **************************************************************************** %
  \usepackage{\templatePath/modules/02a-english}
  \usepackage{\templatePath/modules/03-typeset}
  \usepackage{\templatePath/modules/04a-font-lmodern} % needed although not used
  \usepackage{\templatePath/modules/05-mathematics}
  \usepackage{\templatePath/modules/06-colors}
  \usepackage{\templatePath/modules/07b-bibliography-biblatex}
  \usepackage{\templatePath/modules/08-graphics}
  % \usepackage{\templatePath/modules/09-floats}
  %\usepackage{\templatePath/modules/11-sourcecode}
  % \usepackage{\templatePath/modules/14-references}
  \usepackage{\templatePath/modules/16-units}
  %\usepackage{\templatePath/modules/19-tables}
  %\usepackage{\templatePath/modules/30-conditionals-ifthen}


%%% Matthias custom includes for ticz picture
\usepackage{caption}
\captionsetup{format=hang, justification=RaggedRight}
\usepackage{subcaption}
% for figures
\usepackage{tikz}
\usepackage{gnuplot-lua-tikz}
% math stuff
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\nab}[1]{\boldsymbol{\nabla}_{#1}}
\newcommand{\lap}[1]{\boldsymbol{\nabla}_{#1}^2}
\newcommand{\del}{\partial}
\DeclareSymbolFont{matha}{OML}{txmi}{m}{it}% txfonts
\DeclareMathSymbol{\varv}{\mathord}{matha}{118}

% **************************************************************************** %

% -------------------------------------------------------------------------- %
% THEME
% -------------------------------------------------------------------------- %
  \usetheme{Frankfurt}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% INNER THEME
% -------------------------------------------------------------------------- %
  \useinnertheme{rounded}
  \setbeamertemplate{items}[circle]
  \setbeamertemplate{blocks}[rounded][shadow=false]
  \setbeamertemplate{enumerate items}[default]
  \setbeamertemplate{section in toc}{\inserttocsectionnumber.~\inserttocsection}
  \setbeamertemplate{subsection in toc}[circle]

  % Increase itemize bullet size:
  % \setbeamertemplate{itemize item}{%
  % 	\Large\raise0.5pt\hbox{\textbullet}%
  % }

  % Increase enumerate bullet size:
  % \setbeamertemplate{enumerate item}
  % {
  %   \usebeamerfont*{item projected}%
  %   \usebeamercolor[bg]{item projected}%
  %   \begin{pgfpicture}{-1ex}{0ex}{1ex}{2ex}
  %     \pgfpathcircle{\pgfpoint{0pt}{.75ex}}{1.2ex}
  %     \pgfusepath{fill}
  %     \pgftext[base]{\color{fg}\insertenumlabel}
  %   \end{pgfpicture}%
  % }
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% TEXT/FONT STYLE
% -------------------------------------------------------------------------- %
  \setbeamerfont{alerted text}{series=\bfseries}
  \setbeamerfont{frametitle}{series=\bfseries}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% FONT SIZES
% -------------------------------------------------------------------------- %
% => From rwth-base but needed for nice slide design
% => Second number is baselineskip
% -------------------------------------------------------------------------- %
  % \def\tiny{\fontsize{5pt}{6pt}\selectfont}
  % \def\scriptsize{\fontsize{6pt}{8pt}\selectfont}		% wie viel zeilenhoehe?
  % \def\footnotesize{\fontsize{5pt}{5pt}\selectfont} % was 7/11 before
  % \def\small{\fontsize{8.5pt}{12pt}\selectfont}
  % \def\normalsize{\fontsize{11pt}{13pt}\selectfont}
  % \def\large{\fontsize{12pt}{14pt}\selectfont}
  % \def\Large{\fontsize{14pt}{18pt}\selectfont}
  % \def\LARGE{\fontsize{19pt}{22.5pt}\selectfont}
  % \def\huge{\fontsize{20}{26pt}\selectfont}
  % \def\Huge{\fontsize{25}{30pt}\selectfont}
  % \def\baselinestretch{1}\normalsize
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% COLOR THEME
% -------------------------------------------------------------------------- %
  \usecolortheme{orchid}
  \definecolor{maincolor}{named}{rwth-blue}
  \definecolor{subcolor}{named}{rwth-lblue}
  \definecolor{textcolor}{named}{black}
  \setbeamercolor{structure}{fg=maincolor}
  \setbeamercolor{normal text}{fg=textcolor}
  \setbeamercolor{alerted text}{fg=rwth-blue}
  \setbeamercolor{section in head/foot}{bg=maincolor,fg=white}
  \setbeamercolor{block title}{bg=maincolor,fg=white}
  \setbeamercolor{block title example}{bg=maincolor,fg=white}
  %\setbeamercolor{frametitle}{fg=maincolor,bg=white}
  %\setbeamercolor{block title alert}{bg=rwth-orange,fg=white}
% -------------------------------------------------------------------------- %

% \addtobeamertemplate{proof begin}{%
%   \setbeamercolor{block title}{fg=black,bg=subcolor!50!white}%
%   \setbeamercolor{block body}{fg=black, bg=subcolor!30!white}%
% }{}

% -------------------------------------------------------------------------- %
% SMALL FOOTNOTES
% -------------------------------------------------------------------------- %
  \let\oldfootnotesize\footnotesize
  \renewcommand*{\footnotesize}{\oldfootnotesize\tiny}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% FOOTNOTE POSITION
% => Move footnote up to avoid collision with logo
% -------------------------------------------------------------------------- %
  \addtobeamertemplate{footnote}{}{\vspace{1.0ex}}
  \setlength{\footnotesep}{0pt}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% COMPRESSED EQUATIONS
% => Reduce space above and below equations
% -------------------------------------------------------------------------- %
  \setlength\belowdisplayskip{0pt}
  \setlength\abovedisplayskip{0pt}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% NAVIGATION SYMBOLS
% -------------------------------------------------------------------------- %
  % \beamertemplatenavigationsymbolsempty
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% HALF TRANSPARENT PREVIEW
% -------------------------------------------------------------------------- %
  % \setbeamercovered{transparent}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% NORMAL MATH FONT
% => like lmodern
% -------------------------------------------------------------------------- %
  % \usefonttheme[onlymath]{serif}
% -------------------------------------------------------------------------- %

% **************************************************************************** %



% **************************************************************************** %
% TITLEPAGE AND FOOTER AND HEADER CUSTOMIZATION
% => Copied from rwth-beamer
% **************************************************************************** %

% -------------------------------------------------------------------------- %
% Define Title logo
% -------------------------------------------------------------------------- %
  \def\@backgroundgraphic{}
  \gdef\backgroundgraphic#1{\gdef\@backgroundgraphic{#1}} % global definition
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% Move content closer to frametitle
% -------------------------------------------------------------------------- %
  \addtobeamertemplate{frametitle}{\vspace*{0.0cm}}{\vspace*{-0.2cm}}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% Set footline (left)
% -------------------------------------------------------------------------- %
  \define@key{footlineleft}{height}{\gdef\@footlineleftheight{#1}}
  \define@key{footlineleft}{bgcolor}{\gdef\@footlineleftbgcolor{#1}}
  \define@key{footlineleft}{fgcolor}{\gdef\@footlineleftfgcolor{#1}}
  \newlength\footerleftwidth
  \newcommand\footlineleft[2][]{ % #1 keys   #2 content
    \setkeys{footlineleft}{#1}
    \setbeamercolor{left foot}{bg=\@footlineleftbgcolor, fg=\@footlineleftfgcolor}
    \gdef\@footlineleftcontent{#2}
  }
  \footlineleft[height=1.0em, bgcolor=white, fgcolor=textcolor]{\inserttitlegraphic}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% Set footline (right)
% -------------------------------------------------------------------------- %
  \define@key{footlineright}{width}{\gdef\@footlinerightwidth{#1}}
  \define@key{footlineright}{height}{\gdef\@footlinerightheight{#1}}
  \define@key{footlineright}{bgcolor}{\gdef\@footlinerightbgcolor{#1}}
  \define@key{footlineright}{fgcolor}{\gdef\@footlinerightfgcolor{#1}}
  \newlength\footerrightwidth
  \newcommand\footline[2][]{ % #1 keys   #2 content
    \setkeys{footlineright}{#1}
    \setbeamercolor{right foot}{bg=\@footlinerightbgcolor, fg=\@footlinerightfgcolor}
    \gdef\@footlinerightcontent{#2}
  }
  \footline[height=1em, bgcolor=maincolor, fgcolor=white]{\insertshortauthor\enspace\textbar\enspace\insertshorttitle}
% -------------------------------------------------------------------------- %


% -------------------------------------------------------------------------- %
% Set footline
% -------------------------------------------------------------------------- %
  \setbeamertemplate{footline}{%
    \hbox{%
    \begin{beamercolorbox}[wd=\footerleftwidth,ht=\@footlineleftheight,dp=1ex,center]{left foot}%
        \@footlineleftcontent%
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=\footerrightwidth,ht=\@footlinerightheight,dp=1ex,left, leftskip=3ex, rightskip=2ex]{right foot}%
      % \insertslidenavigationsymbol\insertframenavigationsymbol\insertsubsectionnavigationsymbol\insertsectionnavigationsymbol\insertdocnavigationsymbol\insertbackfindforwardnavigationsymbol % for nav symbols in footer
      \@footlinerightcontent\hspace{0pt plus 1 filll} \textbar\parbox{4.5em}{\centering\insertframenumber/\inserttotalframenumber}%
    \end{beamercolorbox}%
    }%
  }
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% Title page
% -------------------------------------------------------------------------- %
  \setbeamercolor{title foot}{bg=white,fg=black}

  \newlength\@titlegraphicwidth
  \newlength\@titlegraphicheight
  \renewcommand\maketitle{%
    \expandafter\ifstrempty\expandafter{\@backgroundgraphic}{
      \begin{frame}
        \titlepage
      \end{frame}
    }{%
    \usebackgroundtemplate{\@backgroundgraphic}
    \begin{frame}[plain]%
      \vspace{0.66\paperheight}%0.0175 % 08.04.2019 changed from 0.6625 to 0.66
      %
      \normalsize
      \setlength\@titlegraphicwidth{\widthof{\inserttitlegraphic}}
      \setlength\@titlegraphicheight{\maxof{\heightof{\inserttitlegraphic}}{\heightof{\vbox{\insertdate}}}}
      %
      \begin{beamercolorbox}[wd=\paperwidth,ht=0.32\paperheight,dp=1ex,leftskip=2ex, rightskip=2ex]{title foot}%
        \parbox[b][0.32\paperheight-\@titlegraphicheight-1ex][t]{\paperwidth-2ex-2ex}{%
          \vspace{1.0ex}
          {\usebeamerfont{title}{\color{maincolor}\bfseries\inserttitle}\\\usebeamerfont{subtitle}{\color{maincolor}\insertsubtitle}}
          \vfill \normalsize\insertauthor\small \\ \insertinstitute
        }
        % \\%
        \vfill
        \parbox[b][\@titlegraphicheight][t]{\paperwidth-2ex-2ex-\@titlegraphicwidth}{%
          \vfill \small \insertdate~
        }%
        \parbox[b][\@titlegraphicheight][t]{\@titlegraphicwidth}{%
        \normalsize \vfill \hspace*{0.1cm} \inserttitlegraphic
        }%
        \vspace{1.0ex}
      \end{beamercolorbox}%
    \end{frame}
    \usebackgroundtemplate{}
    }
  }

  \newlength\footerimageheight\setlength\footerimageheight{30mm}
  \def\imageframe#1#2#3{%
    \normalsize
    \setlength\@titlegraphicwidth{\widthof{\inserttitlegraphic}}
    \setlength\@titlegraphicheight{\maxof{\heightof{\inserttitlegraphic}}{\heightof{\vbox{\insertdate}}}}
    %
    \begin{frame}[plain]%
      \begin{beamercolorbox}[wd=\paperwidth,ht=\paperheight,dp=1ex,leftskip=2ex, rightskip=2ex]{title foot}%
        % title
        \parbox[b][\@titlegraphicheight+1ex][t]{\paperwidth-2ex-2ex-4ex-\@titlegraphicwidth}{\normalsize\color{maincolor}\Large\bfseries~\\#1}%
        \parbox[b][\@titlegraphicheight+1ex][t]{4ex}{~}%
        \parbox[b][\@titlegraphicheight+1ex][t]{\@titlegraphicwidth}{\normalsize\vfill\inserttitlegraphic}%
        \\%
        % text
        \parbox[t][\paperheight-\@titlegraphicheight-1ex-3ex-\footerimageheight][t]{\paperwidth-2ex-2ex}{\vfill#2\vfill}%~\\[-1em]#2}%
        \\%
        % images
        \hspace*{-2ex}\parbox[b][\footerimageheight][t]{\paperwidth}{\vfill#3}%
      \end{beamercolorbox}%
    \end{frame}
  }
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% Set correct footnote sizes at begin document
% -------------------------------------------------------------------------- %
  \AtBeginDocument{
    % titlegraphic
    \settowidth\footerleftwidth{\tiny\inserttitlegraphic\hspace*{0.025\paperwidth}}%
    \setlength{\footerrightwidth}{\paperwidth-\footerleftwidth}%
  }
% -------------------------------------------------------------------------- %

% **************************************************************************** %



% **************************************************************************** %
% BIBLATEX CHANGES
% **************************************************************************** %

% -------------------------------------------------------------------------- %
% SHORTER FOOTNOTE CITE
% -------------------------------------------------------------------------- %
  \DeclareCiteCommand{\footshortcite}[\mkbibfootnote]
    {\usebibmacro{prenote}}
    {\printnames{labelname}(\printfield[year]{year})}
    {\multicitedelim}
    {\usebibmacro{postnote}}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% BIBLIOGRAPHY STYLE
% => Actual numbers instead of article images in bibliography
% -------------------------------------------------------------------------- %
  \setbeamertemplate{bibliography item}{\insertbiblabel}
% -------------------------------------------------------------------------- %

% -------------------------------------------------------------------------- %
% CLEARER FOOTNOTE
% => Avoid stuff in footnote when usage in beamer with \footfullcite
% -------------------------------------------------------------------------- %
  \AtEveryCitekey{\clearfield{url}}
  \AtEveryCitekey{\clearfield{doi}}
  \AtEveryCitekey{\clearfield{issn}}
  \AtEveryCitekey{\clearfield{urldate}}
  \AtEveryCitekey{\clearfield{urlyear}}
  \AtEveryCitekey{\clearfield{urlmonth}}
  \AtEveryCitekey{\clearfield{urlday}}
  % \AtEveryCitekey{\clearfield{title}}
% -------------------------------------------------------------------------- %

% **************************************************************************** %
