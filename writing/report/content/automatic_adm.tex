\newpage
\section{Automatic AD Mission Utility} \label{sec:automatic_adm}
As mentioned in the introduction, for practical applications, an automated ADM utility is preferred over manual execution of the workflow. % TODO: capitalize introduction?
Based upon the theory presented so far, a proof of concept tool is designed and developed herein, creating a minimal working program for the automatic pipeline execution.
Practical challenges and possible directions for future work are explored.
Furthermore, current limitations and noteworthy avenues for performance optimization are pointed out.
Overall, the aim is to aid future development by providing groundwork facilitating the design of more sophisticated algorithms and tooling.

The outline of this section is as follows:
Section \ref{sec:aadm_design} presents an overview of the design of the utility.
Remarks on the computation of the elemental Jacobians via AD are given in Section \ref{sec:aadm_jacobians}.
Thereafter, the implementation of the four pipeline steps is elaborated Sections \ref{sec:aadm_dag_extraction} to \ref{sec:aadm_recipe}.
Lastly, in Section \ref{sec:aadm_benchmarks} the tool is evaluated on the basis of benchmarks of the test case defined in the previous section.






\subsection{Design and Implementation} \label{sec:aadm_design}
The utility implements the pipeline to automatically generate a so-called \textit{recipe} for a given primal function. % TODO: does the utility implement, or use different phrasing?
The recipe is named as such because it contains all ingredients to accumulate the Jacobian using generalized face elimination.
Calling the recipe corresponds to the automatic execution of the optimized derivative code, which is the end goal of the AD mission workflow.
For the implementation, the Julia programming language \cite{bezanson2012julia} is chosen, since it allows for flexible analysis and augmentation of functions at runtime while retaining performance due to just-in-time compilation.

% TODO: does the \textit make sense for the important terms when they are introduced for the first time here? 
An overview of the design of the utility and the interaction between different program components implementing the pipeline steps is sketched in Figure \ref{fig:automatic_adm_design}.
Firstly, the primal is decomposed into vector elementals on the basis of a call tree obtained by tracing a primal function call using \texttt{Cassette.jl} \cite{julia_cassette}.
Given the \textit{trace}, a \textit{taping} procedure yields the high level dag arising from the choice of method to pick the elementals.
Due to the fact that the elementals are arbitrary multivariate vector functions, dependency analysis needs to be performed to determine the active input and output arguments and their relation.
The second step of the pipeline consists of profiling the dag using \texttt{BenchmarkTools.jl} \cite{julia_benchmarktools}, which results in an \textit{annotated dag}.
Thereafter, the annotated dag is passed to the \texttt{admission} utility \cite{2023_adm} to produce a \textit{schedule}, i.e.~a sequence of \textsc{GFE} steps to accumulate the Jacobian.
AD models of the elemental Jacobians required for profiling and the accumulation are computed via \texttt{Enzyme.jl} \cite{enzyme}.
To enable the accumulation of the Jacobian of the primal at any input arguments, a \textit{retrace}, which is an object to update primal arguments in the dag, is created.
Finally, a recipe is constructed from all ingredients: the dag, schedule, retrace and elemental Jacobian models.
%
In the following sections, the core components of the procedure are discussed in more detail.

\begin{figure}[ht!]
  \centering
  \input ../figures/adm_design.tex
  \caption{Sketch of the design of the automatic AD mission utility. The pipeline input is the primal function \textbf{f} and its arguments, while the output is a recipe to compute the Jacobian. Structures implementing concepts are shown in boxes. Dashed boxes signify concepts implemented as auxiliary functions. Arrows denote processes implementing the pipeline steps. Dotted trapezoids indicate employed Julia packages.}
  \label{fig:automatic_adm_design}
\end{figure}

Several simplifying assumptions are made in the development of the tool. % TODO start with 'Note that'?, also: 'in' or 'for' the development?
Consequently, the application of the utility is subject to these limitations.
The source code is assumed to be procedural.
Additionally, only pure vector valued functions with constant vector sizes are considered.
All non vector arguments of encountered functions are neglected.
Note that this is the reason for the test case implementation \ref{listing:ssprk3_dt} allocating the scalar time step as a vector of size one.
Furthermore, functions are assumed to return only passive values that are not relevant for the Jacobian computation.



























\clearpage % TODO: remove this maybe if not needed anymore for cleaner look (else 2 sentences at bottom of page here and also below)

% TODO: reduce this section? Maybe remove the table, only mention the essential things. Depends on size of the other sections.
\subsubsection{Elemental Jacobians} \label{sec:aadm_jacobians}
The ADM procedure relies on the availability of elemental Jacobians by means of AD models.
For that purpose, the source code transformation tool \texttt{Enzyme.jl} \cite{enzyme} is selected.
Enzyme's core functionality is encapsulated in the \texttt{autodiff} function, which implements forward (tangent) and reverse (adjoint) vector mode AD.
While Enzyme provides a \texttt{jacobian} method, it was found to be unreliable for the studied use cases, failing to compile reverse mode of some functions while taking extremely long to compile for others.
Moreover, it lacks the capability to accept a non identity seed matrix, which is required by the elimination steps of the \textsc{GFE} procedure.
For the above reasons, a custom \texttt{jacobian} function build upon \texttt{autodiff} is implemented.

Given a primal function, its arguments to differentiate at and a pair of input and output argument indices $(i,j)$, the implemented \texttt{jacobian} method computes $\partial v_j /\partial v_i$.
Thereby, $v_j$ is the argument that is treated as output and $v_i$ is the argument treated as input.
Note that arguments can be input and output arguments at the same time.
Internally, a given seed matrix for the input argument is split up into multiple chunks.
Each chunk is passed to a call of \texttt{autodiff}, which evaluates an AD model in vector mode.
Thereafter, derivatives are harvested and written into an output Jacobian matrix.
Repeating the steps for all chunks yields the desired Jacobian times seed matrix product in the output Jacobian.

An important aspect of the implementation is that \texttt{autodiff} operates on tuples of vectors.
Therefore, the input and output chunks are copied to and from, causing theoretically unnecessary allocations.
In practice these extra allocations are hard to avoid for the present choice of environment and implementation method. % realization method
To ensure that the results are representative and not skewed by these details, performance of the custom method is compared to Enzyme inbuilt methods in Table \ref{tab:jacobian_benchmarks}. Note that for the benchmarks, the chunk size is set to 16, as the henceforth used size of 32 crashes the reverse mode compilation of Enzyme's \texttt{jacobian} function.

\begin{table}[!ht]
  \centering

  \begin{tabular}{|p{4cm} || p{1.5cm}|p{1.5cm}|p{1.5cm}|p{1.5cm}|p{1.5cm}|p{1.5cm}|}
  \hline
  & \multicolumn{2}{c|}{$N=32$} & \multicolumn{2}{c|}{$N=128$} & \multicolumn{2}{c|}{$N=1048$} \\
  \cline{2-7} %\hline
  Jacobian method & Forward & Reverse & Forward & Reverse & Forward & Reverse \\
  \hline\hline
  Enzyme \texttt{jacobian} (copy)    & $5.053$ &    -     &  $110.0$  & -     & $68.47 \cdot 10^3$ & - \\
  Enzyme \texttt{jacobian}           & $5.485$ &  $10.02$ & $111.8$ & $213.8$ & $53.82 \cdot 10^3$ & $75.31 \cdot 10^3$ \\
  Enzyme \texttt{autodiff}           & $4.295$ &  $8.851$ & $73.61$ & $157.5$ & $8.931 \cdot 10^3$ & $14.77 \cdot 10^3$ \\
  \texttt{jacobian}                  & $4.429$ &  $9.181$ & $90.70$ & $202.3$ & $9.339 \cdot 10^3$ & $16.48 \cdot 10^3$ \\
  \hline
  \end{tabular}
  \caption{Median runtime in milliseconds to compute the Jacobian of \texttt{SSPRK3!} (fixed time step) with $N$ number of cells for various jacobian functions and Enzyme's \texttt{autodiff} function.
  Enzyme's inbuilt \texttt{jacobian} method needs to be wrapped, since it requires the function output to be returned.
  We distinguish between two wrappers: The first copies the input argument while the second modifies it.
  Note that the former version crashes the reverse mode compilation, while the later version results in erroneous Jacobians.
  \texttt{jacobian} denotes the custom implementation of a Jacobian driver based on repeated \texttt{autodiff} executions.
  The times of \texttt{autodiff} are multiplied with the amount of evaluations required to compute the Jacobian.
  All benchmarks were performed on the RWTH CLAIX-2023 compute cluster with \texttt{BenchmarkTools.jl} configured to run ten thousand samples or for one hour per test, whichever occurs first.}
  \label{tab:jacobian_benchmarks}
\end{table}

Comparing our \texttt{jacobian} implementation to \texttt{autodiff} shows an acceptable level of overhead.
Both Enzyme \texttt{jacobian} methods are considerably slower in all cases, especially for larger system sizes.
This further justifies the use of a custom method.
Due to the timings in Table \ref{tab:jacobian_benchmarks}, benchmark presented in this work are implied to be expressive and permit drawing meaningful conclusions.
%Nonetheless, future work should expend effort to optimize the allocations of the Jacobian computations.
%In particular, larger systems may be memory constrained, necessitating more efficient implementations.
%The only way to accomplish this may be by a source transformation tool acting on the compiler level.

































\subsubsection{Dag Extraction} \label{sec:aadm_dag_extraction} % tracing, taping (+dependency analysis), final dag
The dag extraction step consists of tracing of the primal function and taping of the trace to construct the high level dag.
It is assumed that any control flow in the primal function is at an elemental level, meaning that the high level structure of the program is static.
Furthermore, it is assumed that low level control flow does not change the performance characteristics of the primal.\\

\noindent
\textbf{Tracing:} A trace of the primal is created by using the source code reflection capabilities provided by \texttt{Cassette.jl} \cite{julia_cassette}.
The trace stores the nested structure of called functions, i.e.~the call tree, and their arguments.
In particular, a copy of and a reference to the arguments passed to each function are stored in the trace.
The argument copies are used to re-execute a lower level function out of order for the purpose of computing its Jacobian.
On the other hand, the references are used to track dependencies between functions by checking for equality of the different arguments' object identity. 
The resulting trace structure contains all information needed to generate the dag.
Since we are interested in the high level structure, we truncate the trace below a certain depth.
For the test case, a depth of one is selected.
The resulting truncated call tree is depicted in Figure \ref{fig:call_tree}.

\begin{figure}[ht!]
  \centering
  \input ../figures/rk3_call_tree.tex
  \caption{Call tree of depth one of the function \texttt{SSPRK3!}. The dashed lines indicate absence in the case of a constant time step.}
  \label{fig:call_tree}
\end{figure}

An important observation is the fact that all non leaf nodes of the call tree do not perform any operations, except for calling their children.
Therefore, execution of all leaf node functions in the correct order with the correct arguments is equivalent to executing the primal.
From this we infer, that only the leaf nodes induce edges in the dag.
Thus, a list of leaf nodes with corresponding arguments suffices to extract the dag of the primal program.

Figure \ref{fig:call_tree} indicates that the helper functions play an important role for the application of the utility.
Inlining one of them produces basic operations, such as multiplication or assignment, on the first level of the call tree, crashing the pipeline.
This illustrates the main drawback of this naive truncation approach: The depth does not necessarily equate with the complexity of the function.
As a consequence, careless application of the utility may produce unwanted simple elemental functions, blowing up the complexity of the dag.
Furthermore, unwanted low level elementals may lead to violation of the assumptions on the high level code, causing the pipeline to fail.

A better approach would be to prune the tree based on fitting criteria, such as a measure of function complexity, for example, the runtime.
Alternatively, non call tree based methods of extracting the program structure should be considered.
A promising approach is finding minimal cuts, meaning points in the program execution at which the size and number of active variables is minimal. % TODO: citation? (Simon euroAD Vortrag?)
%  since it was observed in Section \ref{sec:chain_bracketing} that differences in size
In the scope of this work, the simple approach, requiring a benign primal, suffices.\\
%
%
% ============== skip the modified call tree explanation, just mention helpers for illustrating that all need to be on same level
% \begin{figure}[ht!]
%   \centering
%   \begin{subfigure}{\textwidth}
%     \centering
%     \input ../figures/rk3_call_tree.tex
%     \caption{}
%     \label{fig:call_tree_base}
%   \end{subfigure}
%   \begin{subfigure}{\textwidth}
%     \centering
%     \vspace{1cm}
%     \input ../figures/rk3_call_tree_levels.tex
%     \caption{}
%     \label{fig:call_tree_layered}
%   \end{subfigure}
%   \caption{}
%   \label{fig:call_tree}
% \end{figure}
%
%
% ============= dependency =============

\noindent
\textbf{Dependency Analysis:} Leaves of the call tree are arbitrary multivariate vector functions.
Extracting a dag from a list of leaf functions requires knowing for each function which of its arguments are inputs and which are outputs.
This information is then used to generate vertices for the corresponding arguments and edges for the dependencies between arguments.
Dependency analysis is performed to determine the role of the arguments of any given function.
An argument passed to a function can be a pure input argument, meaning that it is not modified by the function call.
In contrast, a pure output argument is only written to and its input value does not influence the function execution.
Furthermore, an argument can act both as an input and output of the function.

An operator overloading approach is employed to implement the dependency analysis.
A tracking type, storing a dependency list, is implemented.
Arithmetic operators are overloaded for this type to propagate the dependencies from input to output arguments.
Replacing the elements of vector arguments of a target function with the tracking type enables the tracking of dependencies through a call of the function.
Afterwards, the dependencies can be harvested from the arguments, yielding a dependency matrix and a list of sparsity patterns.

The dependency matrix is a boolean matrix that denotes whether one argument depends on another.
For example, the dependency matrix of the auxiliary function \texttt{h1(u1, u, k1, $\Delta$t)} is depicted in Table \ref{tab:dependencies_example}.
Analogously to a Jacobian matrix, each row corresponds to an input argument (i.e.~before the function is called) while each column represents an output argument.
For example, the first row in Table \ref{tab:dependencies_example} tells us that \texttt{u1} does not influence any outputs, meaning it is a pure output.
In contrast, the first column tells us that \texttt{u1} depends on the other three arguments as inputs.
\begin{figure}[ht!]
  \begin{floatrow}
    \centering
    \capbtabbox{
      \begin{tabular}{ |c||c|c|c|c| }
        \hline
        input & \multicolumn{4}{c|}{output} \\
        \hline\hline
        $\downarrow$ & u1 & u & k1 & $\Delta t$ \\
        \hline
        u1 &  &  &  &  \\
        \hline
        u & x &  &  &  \\
        \hline
        k1 & x &  &  &  \\
        \hline
        $\Delta t$ & x &  &  &  \\
        \hline
      \end{tabular}
      \vspace{0.5cm}
    }{\caption{Dependency matrix of the function \texttt{h1!}.} \label{tab:dependencies_example}}
    \hspace{1cm}
  \ffigbox{
    \scalebox{.8}{\begin{tikzpicture}[main/.style = {draw, circle, minimum size=15mm}]
      \node[main, thick] at (0,3) (y1_out) {u1};
      \node[main, thick, dashed] at (2,3) (u_out) {u};
      \node[main, thick, dashed] at (4,3) (k1_out) {k1};
      \node[main, thick, dashed] at (6,3) (dt_out) {$\Delta t$};

      \node[main, thick, dashed] at (0,0) (1_in) {u1};
      \node[main, thick] at (2,0) (u_in) {u};
      \node[main, thick] at (4,0) (k1_in) {k1};
      \node[main, thick] at (6,0) (dt_in) {$\Delta t$};

      \draw [-Latex, thick] (u_in) -- (y1_out);
      \draw [-Latex, thick] (k1_in) -- (y1_out);
      \draw [-Latex, thick] (dt_in) -- (y1_out);
    \end{tikzpicture}}
  }{\caption{Subdag corresponding to Table \ref{tab:dependencies_example}. The vertex \texttt{u1} and the three edges are induced by the function \texttt{h1!}.} \label{fig:dependencies_subdag}} % TODO: des the word subdag exist?
  \end{floatrow}
\end{figure}
\clearpage
Each nonzero entry in the dependency matrix induces a corresponding edge in the high level dag.
The relevant portion of the dag is shown in Figure \ref{fig:dependencies_subdag}.
The lower and upper layers denote arguments as input and output respectively.
Dashed vertices are variables for which no vertex is created due to missing dependencies.
Most importantly, note that a single elemental function induces multiple edges in the dag.
This is contrary to the theory of elimination techniques, where it is implicitly assumed, that all elementals and their Jacobian computation are independent of each other.
The link between different edges can be used to eliminate multiple edges at once.
For example, when a function has multiple outputs, multiple Jacobians with respect to one input are accumulated at once when employing tangent mode.
Future work should consider treating sets of edges together to account for this practical aspect of elemental functions.
In this work, edge dependencies are neglected and all elemental Jacobians are treated independently.

Lastly, a sparsity pattern is extracted for each nonzero dependency between a pair of arguments.
Section \ref{sec:sparsity} utilizes these sparsity patterns to significantly speed up the elemental Jacobian accumulation.

Note that only structural dependencies are considered, not however arithmetic dependencies.
Hence, the patterns resulting from the dependency analysis are only structural.
Moreover, the outputs of minimum and maximum functions are set to depend on all inputs, since it is not known which of the inputs will determine the output for an arbitrary set of inputs.
As a consequence, the sparsity pattern of the variable time step test case, presented in Section \ref{sec:test_case}, is fully dense.
This is due to the fact that the time step can depend on any value of the top level input vector while simultaneously influencing all output values.
Resulting issues and a strategy to circumvent them is discussed in Section \ref{sec:sparsity}.\\



\noindent % ============= taping notes =============
\textbf{Taping:} From the trace, using dependency analysis, a dag is constructed by means of a taping procedure.
Taping in the present high level context is analogous to the taping process of an adjoint operator overloading implementation.
Conceptually, all elemental operations are successively assembled into a dag similarly to how they would be recorded into a tape structure.
The procedure is outlined in the following.

A vector of active variables is initialized to keep track of all relevant variables.
An active variable is a variable that plays a role in the derivative calculation.
The activity is propagated via the dependency between arguments, i.e.~a variable that depends on an active variable becomes active.
An entry in the active variable vector is composed of a reference to the corresponding variable and a vertex ID in the dag.
The reference is used to determine if encountered elemental function arguments are active variables.
The ID tracks the current vertex in the dag connected to the variable.
When the variable is modified inside an elemental, the associated vertex is updated accordingly.

At the start, the input argument to the top level primal is registered as input, adding it to the list of active variables and adding a corresponding initial vertex in the dag.
For all leaves collected from the trace, the dag is extended in three steps. % ignore non vector arguments
First, dependency analysis is performed to classify arguments as input or output arguments.
Secondly, for all output arguments that depend on at least one active input argument, a vertex is added into the dag and the argument is either registered as an active variable or the registration is updated.
Thirdly, edges from the active inputs to the outputs are created in the dag according to the dependency matrix.
For example, when processing \texttt{h1!} the vertex \texttt{u1} and the three edges are added to the dag.
This is illustrated in Figure \ref{fig:dependencies_subdag}.
The full dag resulting from the processing of all leaves is depicted in Figure \ref{fig:ssprk3_dag}.

Each edge stores the connected vertices, the elemental function and the argument values it is called with.
This allows the evaluation of AD models of the elementals by means of the Jacobian functions discussed in Section \ref{sec:aadm_jacobians}.
After all elementals are processed, the output of the primal is identified inside the dag.
The dag resulting from the taping procedure contains all necessary data for the execution of an elimination sequence, thus completing the second step of the AD mission pipeline.

\begin{figure}[ht!]
  \centering
  \input ../figures/ssprk3_dag.tex
  \caption{High level dag of the function \texttt{SSPRK3!}. The dashed lines indicate absence in the case of a constant time step. The input and output of the function are denoted with $u^{(0)}$ and $u^{(3)}$ respectively. The intermediate vertices follow the naming scheme of the source code in Listing \ref{listing:ssprk3_dt}.}
  \label{fig:ssprk3_dag}
\end{figure}



















































\subsubsection{Annotation and AD Mission Planning} \label{sec:aadm_annotation_admission}
The second step in the ADM pipeline is creating an annotated dag, containing the AD model evaluation costs for each edge, by means of profiling.
Applying the \texttt{jacobian} methods from Section \ref{sec:aadm_jacobians} to the edges of the dag yields the elemental Jacobian AD models.
The median runtime, taken as representative value for the cost, of these models is determined using BenchmarkTools.jl \cite{julia_benchmarktools}.
For the present test case, no difference in resulting schedules was observed after a few seconds of benchmarking.

Note that the \texttt{admission} utility requires the runtime to be converted into an integer fma operation cost.
Furthermore, the utility computes the cost of a matrix-multiplication of a $n\times k$ with a $k\times m$ matrix as $n\cdot k\cdot m$.
Hence, the calculated fma cost of the edge AD models should be comparable to the theoretical matrix-multiplication cost.
For this purpose, the model evaluations are treated as a matrix vector product.
For each edge, a matrix vector product of corresponding dimensions is benchmarked and the runtime cost is divided by the theoretical cost of the product to obtain a runtime to fma operations conversion factor for the edge.
Applying the cost conversion factor yields a suitable approximation of the integer cost value.
Future work should deal with runtime costs directly in the AD mission planning procedure by including real matrix multiplication timings into the cost calculation, yielding a more refined model for the elimination cost.

A schedule is obtained by calling the \texttt{admission} utility, detailed in \cite{2023_adm}, with the annotated dag.
% dt=const: greedy min fill-in gives a schedule with all types of steps, so good for testing
For the test case with a constant time step, the branch and bound method finds that pure tangent mode is the optimal accumulation strategy.
The additional complexity in the case of a varying time step causes the branch and bound method to take days instead of seconds.
Nonetheless, a solution better than pure tangent is obtained rather quickly.
In this solution, the Jacobian $\partial \Delta t /\partial u^{(0)}$ is accumulated with adjoint mode and then a dyadic product with $\partial u^{(3)}/\partial \Delta t$ is performed.
This results in a slightly lower theoretical cost than pure tangent.








% this is too much
%%%  ================= only show schedules for sparse case? (would be nice to have a schedule example, but it takes quite a bit of space)
% =======> reference the sparse schedules below (example of such a schedule in section .. Figure ..)
% % greedy min fill schedule | note the non pure tangent parts
% \tiny\textcolor{gray}{\begin{align*}
%   F'(0, 2) &\coloneqq\,\,\, \dot{F}(0, 2) \cdot I_{384} \\
%   F'(0, 3) &\coloneqq\,\,\, \dot{F}(0, 3) \cdot I_{384} \\
%   F'(0, 5) &\coloneqq\,\,\, \dot{F}(0, 5) \cdot I_{384} \\
%   F'(0, 7) &\coloneqq\,\,\, \dot{F}(0, 7) \cdot I_{384} \\
%   F'(1, 7) &\coloneqq\,\,\, \dot{F}(1, 7) \cdot I_{1} \\
%   \color{blue} F'(0, 1) \color{blue} &\coloneqq\,\,\, \color{blue} I_{1} \color{blue}\cdot \color{blue}\bar{F}(0, 1) \\
%   F'(0, 1, 3) &\mathrel{+}= \dot{F}(0, 1, 3) \cdot \color{blue}F'(0, 1) \\
%   F'(0, 1, 5) &\mathrel{+}= \dot{F}(0, 1, 5) \cdot \color{blue}F'(0, 1) \\
%   F'(0, 2, 3) &\mathrel{+}= \dot{F}(0, 2, 3) \cdot F'(0, 2) \\
%   F'(0, 2, 5) &\mathrel{+}= \dot{F}(0, 2, 5) \cdot F'(0, 2) \\
%   F'(0, 2, 7) &\mathrel{+}= \dot{F}(0, 2, 7) \cdot F'(0, 2) \\
%   F'(0, 3, 4) &\coloneqq\,\,\, \dot{F}(0, 3, 4) \cdot F'(0, 3) \\
%   F'(0, 4, 5) &\mathrel{+}= \dot{F}(0, 4, 5) \cdot F'(0, 4) \\
%   F'(0, 5, 6) &\coloneqq\,\,\, \dot{F}(0, 5, 6) \cdot F'(0, 5) \\
%   F'(0, 4, 7) &\mathrel{+}= \dot{F}(0, 4, 7) \cdot F'(0, 4) \\
%   F'(0, 6, 7) &\mathrel{+}= \dot{F}(0, 6, 7) \cdot F'(0, 6) \\
%   \color{blue} F'(0, 1, 7) \color{blue} &\mathrel{+}= \color{blue} F'(0, 1) \color{blue}\cdot \color{blue}F'(1, 7)
% \end{align*}}

% ===================
% elimination sequence explained: (i shifted indices by +1)
% (also look at the dag plot for this)
% ACC TAN (5 6)   # accumulate edge v5->v6 => dv6/dv5
% ELI TAN (5 6 7) # evaluate tangent edge v6=>v7 with dv6/dv5 as input => dv7/dv5 (contribution from path 5=>6=>7, which is the full dv7/d5)
% ELI ADJ (1 5 7) # evaluate adjoint edge v1=>v5 with the jacobian dv7/dv5 as seeding! =>  dv7/dv1 (along path 1=>5=>6=>7)
% ACC ADJ (1 7)   # accumulate dv7/dv1 (and add to the existing dv7/dv1) -> NOTE: jacobian function modification: accumulation (pass in jacobian & modify)
% ACC TAN (1 2)   # accumulate dv2/dv1
% ELI TAN (1 2 7) # use dv2/dv1 as seed to evaluate edge 2=>7 to increment onto dv7/dv1
% ELI TAN (1 2 3) # use dv2/dv1 as seed to evaluate edge 2=>3 to increment onto dv3/dv1 (which was zero before)
% ELI TAN (1 2 5) # use dv2/dv1 as seed to evaluate edge 2=>5 to increment onto dv5/dv1 (which was zero before)
% ELI MUL (1 5 7) # multiply dv7/dv5 (from step 2) with dv5/dv1 from last step onto dv7/dv1 += dv7/dv5*dv5/dv1
% ELI ADJ (4 5 7) # use dv7/dv5 as seeding to evaluate edge 4=>5 to increment dv7/dv4 (which was zero before)
% ACC TAN (1 3)   # accumulate dv3/dv1 (+= accumulation, since dv3/dv1 was written to before)
% ELI TAN (1 3 4) # use dv3/dv1 as seed to evaluate edge 3=>4 to increment dv4/dv1 (which was zero before)
% ELI TAN (1 4 7) # use dv4/dv1 as seed to evaluate edge 4=>7 to increment dv7/dv1
% ELI MUL (1 4 7) # multiply dv7/dv1 += dv7/dv4 * dv4/dv1 (this accounts for the dependency of v7 on v4 over vertex v5)

%  note: the first and second step could be done together (reduce ADM overhead by concatenating those steps) -> compiler tool can then remove the intermediate vertex v6 from its code dag representation to speed things up (Similarly for v2, I think)

% ===================


























\subsubsection{Recipe Creation and Execution} \label{sec:aadm_recipe}
The final step in the ADM pipeline consists of automatically implementing the optimized elimination sequence derived in the previous steps.
This is done by constructing a recipe object, so called because it contains all ingredients for an efficient Jacobian accumulation, thus forming a recipe for calculating the Jacobian.
The core components are (a) a list of allocated storage for all intermediate Jacobians required by the elimination sequence, (b) an augmented schedule that stores the elimination sequence steps, (c) the dag containing the elemental functions and their arguments and (d) a retrace to update the elemental function arguments.

As illustrated in the design sketch, Figure \ref{fig:automatic_adm_design}, the recipe is created from a retrace, which is clarified below, the dag and the schedule.
The schedule consists of (tangent and adjoint) accumulation steps and (tangent, adjoint and matrix-multiplication) elimination steps.
A Jacobian matrix-multiplication step operates on the relevant Jacobians in the recipe's list of Jacobians.
Execution of the other steps take the appropriate elemental and its arguments from the dag and evaluate the AD models via the methods introduced in Section \ref{sec:aadm_jacobians}.
The seed for the models are either Jacobians from the recipe's list for an elimination step, or an identity matrix for an accumulation step.
Accounting for the above relations, an anonymous function, with references to the appropriate objects as arguments, is created for each step and stored in an augmented schedule.
Executing all functions in the augmented schedule in order amounts to performing the elimination sequence.

% TODO: keep this paragraph?
%Note that all schedule steps are treated independently, meaning that successive evaluations with a single mode are not combined into a single step.
%In principle, given a schedule, the high level dag and line dag could be reduced to a minimal form to decrease overhead.
%For example, a pure tangent schedule can reduce a the test case dag to two vertices and a single edge, with the elemental being the primal itself.
%Although the reduction of overhead might be overkill, since it goes away for expensive elementals. \\

To compute an elemental Jacobian, the values of the input arguments of the elemental are required.
These need to be computed first.
In principle, the primal evaluation may be performed in parts during the elimination sequence, and "the additional evaluation of the primal should be accounted for when defining the computational costs associated with the vertices in the line dag"\cite{2023_adm}.
We make the simplifying assumption that the cost of the primal evaluation is negligible compared to the cost of executing the elimination sequence.
% For the considered test case this assumption seems to hold in the inspected cases.
% This assumption is not guaranteed in the sparse case, where the jacobian accumulation time is of the same asymptotic order as the primal
%and optimally is only a small constant factor more expensive. Then accounting for primal execution in the ADM scheduling/planing may yield significant benefits.
Therefore, before executing the augmented schedule, the entire primal is traced once to collect all desired elemental arguments.
This is the purpose of the retrace object.

The retrace stores references to the edges of the dag in a nested structure corresponding to the call tree structure of the primal program.
Similarly to the trace in Section \ref{sec:aadm_dag_extraction}, running the retracing functionality traces the primal function using \texttt{Cassette.jl}.
However, instead of extracting the structure, the existing structure is traversed and the encountered relevant elemental arguments are copied into the dag edges.

With all components in place, calling the recipe with a given primal input argument updates the elemental argument values by means of the retrace, zeros the Jacobian list, executes all functions in the augmented schedule and finally returns the primal return and its Jacobian.
This concludes the automatic pipeline execution.

% mention somewhere ? :  when a jacobian is used in ADM, it is final (no further changes)







































\clearpage % TODO: rm clearpage?
\subsection{Benchmarks} \label{sec:aadm_benchmarks} % vs "Benchmark Results and Interpretation"
The performance of the presented implementation of the automated AD mission pipeline is assessed on the basis of the test case introduced in Section \ref{sec:test_case}.
%Comparisons to conventional Jacobian accumulation methods are draw to evaluate the ADM approach.
Figure \ref{fig:runtime_dense} compares the runtime of different Jacobian accumulation methods.

In the case of a constant time step, plotted in Figure \ref{fig:runtime_dense_const_dt}, the schedule corresponds to dense tangent mode.
This case is useful to determine the overhead incurred by the implementation of the ADM approach.
For small system sizes some overhead is visible.
As the system size grows, the blue line draws closer to the purple line, meaning that the overhead becomes small relative to the total runtime.
As expected, the dense adjoint (reverse) mode is more expensive than the tangent (forward) mode.

The runtime of the primal scales linearly with the system size.
Hence, a single AD model evaluation of the primal, yielding a row or column of the Jacobian, scales in the same way.
Additionally, the number of rows and columns in the square Jacobian matrix grows linearly with the system size.
Therefore, we expect the overall asymptotic behavior of all accumulation methods to be quadratic in the number of cells.
This is observed in Figure \ref{fig:runtime_dense_const_dt}, as the dense AD mode and recipe lines are approximately parallel to the shown reference line.
Note that the Jacobian of the primal is a banded sparse matrix. %, because the one-dimensional explicit time stepping scheme induces a finite domain of dependence in a single step.
Jacobian coloring methods, discussed in the following Section \ref{sec:sparsity}, exploit the sparsity to reduce the number of AD model evaluations required to accumulate the Jacobian.
As a consequence, linear runtime is achieved, since only a constant amount of model evaluations is needed regardless of the system size.
To compete with these methods, the ADM procedure needs to also consider sparsity of the primal and elementals.

\begin{figure}[ht!]
  \centering
  \begin{subfigure}{0.49\textwidth}
    \centering
    \input ../figures/tex_images/runtime/runtime_const_dt_dense.tex
    \caption{$\Delta t = const.$}
    \label{fig:runtime_dense_const_dt}
  \end{subfigure}
  \begin{subfigure}{0.49\textwidth}
    \centering
    \input ../figures/tex_images/runtime/runtime_varying_dt_dense.tex
    \caption{$\Delta t \neq const$}
    \label{fig:runtime_dense_varying_dt}
  \end{subfigure}
  \caption{Median runtime of different Jacobian accumulation methods in relation to the primal system size.
  The constant time step (a) and varying time step (b) versions of the \texttt{SSPRK3!} primal are considered.
  The dense recipes are based on the branch and bound schedules described in Section \ref{sec:aadm_annotation_admission}.
  "jacobian" denotes use of the \texttt{jacobian} function introduced in Section \ref{sec:aadm_jacobians}.
  "compressed" denotes a coloring based Jacobian accumulation discussed in Section \ref{sec:jacobian_coloring}.
  Reference lines of linear and quadratic growth are dashed.
  The size of the square Jacobian is three times the number of cells $N$.
  Benchmarks were performed on the RWTH CLAIX-2023 compute cluster with BenchmarkTools.jl configured to run ten thousand samples or for one hour per test, whichever occurs first.}
  % TODO: either update benchmarks to be really cluster, or change description
  \label{fig:runtime_dense}
\end{figure}

In the case of a varying time step, plotted in Figure \ref{fig:runtime_dense_varying_dt}, the slight theoretical advantage in performance of the recipe is not visible.
Furthermore, the runtime of the compressed coloring and dense methods coincide.
The reason being that the structural sparsity pattern of the primal becomes dense due to the time step $\Delta t$ possibly depending on any input variable and influencing all output variables.
Note that the time step only depends on the single cell with the highest wave speed, resulting in a numerically sparse Jacobian.
However, the specific cell, that the time step depends on, is not known a priori, preventing basic compression methods from achieving any performance increase.
This, of course, is undesirable, since the elemental functions themselves still retain their sparsity.
Clearly, the AD mission approach needs to be extended to exploit sparsity and beyond that maintain the sparse efficiency of the elementals.

