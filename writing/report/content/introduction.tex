\section{Introduction} \label{sec:introduction}
Computing derivatives is a key component of many numerical methods and software programs.
Of particular interest are Jacobian matrices.
These play a vital role in Newton solvers, which are commonly used for the solution of nonlinear systems of equations.
Another application are AD checkpointing schemes, which are often required for remaining within memory bounds when differentiating algorithms and software with adjoint mode AD.
These schemes compress sections of the tape into a more memory efficient Jacobian.
Numerical programs often repeatedly execute the same function, such as in time integration schemes, where many structurally equal time steps are performed.
In these cases, instead of performing the tape compression operation on the fly, an efficient method of computing (i.e. accumulating) the Jacobian could be derived beforehand.
This has the potential to yield a significant reduction in computational cost, since the effort of deriving an optimal Jacobian accumulation procedure is amortized over the many applications.
In this way, even optimization problems requiring gradients may still rely on and benefit from efficient Jacobian accumulation procedures.

This work deals with the accumulation of moderately to large sized Jacobian matrices of multivariate vector functions, obtained for example by differentiation of large partial differential equation (PDEs) solvers or their components.
For this purpose, generalized face elimination is employed within the context of AD mission \cite{2023_adm}.
A high level view of numerical software is adopted, as the elimination technique is only performed on the coarse scale structure of the target program.
In general, elimination techniques are a promising method that has been successfully applied to low level functions in \cite{code_generation_vertex_elimination, source_transform_vertex_elimination} to gain runtime improvements.
The apparent usefulness motivates the application of the techniques also at a coarse scale.

Since the application of the AD mission workflow on a target code by hand is rather cumbersome, an automated procedure is desired.
Furthermore, the practical challenges faced during manual application have so far prevented the investigation of real world test cases.
%The cumbersome nature of the application of the AD mission workflow on a target code by hand has prevented the investigation of real world test cases.
The goal of this work is the design and development of a proof of concept tool capable of automatic execution of the AD mission pipeline.
Rapid application of the AD mission pipeline on real code by means of the tool enables the search for real world examples of runtime cost savings.
Insights gained from the application of the tool and during the development has the potential to guide future research.
Hence, a framework is established that may assist in the ongoing development of the methods and tools for their execution.
Moreover, the reader is introduced to all concepts necessary to understand the subject matter and start working in the presented research area. %TODO: didactic focus mention so ok?

The outline of the remaining report is as follows: %TODO: adjust this if needed after writing all sections
Section \ref{sec:theory} gives an overview over the required AD fundamentals to understand this work.
Therein, emphasis is put on elimination techniques for Jacobian accumulation.
Importantly, Section \ref{sec:theory_adm} covers AD mission planning and the AD mission workflow.
A test and benchmark case based on a Runge-Kutta time integrator is described in Section \ref{sec:test_case}.
Section \ref{sec:automatic_adm} discusses the design of the developed tool, followed by comparative benchmarks in Section \ref{sec:aadm_benchmarks}. 
Lastly, sparsity is integrated into the utility in Section \ref{sec:sparsity}.
Recommendations for future work are made and conclusions are drawn in Section \ref{sec:conclusion}.
