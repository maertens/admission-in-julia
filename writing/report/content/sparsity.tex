
%% TODO: 
% [x] write section intro
% [x] write benchmarks section
% [ ] write implementation section
% [ ] write coloring section

\section{Exploiting Sparsity of Elementals} \label{sec:sparsity}
The results obtained in the preceding Section indicate the necessity to handle sparse Jacobians efficiently.
In general, many numerical algorithms exhibit sparse structures which are inherited by their Jacobians.
In this section, the utility is expanded to efficiently accumulate sparse Jacobians of primal programs.
Furthermore, the ADM framework allows the exploitation of sparsity at the elemental component level.
In the following, Jacobian coloring is employed to perform compressed evaluations of the elemental Jacobians.

% include or leave out this paragraph? => leave out
% Note that for the considered test case, the dependency of the time step can be neglected to apply compressed accumulation methods.
% However, the incurred error can be quite significant.
% For example, with $N=128$ cells at simulation time $t=0.2$ the relative error in the Jacobian is approximately 2\% in the Frobenius norm and 20\% in the spectral norm.
% The accuracy of derivatives can be very important \cite{naumann_book}.
% Hence, accumulation methods should strive to include to maintain accuracy by including all dependencies while benefiting from sparse program portions.












% We restrict ourselves to (numerically) nonsymmetric Jacobians.
% Furthermore, we want to obtain the recover the jacobian from its compressed form directly (opposed to indirectly by successive substitution).
% Lastly, we use unidirectional matrix partitioning, meaning for adjoint a row-partitioning while for tangent a column partitioning.
% note: in our case, sparsity patterns are quite simple and amount to CPR (Curtis-Powell-Reid seeding)

% Limit ourselves to a basic applications of ColPack to explore the benefits of exploiting sparsity.
% This is a complex topic and for more complex Jacobian structures mixed tangent&adjoint methods could be considered.


\subsection{Embedding Jacobian Coloring into ADM} \label{sec:jacobian_coloring}
A method to efficiently accumulate sparse Jacobians is by means of graph coloring algorithms, detailed in \cite{what_color_is_your_jacobian}.
In the most basic form, the idea is to find groups of structurally independent columns or rows of the Jacobian.
Notice that structurally independent columns can be accumulated together in a single tangent mode evaluation, since no nonzero derivative values overlap in the linear combination of the resulting tangent vectors.
Hence, all seeding vectors of each color group can be added together into a single vector, resulting in a compressed seed matrix $S$.
Propagating $S$ through an AD model of the Jacobian $F'$, yields a compressed Jacobian $C$, from which the full Jacobian can be reconstructed.

% TODO: ref figure here if space in report left

% \begin{figure}[ht!]
%   \centering
%   \scalebox{.6}{\input{../figures/tex_images/colpack_tikz.tex}}
%   \caption{Column Compression}
%   \label{fig:coloring}
% \end{figure}


The compressed accumulation procedure can be applied to the primal itself, or in the context of ADM, to the elemental Jacobians.
The ColPack utility \cite{colpack} is used to compute colorings, i.e.~groups of structurally independent rows and columns, for a given sparsity pattern.
Reconstruction is performed by splitting up the columns or rows of the compressed Jacobian according to the sparsity pattern and color groups.
A reconstruction pattern is implemented, storing the column (for tangent mode or row for adjoint mode), to which each element of the compressed Jacobian belongs to.
Since the row (or column) index of an element remains the same during compression, the position of each element in the full matrix can be determined.

For the purpose of ADM, the compressed accumulation is extended to the case of Jacobians as seed matrix, required by the elimination steps.
In matrix notation, the accumulation by means of column-compression for tangent mode is equivalent to $C = F'\cdot S$.
Choosing the AD model evaluation $F' \coloneqq A'\cdot J$, with elemental Jacobian $A'$ and seed matrix $J$, as target for accumulation yields
\begin{equation}
  C = (A'\cdot J)\cdot S = A'\cdot (J\cdot S)\,.
\end{equation}
Hence, the elimination step, amounting to a Jacobian product, can be expressed as the evaluation of the elemental Jacobian with a modified seed matrix $J\cdot S$.
Analogous statements hold for adjoint mode.

To execute an elimination step, the seed matrix $S$ for the combined sparsity pattern of the elemental Jacobian $A'$ and seed Jacobian $J$ are computed.
Then, the seed Jacobian $J$ and matrix $S$ are multiplied to obtain the seed for the elemental model evaluation.
After the AD model evaluation, the full Jacobian can be recovered from the compressed Jacobian $C$ using the reconstruction pattern corresponding to the combined sparsity pattern of $J$ and $S$. 

The existing automatic ADM pipeline is augmented to create sparse recipes.
Compared to the dense recipe, a sparse recipe additionally contains compressed seed matrices and reconstruction patterns for all steps.
The steps in the augmented schedule are changed to perform compression and reconstruction at the beginning and end of each step respectively.
Note that the first three pipeline steps are kept as before, meaning that the cost model does not account for the sparsity.












\subsection{Benchmarks} \label{sec:sparse_benchmarks}
Similarly to Section \ref{sec:aadm_benchmarks}, the sparse ADM implementation is evaluated on the basis of test case benchmarks.
Figure \ref{fig:runtime_sparse} compares the runtime of different Jacobian accumulation methods. The reverse modes are left out for visual clarity.

In the case of a constant time step, depicted in Figure \ref{fig:runtime_sparse_const_dt}, the Jacobian is structurally sparse.
Consequently, we expect the compressed accumulation and the sparse recipe to perform similarly.
As in the dense case, some recipe overhead is observed for small system sizes.
However, as the system size grows, the sparse recipe runtime departs from the linear reference line.
This effect is attributed to the inefficient handling of sparse matrices in the implementation.
In the future, different matrix storage formats should be explored.
Additionally, the design of efficient interfaces between sparse matrices and the AD model functions becomes necessary to reduce overhead.

\begin{figure}[ht!]
  \centering
  \begin{subfigure}{0.49\textwidth}
    \centering
    \input ../figures/tex_images/runtime/runtime_const_dt_sparse.tex
    \caption{$\Delta t = const.$}
    \label{fig:runtime_sparse_const_dt}
  \end{subfigure}
  \begin{subfigure}{0.49\textwidth}
    \centering
    \input ../figures/tex_images/runtime/runtime_varying_dt_sparse.tex
    \caption{$\Delta t \neq const$}
    \label{fig:runtime_sparse_varying_dt}
  \end{subfigure}
  \caption{Median runtime of different sparse Jacobian accumulation methods in relation to the primal system size.
  The constant time step (a) and varying time step (b) versions of the \texttt{SSPRK3!} primal are considered.
  The sparse recipes are based on the branch and bound schedules described in Section \ref{sec:aadm_annotation_admission}.
  The "sparse recipe+" denotes the recipe employing the improved alternative schedule.
  "jacobian forward" and "compressed forward" denote tangent mode dense and compressed Jacobian accumulation respectively.
  Reference lines of linear and quadratic growth are dashed.
  The size of the square Jacobian is three times the number of cells $N$.
  Benchmarks were performed on the RWTH CLAIX-2023 compute cluster with BenchmarkTools.jl configured to run ten thousand samples or for one hour per test, whichever occurs first.}
  % TODO: either update benchmarks to be really cluster, or change description
  \label{fig:runtime_sparse}
\end{figure}

Runtime comparisons for the varying time step version of the primal are plotted in Figure \ref{fig:runtime_sparse_varying_dt}.
Once again, the color based compression fails in this case due to the dense structural sparsity.
Moreover, the schedule used so far also results in a similar quadratic runtime behavior.
The reason of this is the order of elimination steps.
The dense dependency of the primal output on the input over the time step is evaluated at the beginning, yielding a structurally dense Jacobian.
This Jacobian is then used as a seed for model evaluation in the following steps, inducing dense evaluations of elementals.
In this way, the dense dependency contaminates the elimination sequence.

A cheaper alternative schedule, that does not suffer from the aforementioned issue, can be constructed manually by delaying the accumulation of the dense dependency.
Furthermore, the new schedule fully exploits the size of the time step 'vector'.
By the chain rule \eqref{eq:chain_rule}, the target Jacobian can be written as
\begin{equation} \label{eq:delay_dt}
  \frac{du^{(3)}}{du^{(0)}} = \frac{\partial u^{(3)}}{\partial u^{(0)}} + \frac{\partial u^{(3)}}{\partial \Delta t}\frac{\partial \Delta t}{\partial u^{(0)}}\,,
\end{equation}
where the partial Jacobian $\partial u^{(3)}/\partial u^{(0)}$ does not contain the path over $\Delta t$.
This means that the dependency of $u^{(3)}$ on $u^{(0)}$ over $\Delta t$ is resolved at the end of the elimination sequence.
The first Jacobian of the sum in Equation \eqref{eq:delay_dt} is accumulated by means of a tangent elimination sequence in the same way as for the case of a constant time step.
Successive tangent steps are employed to cheaply compute the single column Jacobian $\partial u^{(3)} / \partial \Delta t$.
A single adjoint accumulation yields the gradient $\partial \Delta t / \partial u^{(0)}$.
Equation \eqref{eq:delay_dt} then represents the last elimination step in the \textsc{GFE} sequence, consisting of multiplication of the two right factors followed by addition onto the left Jacobian.
The corresponding schedule has a lower cost than the previous schedule mentioned in Section \ref{sec:aadm_annotation_admission} and is thus expected to be eventually obtained by the branch and bound procedure.
More importantly, the schedule has the property of preserving the sparsity of the elemental Jacobians during the elimination sequence.
Only at the very end will the dyadic product be performed and the Jacobian becomes dense.
This allows for the exploitation of sparsity in all intermediate elimination steps.

The new schedule is denoted with a plus sign in Figure \ref{fig:runtime_sparse_varying_dt} and achieves a linear runtime complexity for small systems.
Once again, the recipe departs from the linear runtime reference line for larger systems due to inefficient sparse matrix handling.
Nonetheless, it is shown that the sparse recipe yields a significantly lower runtime than any of the conventional methods.
The success of the method is owed to AD mission enabling us to leverage the high level program structure to maintain sparsity during the accumulation of the different elemental components.
These promising results motivate further investigation of the combination of sparse methods with generalized face elimination and AD mission.
