
\section{Test Case: 3rd Order Runge-Kutta Scheme} \label{sec:test_case} % Test Case: 3rd Order Runge-Kutta Finite Volume Solver
%In the following, we briefly introduce the application case to which the ADM utility is applied to in later sections.
%Furthermore, this example is used as a test case to evaluate different Jacobian accumulation methods.
A single step of a time stepping scheme is chosen as the application case, since time integrators are an essential high level component of many numerical schemes.
The ADM utility is applied to this test case in following sections and it is used to evaluate different Jacobian accumulation methods.
In the following, the numerical scheme is introduced briefly.
As this is not the focus of the work, the reader is referred to \cite{leveque2002} for a more detailed description of the methods.

% TODO: all methods are mentioned in the leveque book, but the original sources are the three papers (rusanov, shu & osher, sod); should I cite those three papers also? Seems correct to do, but 'clutters' references with non-ad stuff




\subsection{Problem Setup}
We consider the solution of a 1D Riemann problem on the domain $x\in[0,\,1]$ using a finite volume scheme.
The Riemann problem is centered at $x=0.5$ and has the initial left and right states $\rho_l = 1,\,p_l = 1,\,v_l = 0$ and $\rho_r = 0.125,\,p_r=0.1,\,v_r = 0$ respectively, which correspond to the so-called Sod shock tube problem \cite{sod_shock_tube,leveque2002}.
Here $\rho$ denotes the density, $v$ the velocity and $p$ the pressure with equation of state $p = (\gamma - 1)(\rho E - \rho vv/2)$, where $\gamma$ is the heat capacity ration and $E$ the specific total energy.
Note that the units of the quantities are disregarded herein.
The problem is governed by the Euler equations of gas dynamics:
\begin{equation} \label{eq:euler}
  \frac{\partial U}{\partial t} + \frac{\partial f(U)}{\partial x} = 0,\quad \text{with}\quad U = \begin{bmatrix}\rho \\ \rho v \\ \rho E\end{bmatrix} \quad \text{and} \quad f(U) = \begin{bmatrix}\rho v \\ \frac{2}{3}(\rho vv + \rho E)  \\ (\frac{5}{3}\rho E - \frac{1}{3}\rho v v)\,v \end{bmatrix},
\end{equation}
where $U$ is the vector of solution variables, $f$ the analytical flux function and a diatomic gas ($\gamma=7/5$), such as nitrogen, is assumed.
For the sake of simplicity, boundary conditions are set to Dirichlet conditions with values given by the initial conditions.

% TODO: active-passive form, which is better? (currently good, but starts with "The"... alternative: Discretization of the equations is performed... 
The equations are discretized in space with a simple finite volume scheme, resulting in the semi-discrete form:
\begin{equation} \label{eq:semidiscretization}
  \frac{\partial U_i}{\partial t} = \frac{1}{\Delta x}\left[\mathcal{F}(U_i,\,U_{i-1}) - \mathcal{F}(U_{i+1},\,U_{i})\right] \eqqcolon L(U_i)\,,
\end{equation}
where $\mathcal{F}$ is the numerical flux and the index $i$ ranges from one to the number of cells $N\in\mathbb{N}$.
The operator $L$ forms the right-hand side (\texttt{rhs}) of the ordinary differential equation (ODE) system \eqref{eq:semidiscretization}.
For the numerical flux, we employ the local Lax-Friedrichs flux \cite{rusanov_llf_flux,leveque2002}:
\begin{equation} \label{eq:llf_flux}
  %\mathcal{F}(U_i^{n},\,U_{i-1}^n) = \frac{1}{2}(f(U_{i}^n) + f(U_{i-1}^n))-\lambda\left(U_{i}^n - U_{i-1}^n\right),
  \mathcal{F}(U_i,\,U_{i-1}) = \frac{1}{2}(f(U_{i}) + f(U_{i-1}))-\lambda\left(U_{i} - U_{i-1}\right),
\end{equation}
where $\lambda$ is a maximum wave speed estimate of the two states given by:
\begin{equation}
  \lambda = \max_{k\in\{i-1,\,i\}}\sqrt{\gamma\,p_k/\rho_k}+|v_k|\,.
\end{equation}

%The numerical formulation arising from the spatial discretization is completed by discretization in time of the ODE system.
The ODE system, arising from the spatial discretization, is discretized in time to complete the numerical solver.
We select the strong stability preserving 3rd order Runge-Kutta (SSPRK3) scheme, given by \cite{shu_osher_ssprk3}:
\begin{subequations}\label{eq:ssprk3}
  \begin{align}
      u^{(1)} &= u^{(0)} + \Delta t\,L(u^{(0)}) \\[0.25cm]
      u^{(2)} &= u^{(0)} + \Delta t\left(\frac{1}{4} u^{(0)} + \frac{1}{4} L(u^{(1)})\right) \\[0.25cm]
      u^{(3)} &= u^{(0)} + \Delta t\left(\frac{1}{6} u^{(0)} + \frac{1}{6} L(u^{(1)}) + \frac{2}{3} L(u^{(2)})\right)\,,
  \end{align}
\end{subequations}
where $u^{(0)}$ is the previous time steps solution vector and $u^{(3)}$ is the result of the current time step.
The structure of the scheme \eqref{eq:ssprk3} is exposed to the AD mission procedure to optimize the computation of a single time step's Jacobian. % TODO: is this sentence here fitting? or section below in implementation?
Note that the time step $\Delta t$ can either be prescribed constant or depend on the solution of the previous time step via the CFL number \cite{leveque2002}, as is normally done in practice.
In the later case, the time step is determined by the maximal wave speed throughout the entire domain.
This yields two versions of the time stepping function, both of which are considered in this work as targets for Jacobian accumulation.







\subsection{Implementation}
A basic implementation of the time stepping scheme \eqref{eq:ssprk3} in the Julia programming language \cite{bezanson2012julia} is given in Listing \ref{listing:ssprk3_dt}.
The function \texttt{SSPRK3!} performs a time step and updates the solution vector \texttt{u} accordingly.
By convention, the exclamation mark indicates that the function modifies its arguments.
Here, the internally called functions write to their first argument as output.
Auxiliary vectors are allocated in line 2, which is not fully shown for brevity.
The time step \texttt{dt} is a vector of size one for technical reasons elaborated in Section \ref{sec:aadm_design}.
Successive calls to the right-hand side \texttt{rhs!} and helper functions \texttt{h1!}, \texttt{h2!} and \texttt{h3!} implement the three equations of scheme \eqref{eq:ssprk3}.
Lines 3 and 4 are omitted for the case of a constant time step.
Furthermore, the dependency of functions on the time step vanishes.
Verification of the code is performed by comparing results to reference solutions from \cite{leveque2002}.\\ % 'is' or 'was' performed

\noindent
\textsc{Listing 3.2:} \textit{Source code of a primal Julia function implementing a time step via Equation \eqref{eq:ssprk3}.}
% TODO: omit zero-init in first line for brevity?
\lstset{numbers=left,xleftmargin=2em, mathescape=true} % https://tex.stackexchange.com/questions/22368/line-numbers-in-listing-outside-of-paragraph
\lstset{basicstyle=\ttfamily, columns=fullflexible, keepspaces=true}
\begin{lstlisting}[label=listing:ssprk3_dt]
function SSPRK3!(u)
  k1, k2, k3, u1, u2 = zeros(eltype(u), length(u)), ...
  $\Delta$t = zeros(eltype(u), 1)
  calc_dt!($\Delta$t, u)
  rhs!(k1, u)
  h1!(u1, u, k1, $\Delta$t)
  rhs!(k2, u1)
  h2!(u2, u, k1, k2, $\Delta$t)
  rhs!(k3, u2)
  h3!(u, k1, k2, k3, $\Delta$t)
  return nothing
end
\end{lstlisting} % TODO: show results figure? kinda not really important, distracts from the AD => just mention that code is verified by comparing to reference solution
The Julia function produced by the source code \ref{listing:ssprk3_dt} is the primal target for optimization of the Jacobian accumulation by means of an automatic ADM utility developed in the following sections.
For that reason the structure of the source code is important.
Emphasis is put on simplicity to ease the automated processing of the code and for didactic purposes, reducing the amount of technical problems to treat.

No effort is made to optimize the performance of the underlying numerics.
As a consequence, many allocations are performed during the function's execution.
A rough estimate indicates a possible loss of performance of one to two orders of magnitude in program runtime. % factor ~50
Most of the inefficiencies are present in the lower level functions, such as \texttt{rhs!}.
We assume that the runtime proportion between primal functions and AD models remains the same between optimized and non-optimized versions of the code.
Note that the high level elementals should be adequately expensive for ADM overhead to be amortized.
As long as this property holds, the exact runtime does not matter.
%Therefore, the suboptimal runtime is treated as emulating a more sophisticated underlying numerical algorithm. % TODO: this is an alternative sentence...
%Increases in performance can be offset by optimization of the ADM overhead and by choosing more complex numerical algorithms.
Hence, we conclude that qualitative results will not be influenced by differences in implementation of the scheme.





